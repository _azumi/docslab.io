---
title: 'Tools'
---
- https://github.com/google/timesketch
- https://github.com/k1nd0ne/VolWeb
- https://github.com/ArsenalRecon/Arsenal-Image-Mounter
- https://github.com/Sentinel-One/CobaltStrikeParser
- https://ericzimmerman.github.io/KapeDocs/#!index.md
- https://ericzimmerman.github.io/#!index.md