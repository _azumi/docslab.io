---
title: 'Cheatsheet'
---

# Discovery and enumeration
```
nmap -A -sV <IP>
```

```
nikto -h <IP>
```

```
dirb http://<IP>
```

```
gobuster dir --url http://<IP> -w /usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt
```