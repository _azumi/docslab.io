# Red Team Recon

## Introduction

In a red team operation, you might start with no more than a company name, from which you need to start gathering information about the target. This is where reconnaissance comes into play. Reconnaissance (recon) can be defined as a preliminary survey or observation of your target (client) without alerting them to your activities. If your recon activities create too much noise, the other party would be alerted, which might decrease the likelihood of your success.

## Taxonomy of Reconnaissance

Reconnaissance (recon) can be classified into two parts:

- **Passive Recon**: can be carried out by watching passively
- **Active Recon**: requires interacting with the target to provoke it in order to observe its response.

Passive recon doesn't require interacting with the target. In other words, you aren't sending any packets or requests to the target or the systems your target owns. Instead, passive recon relies on publicly available information that is collected and maintained by a third party. Open Source Intelligence (OSINT) is used to collect information about the target and can be as simple as viewing a target's publicly available social media profile. Example information that we might collect includes domain names, IP address blocks, email addresses, employee names, and job posts.

Active recon requires interacting with the target by sending requests and packets and observing if and how it responds. The responses collected - or lack of responses - would enable us to expand on the picture we started developing using passive recon. An example of active reconnaissance is using Nmap to scan target subnets and live hosts.

Active recon can be classified as:

- **External Recon**: Conducted outside the target's network and focuses on the externally facing assets assessable from the Internet. One example is running Nikto from outside the company network.
- **Internal Recon**: Conducted from within the target company's network. In other words, the pentester or red teamer might be physically located inside the company building. In this scenario, they might be using an exploited host on the target's network. An example would be using Nessus to scan the internal network using one of the target’s computers.

## Built-in Tools

WHOIS is a request and response protocol that follows the RFC 3912 specification. A WHOIS server listens on TCP port 43 for incoming requests. The domain registrar is responsible for maintaining the WHOIS records for the domain names it is leasing. `whois` will query the WHOIS server to provide all saved records. In the following example, we can see `whois` provides us with:

- Registrar WHOIS server
- Registrar URL
- Record creation date
- Record update date
- Registrant contact info and address (unless withheld for privacy)
- Admin contact info and address (unless withheld for privacy)
- Tech contact info and address (unless withheld for privacy)

```bash
➜  ~ whois azumi.fr
% IANA WHOIS server
% for more information on IANA, visit http://www.iana.org
% This query returned 1 object

refer:        whois.nic.fr

domain:       FR

organisation: Association Française pour le Nommage Internet en Coopération (A.F.N.I.C.)
address:      Immeuble Le Stephenson
address:      1, rue Stephenson
address:      Montigny-Le-Bretonneux 78180
address:      France

contact:      administrative
name:         TLD Admin Contact
organisation: Association Française pour le Nommage Internet en Coopération (A.F.N.I.C.)
address:      Immeuble Le Stephenson
address:      1, rue Stephenson
address:      Montigny-Le-Bretonneux 78180
address:      France
phone:        +33 1 39 30 83 05
fax-no:       +33 1 39 30 83 01
e-mail:       tld-admin@nic.fr

contact:      technical
name:         TLD Technical Contact
organisation: Association Française pour le Nommage Internet en Coopération (A.F.N.I.C.)
address:      Immeuble Le Stephenson
address:      1, rue Stephenson
address:      Montigny-Le-Bretonneux 78180
address:      France
phone:        +33 1 39 30 83 00
fax-no:       +33 1 39 30 83 01
e-mail:       tld-tech@nic.fr

nserver:      D.NIC.FR 194.0.9.1 2001:678:c:0:0:0:0:1
nserver:      E.EXT.NIC.FR 193.176.144.22 2a00:d78:0:102:193:176:144:22
nserver:      F.EXT.NIC.FR 194.146.106.46 2001:67c:1010:11:0:0:0:53
nserver:      G.EXT.NIC.FR 194.0.36.1 2001:678:4c:0:0:0:0:1
ds-rdata:     51508 13 2 1b3386864d30ccc8f4541b985bf2ca320e4f52c57c53353f6d29c9ad58a5671f

whois:        whois.nic.fr

status:       ACTIVE
remarks:      Registration information: http://www.nic.fr

created:      1986-09-02
changed:      2022-10-04
source:       IANA

# whois.nic.fr

%%
%% This is the AFNIC Whois server.
%%
%% complete date format: YYYY-MM-DDThh:mm:ssZ
%%
%% Rights restricted by copyright.
%% See https://www.afnic.fr/en/domain-names-and-support/everything-there-is-to-know-about-domain-names/find-a-domain-name-or-a-holder-using-whois/
%%
%%

domain:                        azumi.fr
status:                        ACTIVE
eppstatus:                     active
hold:                          NO
holder-c:                      ANO00-FRNIC
admin-c:                       ANO00-FRNIC
tech-c:                        OVH5-FRNIC
registrar:                     OVH
Expiry Date:                   2023-04-27T12:26:44Z
created:                       2021-04-27T12:26:44Z
last-update:                   2022-04-27T12:36:29Z
source:                        FRNIC

nserver:                       dns107.ovh.net
nserver:                       ns107.ovh.net
source:                        FRNIC

key1-tag:                      8122
key1-algo:                     8 [RSASHA256]
key1-dgst-t:                   2 [SHA256]
key1-dgst:                     04F1BDCAEF26EEFC930E85147605B800C436316DA0A21B25AEFC74B9197D09F1
source:                        FRNIC

registrar:                     OVH
address:                       2 Rue Kellermann
address:                       59100 ROUBAIX
country:                       FR
phone:                         +33.899701761
fax-no:                        +33.320200958
e-mail:                        support@ovh.net
website:                       http://www.ovh.com
anonymous:                     No
registered:                    1999-10-18T00:00:00Z
source:                        FRNIC

nic-hdl:                       ANO00-FRNIC
type:                          PERSON
contact:                       Ano Nymous
registrar:                     OVH
anonymous:                     YES
remarks:                       -------------- WARNING --------------
remarks:                       While the registrar knows him/her,
remarks:                       this person chose to restrict access
remarks:                       to his/her personal data. So PLEASE,
remarks:                       don't send emails to Ano Nymous. This
remarks:                       address is bogus and there is no hope
remarks:                       of a reply.
remarks:                       -------------- WARNING --------------
obsoleted:                     NO
eppstatus:                     associated
eppstatus:                     active
eligstatus:                    not identified
reachstatus:                   not identified
source:                        FRNIC

nic-hdl:                       OVH5-FRNIC
type:                          ORGANIZATION
contact:                       OVH NET
address:                       OVH
address:                       140, quai du Sartel
address:                       59100 Roubaix
country:                       FR
phone:                         +33.899701761
e-mail:                        tech@ovh.net
registrar:                     OVH
changed:                       2023-03-03T07:37:48.631336Z
anonymous:                     NO
obsoleted:                     NO
eppstatus:                     associated
eppstatus:                     active
eligstatus:                    not identified
reachstatus:                   not identified
source:                        FRNIC

nic-hdl:                       ANO00-FRNIC
type:                          PERSON
contact:                       Ano Nymous
registrar:                     OVH
anonymous:                     YES
remarks:                       -------------- WARNING --------------
remarks:                       While the registrar knows him/her,
remarks:                       this person chose to restrict access
remarks:                       to his/her personal data. So PLEASE,
remarks:                       don't send emails to Ano Nymous. This
remarks:                       address is bogus and there is no hope
remarks:                       of a reply.
remarks:                       -------------- WARNING --------------
obsoleted:                     NO
eppstatus:                     associated
eppstatus:                     active
eligstatus:                    not identified
reachstatus:                   not identified
source:                        FRNIC

>>> WHOIS request date: 2023-03-03T07:47:04.03655Z <<<
```

As we can see above, it is possible to gain a lot of valuable information with only a domain name. After a `whois` lookup, we might get lucky and find names, email addresses, postal addresses, and phone numbers, in addition to other technical information. At the end of the `whois` query, we find the authoritative name servers for the domain in question.

DNS queries can be executed with many different tools found on our systems, especially Unix-like systems. One common tool found on Unix-like systems, Windows, and macOS is `nslookup`. In the following query, we can see how `nslookup` uses the default DNS server to get the A and AAAA records related to our domain.

```bash
➜  ~ nslookup azumi.fr
Server:		172.16.1.1
Address:	172.16.1.1#53

Non-authoritative answer:
Name:	azumi.fr
Address: 172.16.0.140
Name:	azumi.fr
Address: 82.65.82.55
```

Another tool commonly found on Unix-like systems is `dig`, short for Domain Information Groper (dig). `dig` provides a lot of query options and even allows you to specify a different DNS server to use. For example, we can use Cloudflare's DNS server: `dig @1.1.1.1 azumi.fr`.

```bash
➜  ~ dig azumi.fr

; <<>> DiG 9.10.6 <<>> azumi.fr
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 61047
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 1, ADDITIONAL: 0

;; QUESTION SECTION:
;azumi.fr.			IN	A

;; ANSWER SECTION:
azumi.fr.		1568	IN	A	82.65.82.55
azumi.fr.		545	IN	A	172.16.0.140

;; AUTHORITY SECTION:
azumi.fr.		545	IN	NS	ns1.azumi.fr.

;; Query time: 57 msec
;; SERVER: 172.16.1.1#53(172.16.1.1)
;; WHEN: Fri Mar 03 08:53:44 CET 2023
;; MSG SIZE  rcvd: 82
```

`host` is another useful alternative for querying DNS servers for DNS records. Consider the following example.

```bash
➜  ~ host azumi.fr
azumi.fr has address 172.16.0.140
azumi.fr has address 82.65.82.55
```

The final tool that ships with Unix-like systems is `traceroute`, or on MS Windows systems, `tracert`. As the name indicates, it traces the route taken by the packets from our system to the target host. The console output below shows that `traceroute` provided us with the routers (hops) connecting us to the target system. It's worth stressing that some routers don’t respond to the packets sent by `traceroute`, and as a result, we don’t see their IP addresses; a `*` is used to indicate such a case.

```bash
➜  ~ traceroute google.com
traceroute to google.com (142.250.74.238), 64 hops max, 52 byte packets
 1  172.16.1.1 (172.16.1.1)  0.961 ms  0.391 ms  0.346 ms
 2  194.149.169.156 (194.149.169.156)  17.789 ms  18.176 ms  17.231 ms
 3  magiconline.proxad.net (212.27.40.194)  17.531 ms  17.250 ms  17.350 ms
 4  72.14.220.92 (72.14.220.92)  18.618 ms  16.972 ms  17.331 ms
 5  * * *
 6  142.251.49.134 (142.251.49.134)  17.474 ms  17.451 ms
    par10s40-in-f14.1e100.net (142.250.74.238)  17.342 ms
```

In summary, we can always rely on:

- `whois` to query the WHOIS database
- `nslookup`, `dig`, or `host` to query DNS servers

WHOIS databases and DNS servers hold publicly available information, and querying either does not generate any suspicious traffic.

Moreover, we can rely on Traceroute (`traceroute` on Linux and macOS systems and `tracert` on MS Windows systems) to discover the hops between our system and the target host.

We can resolve `ipv6` with the following command:

```bash
➜  ~ dig google.com AAAA

; <<>> DiG 9.10.6 <<>> google.com AAAA
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 52046
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;google.com.			IN	AAAA

;; ANSWER SECTION:
google.com.		63	IN	AAAA	2a00:1450:4007:80b::200e

;; Query time: 55 msec
;; SERVER: 172.16.1.1#53(172.16.1.1)
;; WHEN: Fri Mar 03 09:07:56 CET 2023
;; MSG SIZE  rcvd: 56
```

## Advanced Searching

Being able to use a search engine efficiently is a crucial skill. The following table shows some popular search modifiers that work with many popular search engines.

| Symbol / Syntax | Function |
| --- | --- |
| `"search phrase"` | Find results with exact search phrase |
| `OSINT filetype:pdf` | Find files of type PDF related to a certain term. |
| `salary site:blog.tryhackme.com` | Limit search results to a specific site. |
| `pentest -site:example.com` | Exclude a specific site from results |
| `walkthrough intitle:TryHackMe` | Find pages with a specific term in the page title. |
| `challenge inurl:tryhackme` | Find pages with a specific term in the page URL. |

Note: In addition to `pdf`, other filetypes to consider are: `doc`, `docx`, `ppt`, `pptx`, `xls` and `xlsx`.

Each search engine might have a slightly varied set of rules and syntax. To learn about the specific syntax for the different search engines, you will need to visit their respective help pages. Some search engines, such as Google, provide a web interface for advanced searches: [Google Advanced Search](https://www.google.com/advanced_search). Other times, it is best to learn the syntax by heart, such as [Google Refine Web Searches](https://support.google.com/websearch/answer/2466433), [DuckDuckGo Search Syntax](https://help.duckduckgo.com/duckduckgo-help-pages/results/syntax/), and [Bing Advanced Search Options](https://help.bing.microsoft.com/apex/index/18/en-US/10002).

Search engines crawl the world wide web day and night to index new web pages and files. Sometimes this can lead to indexing confidential information. Examples of confidential information include:

- Documents for internal company use
- Confidential spreadsheets with usernames, email addresses, and even passwords
- Files containing usernames
- Sensitive directories
- Service version number (some of which might be vulnerable and unpatched)
- Error messages

Combining advanced Google searches with specific terms, documents containing sensitive information or vulnerable web servers can be found. Websites such as [Google Hacking Database](https://www.exploit-db.com/google-hacking-database) (GHDB) collect such search terms and are publicly available. Let's take a look at some of the GHDB queries to see if our client has any confidential information exposed via search engines. GHDB contains queries under the following categories:

- **Footholds**
    Consider [GHDB-ID: 6364](https://www.exploit-db.com/ghdb/6364) as it uses the query `intitle:"index of" "nginx.log"` to discover Nginx logs and might reveal server misconfigurations that can be exploited.
- **Files Containing Usernames**
    For example, [GHDB-ID: 7047](https://www.exploit-db.com/ghdb/7047) uses the search term `intitle:"index of" "contacts.txt"` to discover files that leak juicy information.
- **Sensitive Directories**
    For example, consider [GHDB-ID: 6768](https://www.exploit-db.com/ghdb/6768), which uses the search term `inurl:/certs/server.key` to find out if a private RSA key is exposed.
- **Web Server Detection**
    Consider [GHDB-ID: 6876](https://www.exploit-db.com/ghdb/6876), which detects GlassFish Server information using the query `intitle:"GlassFish Server - Server Running"`.
- **Vulnerable Files**
    For example, we can try to locate PHP files using the query `intitle:"index of" "*.php"`, as provided by [GHDB-ID: 7786](https://www.exploit-db.com/ghdb/7786).
- **Vulnerable Servers**
    For instance, to discover SolarWinds Orion web consoles, [GHDB-ID: 6728](https://www.exploit-db.com/ghdb/6728) uses the query `intext:"user name" intext:"orion core" -solarwinds.com`.
- **Error Messages**
    Plenty of useful information can be extracted from error messages. One example is [GHDB-ID: 5963](https://www.exploit-db.com/ghdb/5963), which uses the query `intitle:"index of" errors.log` to find log files related to errors.

You might need to adapt these Google queries to fit your needs as the queries will return results from all web servers that fit the criteria and were indexed. To avoid legal issues, it is best to refrain from accessing any files outside the scope of your legal agreement.

Now we'll explore two additional sources that can provide valuable information without interacting with our target:

- Social Media
- Job ads

### Social Media

Social media websites have become very popular for not only personal use but also for corporate use. Some social media platforms can reveal tons of information about the target. This is especially true as many users tend to overshare details about themselves and their work. To name a few, it's worthwhile checking the following:

- LinkedIn
- Twitter
- Facebook
- Instagram

Social media websites make it easy to collect the names of a given company's employees; moreover, in certain instances, you might learn specific pieces of information that can reveal answers to password recovery questions or gain ideas to include in a targeted wordlist. Posts from technical staff might reveal details about a company’s systems and vendors. For example, a network engineer who was recently issued Juniper certifications may allude to Juniper networking infrastructure being used in their employer’s environment.

### Job Ads

Job advertisements can also tell you a lot about a company. In addition to revealing names and email addresses, job posts for technical positions could give insight into the target company’s systems and infrastructure. The popular job posts might vary from one country to another. Make sure to check job listing sites in the countries where your client would post their ads. Moreover, it is always worth checking their website for any job opening and seeing if this can leak any interesting information.

Note that the [Wayback Machine](https://archive.org/web/) can be helpful to retrieve previous versions of a job opening page on your client’s site.

## Specialized Search Engines

### WHOIS and DNS Related

Beyond the standard WHOIS and DNS query tools, there are third parties that offer paid services for historical WHOIS data. One example is WHOIS history, which provides a history of WHOIS data and can come in handy if the domain registrant didn’t use WHOIS privacy when they registered the domain.

There are a handful of websites that offer advanced DNS services that are free to use. Some of these websites offer rich functionality and could have a complete room dedicated to exploring one domain.

We will consider the following:

- [ViewDNS.info](https://viewdns.info/)
- [Threat Intelligence Platform](https://threatintelligenceplatform.com/)

### ViewDNS.info

[ViewDNS.info](https://viewdns.info/) offers *Reverse IP Lookup*. Initially, each web server would use one or more IP addresses; however, today, it is common to come across shared hosting servers. With shared hosting, one IP address is shared among many different web servers with different domain names. With reverse IP lookup, starting from a domain name or an IP address, you can find the other domain names using a specific IP address(es).

In the figure below, we used reverse IP lookup to find other servers sharing the same IP addresses used by `google.com`. Therefore, it is important to note that knowing the IP address does not necessarily lead to a single website.

![a48af0ae84a65eceaec6fdd5c3c19f93.png](../img//a48af0ae84a65eceaec6fdd5c3c19f93.png)

### Threat Intelligence Platform

[Threat Intelligence Platform](https://threatintelligenceplatform.com/) requires you to provide a domain name or an IP address, and it will launch a series of tests from malware checks to WHOIS and DNS queries. The WHOIS and DNS results are similar to the results we would get using `whois` and `dig`, but Threat Intelligence Platform presents them in a more readable and visually appealing way. There is extra information that we get with our report. For instance, after we look up `azumi.fr`, we see that Name Server (NS) records were resolved to their respective IPv4 and IPv6 addresses, as shown in the figure below.

![619ca2c7e17938b7320c1866862336f8.png](../img//619ca2c7e17938b7320c1866862336f8.png)

On the other hand, when we searched for `google.com`, we could also get a list of other domains on the same IP address. The result we see in the figure below is different from the results we obtained using [ViewDNS.info](https://viewdns.info/).

![e8bd5092881d76186b238454511de332.png](../img//e8bd5092881d76186b238454511de332.png)

### Specialized Search Engines

#### Censys

[Censys Search](https://search.censys.io) can provide a lot of information about IP addresses and domains. In this example, we look up one of the IP addresses that `cafe.thmredteam.com` resolves to. We can easily infer that the IP address we looked up belongs to Cloudflare. We can see information related to ports 80 and 443, among others; however, it's clear that this IP address is used to server websites other than `cafe.thmredteam.com`. In other words, this IP address belongs to a company other than our client, [Organic Cafe](https://cafe.thmredteam.com). It's critical to make this distinction so that we don’t probe systems outside the scope of our contract.

![dd56effce08e796888d369325461ba90.png](../img//dd56effce08e796888d369325461ba90.png)

#### Shodan

To use Shodan from the command-line properly, you need to create an account with [Shodan](https://www.shodan.io/), then configure `shodan` to use your API key using the command, `shodan init API_KEY`.

You can use different filters depending on the [type of your Shodan account](https://account.shodan.io/billing). To learn more about what you can do with `shodan`, we suggest that you check out [Shodan CLI](https://cli.shodan.io/). Let’s demonstrate a simple example of looking up information about one the IP addresse we got from `nslookup azumi.fr`. Using `shodan host IP_ADDRESS`, we can get the geographical location of the IP address and the open ports, as shown below.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ shodan host 82.65.82.55
82.65.82.55
Hostnames:               82-65-82-55.subs.proxad.net
City:                    Paris
Country:                 France
Organization:            Proxad / Free SAS
Updated:                 2023-03-02T22:01:49.833240
Number of open ports:    2

Ports:
     80/tcp  
    443/tcp
```

## Recon-ng

Recon-ng is a framework that helps automate the OSINT work. It uses modules from various authors and provides a multitude of functionality. Some modules require keys to work; the key allows the module to query the related online API.

From a penetration testing and red team point of view, Recon-ng can be used to find various bits and pieces of information that can aid in an operation or OSINT task. All the data collected is automatically saved in the database related to your workspace. For instance, you might discover host addresses to later port-scan or collect contact email addresses for phishing attacks.

You can start Recon-ng by running the command `recon-ng`. Starting Recon-ng will give you a prompt like `[recon-ng][default] >`. At this stage, you need to select the installed module you want to use. However, if this is the first time you're running `recon-ng`, you will need to install the module(s) you need.

### Creating a Workspace

Run `workspaces create WORKSPACE_NAME` to create a new workspace for your investigation. For example, `workspaces create thmredteam` will create a workspace named `thmredteam`.

`recon-ng -w WORKSPACE_NAME` starts recon-ng with the specific workspace.

### Seeding the Database

In reconnaissance, you are starting with one piece of information and transforming it into new pieces of information. For instance, you might start your research with a company name and use that to discover the domain name(s), contacts and profiles. Then you would use the new information you obtained to transform it further and learn more about your target.

Let’s consider the case where we know the target's domain name, `thmredteam.com`, and we would like to feed it into the Recon-ng database related to the active workspace. If we want to check the names of the tables in our database, we can run `db schema`.

We want to insert the domain name `azumi.fr` into the domains table. We can do this using the command `db insert domains`.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace]
└─$ recon-ng -w thm
[*] Version check disabled.

    _/_/_/    _/_/_/_/    _/_/_/    _/_/_/    _/      _/            _/      _/    _/_/_/
   _/    _/  _/        _/        _/      _/  _/_/    _/            _/_/    _/  _/       
  _/_/_/    _/_/_/    _/        _/      _/  _/  _/  _/  _/_/_/_/  _/  _/  _/  _/  _/_/_/
 _/    _/  _/        _/        _/      _/  _/    _/_/            _/    _/_/  _/      _/ 
_/    _/  _/_/_/_/    _/_/_/    _/_/_/    _/      _/            _/      _/    _/_/_/    


                                          /\
                                         / \\ /\
    Sponsored by...               /\  /\/  \\V  \/\
                                 / \\/ // \\\\\ \\ \/\
                                // // BLACK HILLS \/ \\
                               www.blackhillsinfosec.com

                  ____   ____   ____   ____ _____ _  ____   ____  ____
                 |____] | ___/ |____| |       |   | |____  |____ |
                 |      |   \_ |    | |____   |   |  ____| |____ |____
                                   www.practisec.com

                      [recon-ng v5.1.2, Tim Tomes (@lanmaster53)]                       

[*] No modules enabled/installed.

[recon-ng][thm] > db insert domains
domain (TEXT): azumi.fr
notes (TEXT): 
[*] 1 rows affected.
```

### Recon-ng Marketplace

We have a domain name, so a logical next step would be to search for a module that transforms domains into other types of information. Assuming we are starting from a fresh installation of Recon-ng, we will search for suitable modules from the marketplace.

Before you install modules using the marketplace, these are some useful commands related to marketplace usage:

- `marketplace search KEYWORD` to search for available modules with keyword.
- `marketplace info MODULE` to provide information about the module in question.
- `marketplace install MODULE` to install the specified module into Recon-ng.
- `marketplace remove MODULE` to uninstall the specified module.

The modules are grouped under multiple categories, such as discovery, import, recon and reporting. Moreover, recon is also divided into many subcategories depending on the transform type. Run `marketplace search` to get a list of all available modules.

In the terminal below, we search for modules containing `domains-`.

```bash
[recon-ng][thm] > marketplace search domains-
[*] Searching module index for 'domains-'...

  +---------------------------------------------------------------------------------------------------+
  |                        Path                        | Version |     Status    |  Updated   | D | K |
  +---------------------------------------------------------------------------------------------------+
  | recon/domains-companies/censys_companies           | 2.0     | not installed | 2021-05-10 | * | * |
  | recon/domains-companies/pen                        | 1.1     | not installed | 2019-10-15 |   |   |
  | recon/domains-companies/whoxy_whois                | 1.1     | not installed | 2020-06-24 |   | * |
  | recon/domains-contacts/hunter_io                   | 1.3     | not installed | 2020-04-14 |   | * |
  | recon/domains-contacts/metacrawler                 | 1.1     | not installed | 2019-06-24 | * |   |
  | recon/domains-contacts/pen                         | 1.1     | not installed | 2019-10-15 |   |   |
  | recon/domains-contacts/pgp_search                  | 1.4     | not installed | 2019-10-16 |   |   |
  | recon/domains-contacts/whois_pocs                  | 1.0     | not installed | 2019-06-24 |   |   |
  | recon/domains-contacts/wikileaker                  | 1.0     | not installed | 2020-04-08 |   |   |
  | recon/domains-credentials/pwnedlist/account_creds  | 1.0     | not installed | 2019-06-24 | * | * |
  | recon/domains-credentials/pwnedlist/api_usage      | 1.0     | not installed | 2019-06-24 |   | * |
  | recon/domains-credentials/pwnedlist/domain_creds   | 1.0     | not installed | 2019-06-24 | * | * |
  | recon/domains-credentials/pwnedlist/domain_ispwned | 1.0     | not installed | 2019-06-24 |   | * |
  | recon/domains-credentials/pwnedlist/leak_lookup    | 1.0     | not installed | 2019-06-24 |   |   |
  | recon/domains-credentials/pwnedlist/leaks_dump     | 1.0     | not installed | 2019-06-24 |   | * |
  | recon/domains-domains/brute_suffix                 | 1.1     | not installed | 2020-05-17 |   |   |
  | recon/domains-hosts/binaryedge                     | 1.2     | not installed | 2020-06-18 |   | * |
  | recon/domains-hosts/bing_domain_api                | 1.0     | not installed | 2019-06-24 |   | * |
  | recon/domains-hosts/bing_domain_web                | 1.1     | not installed | 2019-07-04 |   |   |
  | recon/domains-hosts/brute_hosts                    | 1.0     | not installed | 2019-06-24 |   |   |
  | recon/domains-hosts/builtwith                      | 1.1     | not installed | 2021-08-24 |   | * |
  | recon/domains-hosts/censys_domain                  | 2.0     | not installed | 2021-05-10 | * | * |
  | recon/domains-hosts/certificate_transparency       | 1.2     | not installed | 2019-09-16 |   |   |
  | recon/domains-hosts/google_site_web                | 1.0     | not installed | 2019-06-24 |   |   |
  | recon/domains-hosts/hackertarget                   | 1.1     | not installed | 2020-05-17 |   |   |
  | recon/domains-hosts/mx_spf_ip                      | 1.0     | not installed | 2019-06-24 |   |   |
  | recon/domains-hosts/netcraft                       | 1.1     | not installed | 2020-02-05 |   |   |
  | recon/domains-hosts/shodan_hostname                | 1.1     | not installed | 2020-07-01 | * | * |
  | recon/domains-hosts/spyse_subdomains               | 1.1     | not installed | 2021-08-24 |   | * |
  | recon/domains-hosts/ssl_san                        | 1.0     | not installed | 2019-06-24 |   |   |
  | recon/domains-hosts/threatcrowd                    | 1.0     | not installed | 2019-06-24 |   |   |
  | recon/domains-hosts/threatminer                    | 1.0     | not installed | 2019-06-24 |   |   |
  | recon/domains-vulnerabilities/ghdb                 | 1.1     | not installed | 2019-06-26 |   |   |
  | recon/domains-vulnerabilities/xssed                | 1.1     | not installed | 2020-10-18 |   |   |
  +---------------------------------------------------------------------------------------------------+

  D = Has dependencies. See info for details.
  K = Requires keys. See info for details.

```

We notice many subcategories under `recon`, such as `domains-companies`, `domains-contacts`, and `domains-hosts`. This naming tells us what kind of new information we will get from that transformation. For instance, `domains-hosts` means that the module will find hosts related to the provided domain.

Some modules, like `whoxy_whois`, require a key, as we can tell from the `*` under the `K` column. This requirement indicates that this module is not usable unless we have a key to use the related service.

Other modules have dependencies, indicated by a `*` under the `D` column. Dependencies show that third-party Python libraries might be necessary to use the related module.

Let’s say that you are interested in `recon/domains-hosts/google_site_web`. To learn more about any particular module, you can use the command `marketplace info MODULE`; this is an essential command that explains what the module does. For example, `marketplace info google_site_web` provides the following description: “Harvests hosts from Google.com by using the ‘site’ search operator. Updates the ‘hosts’ table with the results.” In other words, this module will use the Google search engine and the “site” operator.

We can install the module we want with the command `marketplace install MODULE`, for example, `marketplace install google_site_web`.

### Working with Installed Modules

We can work with modules using:

- `modules search` to get a list of all the installed modules
- `modules load MODULE` to load a specific module to memory

Let’s load the module that we installed earlier from the marketplace, `modules load viewdns_reverse_whois`. To `run` it, we need to set the required options.

- `options list` to list the options that we can set for the loaded module.
- `options set <option> <value>` to set the value of the option.

In a previous step, we have installed the module `google_site_web`, so let’s load it using `load google_site_web` and run it with `run`. We have already added the domain `thmredteam.com` to the database, so when the module is run, it will read that value from the database, get new kinds of information, and add them to the database in turn. The commands and the results are shown in the terminal output below.

```bash
           
pentester@TryHackMe$ recon-ng -w thmredteam
[...]
[recon-ng][thmredteam] > load google_site_web
[recon-ng][thmredteam][google_site_web] > run

--------------
THMREDTEAM.COM
--------------
[*] Searching Google for: site:thmredteam.com
[*] Country: None
[*] Host: cafe.thmredteam.com
[*] Ip_Address: None
[*] Latitude: None
[*] Longitude: None
[*] Notes: None
[*] Region: None
[*] --------------------------------------------------
[*] Country: None
[*] Host: clinic.thmredteam.com
[*] Ip_Address: None
[*] Latitude: None
[*] Longitude: None
[*] Notes: None
[*] Region: None
[*] --------------------------------------------------
[...]
[*] 2 total (2 new) hosts found.
[recon-ng][thmredteam][google_site_web] >
```

This module has queried Google and discovered two hosts, `cafe.thmredteam.com` and `clinic.thmredteam.com`. It is possible that by the time you run these steps, new hosts will also appear.

### Keys

Some modules cannot be used without a key for the respective service API. `K` indicates that you need to provide the relevant service key to use the module in question.

- `keys list` lists the keys
- `keys add KEY_NAME KEY_VALUE` adds a key
- `keys remove KEY_NAME` removes a key

Once you have the set of modules installed, you can proceed to load and run them.

- `modules load MODULE` loads an installed module
- `CTRL + C` unloads the module.
- `info` to review the loaded module’s info.
- `options list` lists available options for the chosen module.
- `options set NAME VALUE`
- `run` to execute the loaded module.

## Maltego

[Maltego](https://www.maltego.com/) is an application that blends mind-mapping with OSINT. In general, you would start with a domain name, company name, person’s name, email address, etc. Then you can let this piece of information go through various transforms.

The information collected in Maltego can be used for later stages. For instance, company information, contact names, and email addresses collected can be used to create very legitimate-looking phishing emails.

Think of each block on a Maltego graph as an entity. An entity can have values to describe it. In Maltego’s terminology, a **transform** is a piece of code that would query an API to retrieve information related to a specific entity. The logic is shown in the figure below. *Information* related to an entity goes via a *transform* to return zero or more entities.

![0daa8b88bb440e09736555cca0793cb4.png](../img//0daa8b88bb440e09736555cca0793cb4.png)

Every transform might lead to several new values. For instance, if we start from the “DNS Name” `cafe.thmredteam.com`, we expect to get new kinds of entities based on the transform we use. For instance, “To IP Address” is expected to return IP addresses as shown next.

![036ac0224d56c969a55d9342fa9c0ea7.png](../img//036ac0224d56c969a55d9342fa9c0ea7.png)

One way to achieve this on Maltego is to right-click on the “DNS Name” `cafe.thmredteam.com` and choose:

1.  Standard Transforms
2.  Resolve to IP
3.  To IP Address (DNS)

After executing this transform, we would get one or more IP addresses, as shown below.

![606c7f479c8305c37450b22a6d6fdb3d.png](../img//606c7f479c8305c37450b22a6d6fdb3d.png)

Then we can choose to apply another transform for one of the IP addresses. Consider the following transform:

1.  DNS from IP
2.  To DNS Name from passive DNS (Robtex)

This transform will populate our graph with new DNS names. With a couple more clicks, you can get the location of the IP address, and so on. The result might be similar to the image below.

![f6b9692f4b1310659060d908d6206ff8.png](../img//f6b9692f4b1310659060d908d6206ff8.png)

The above two examples should give you an idea of the workflow using Maltego. You can observe that all the work is based on transforms, and Maltego will help you keep your graph organized. You would get the same results by querying the different online websites and databases; however, Maltego helps you get all the information you need with a few clicks.

You get plenty of information, from names and email addresses to IP addresses. The results of `whois` and `nslookup` are shown visually in the following Maltego graph. Interestingly, Maltego transforms were able to extract and arrange the information returned from the WHOIS database. Although the returned email addresses are not helpful due to privacy protection, it is worth seeing how Maltego can extract such information and how it's presented.

![5ea3cfd2ba73c5c0cda6419c1796fe35.png](../img//5ea3cfd2ba73c5c0cda6419c1796fe35.png)

Now that we have learned how Maltego’s power stems from its transforms, the only logical thing is to make Maltego more powerful by adding new Transforms. Transforms are usually grouped into different categories based on data type, pricing, and target audience. Although many transforms can be used using Maltego Community Edition and free transforms, other transforms require a paid subscription. A screenshot is shown below to give a clearer idea.

![3edcc5f5855d8c37856a7d84a7222a7e.png](../img//3edcc5f5855d8c37856a7d84a7222a7e.png)

Using Maltego requires activation, even if you opt for Maltego CE (Community Edition). Therefore, the following questions can be answered by visiting [Maltego Transform Hub](https://www.maltego.com/transform-hub/) or by installing and activating Maltego CE on your own system.