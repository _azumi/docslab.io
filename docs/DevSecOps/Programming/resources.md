---
title: 'Resources'
---

## Python
- https://github.com/TheAlgorithms/Python
- https://github.com/vinta/awesome-python

## Bash
- https://github.com/dylanaraps/pure-bash-bible

## Vim
- https://linuxhandbook.com/pro-vim-tips/

## Freely available programming books
- https://github.com/EbookFoundation/free-programming-books

## Coding Interview University
- https://github.com/jwasham/coding-interview-university

## Awesome lists about all kinds of interesting topics.
- https://github.com/sindresorhus/awesome

## Roadmap to becoming a web developer in 2022.
- https://github.com/kamranahmedse/developer-roadmap

## JavaScript
- https://github.com/trekhleb/javascript-algorithms
- https://github.com/ryanmcdermott/clean-code-javascript
- https://github.com/lydiahallie/javascript-questions/

## NodeJS
- https://github.com/goldbergyoni/nodebestpractices

## Real World
- https://github.com/gothinkster/realworld

## Tech Interview Handbook
- https://github.com/yangshun/tech-interview-handbook

## Free For Dev
- https://github.com/ripienaar/free-for-dev

## Awesome For Beginners
- https://github.com/MunGell/awesome-for-beginners

## AWS
- https://github.com/open-guides/og-aws

## Awesome Web Development Resources
- https://github.com/markodenic/web-development-resources