---
title: 'Cheatsheet'
---

# Force deleting secrets entry
Sometimes you can have an error like:
```
* failed to revoke "db/prod/creds/admin/<id>" (1 / 1): failed to revoke entry: resp: (*logical.Response)(nil) err: failed to find entry for connection with name: "postgres"
```

```bash
vault lease revoke -force -prefix $DB_NAME/creds/
vault delete sys/mounts/$DB_NAME
```