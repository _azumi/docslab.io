---
title: 'Cheatsheet'
---

# Gitlab Cheatsheet

#### Create a new repo from scratch
```bash
git init
git add .
git commit -m 'First commit'
git remote add origin git@gitlab.azumi.fr:azumi-lab/<group>/<project>
git push -u origin <master/main>
```

#### Pull from remote overwriting local changes.
```bash
git stash #revert and stash changes locally
git pull #pull from remote normally
```

**Note**: To retrieve changes, do `git stash apply``

#### Delete only untracked files from working tree
```bash
git clean -f -d
```

#### Unstage files from index (but keep the files locally)
```bash
git rm --cached
```

#### Undo a merge
```bash
git checkout branch-name
git log --oneline
git revert -m commit-id
```

#### Remove a file from remote
```bash
git rm filename (remove file from git)
git commit -m "commit message"
git push
```

#### Undo last n commits
```bash
git reset --soft HEAD^n #where n is the number of last commits you want to undo
```

#### See diff between branches
```bash
git diff branch_1..branch_2
```

#### Remove a tag from branch
```bash
git push origin :refs/tags/tagname
git tag -d
```

#### Rename a branch locally & remote
```bash
git checkout old-branch
git branch -m new-name
git push origin :old-name new-name
```

#### Move existing, uncommitted work to a new branch
```bash
git switch -c <new-branch> (git 2.3 onwards)
```